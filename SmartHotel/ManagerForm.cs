﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;

namespace SmartHotel
{
    public partial class ManagerForm : Form
    {
        public Button btnNow;//выбранная в меню кнопка

        public ManagerForm()
        {
            InitializeComponent();
            btnNow = GraphBtn;
            _graphControl.BringToFront();
            _reservationControl.SetArrivalControl(_arrivalControl);
            _graphControl.SetControl(_reservationControl,_arrivalControl,_evictionControl);


        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void ChangeMenuBtn(Button changeBtn)
        {
            SelectorMenu.Top = changeBtn.Top;
            changeBtn.BackColor = Color.FromArgb(60, 23, 49);
            btnNow.BackColor = Color.FromArgb(54, 21, 38);
            btnNow = changeBtn;
        }

        private void ReservationBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(ReservationBtn);
            _reservationControl.ShowForm();
            _reservationControl.BringToFront();
        }

        private void GraphBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(GraphBtn);
            _graphControl.ShowForm();
            _graphControl.BringToFront();
        }



        private void ArrivalBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(ArrivalBtn);
            _arrivalControl.ShowForm();
            _arrivalControl.BringToFront();

        }

        private void ConferencHallBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(ConferencHallBtn);
            _planningHallControl.BringToFront();
        }

        private void EvictionBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(EvictionBtn);
            _evictionControl.ShowForm();
            _evictionControl.BringToFront();
        }

        //private void ServiceHotelBtn_Click(object sender, EventArgs e)
        //{
        //    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
        //    {
        //        if (db.PlanningRooms.FirstOrDefault() != null)
        //        {
        //            ChangeMenuBtn(ServiceHotelBtn);
        //            _serviceControl.ShowForm();
        //            _serviceControl.BringToFront();
        //        }
        //        else
        //        {
        //            SmartHotel.MessageClass.ShowPlanningServiceError();
        //            ChangeMenuBtn(GraphBtn);//переход на элемент График
        //            _graphControl.BringToFront();
        //        }
        //    }
        //}
    }
}
