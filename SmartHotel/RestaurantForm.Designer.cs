﻿namespace SmartHotel
{
    partial class RestaurantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestaurantForm));
            this.panelHeader = new System.Windows.Forms.Panel();
            this.MinimizeButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SelectorMenu = new System.Windows.Forms.Panel();
            this.NewDishBtn = new System.Windows.Forms.Button();
            this.GraphBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this._graphRestaurantControl = new SmartHotel.RestaurantControl.GraphRestaurantControl();
            this._newDishControl = new SmartHotel.RestaurantControl.NewDishControl();
            this.panelHeader.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(24)))), ((int)(((byte)(32)))));
            this.panelHeader.Controls.Add(this.MinimizeButton);
            this.panelHeader.Controls.Add(this.ExitButton);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(213, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(899, 29);
            this.panelHeader.TabIndex = 7;
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.BackgroundImage = global::SmartHotel.Properties.Resources.Mini;
            this.MinimizeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MinimizeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.Location = new System.Drawing.Point(842, 5);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(20, 20);
            this.MinimizeButton.TabIndex = 4;
            this.MinimizeButton.UseVisualStyleBackColor = false;
            this.MinimizeButton.Click += new System.EventHandler(this.MinimizeButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.BackgroundImage = global::SmartHotel.Properties.Resources.Exit;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.Location = new System.Drawing.Point(871, 4);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(20, 20);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.SelectorMenu);
            this.panel1.Controls.Add(this.NewDishBtn);
            this.panel1.Controls.Add(this.GraphBtn);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 694);
            this.panel1.TabIndex = 6;
            // 
            // SelectorMenu
            // 
            this.SelectorMenu.BackgroundImage = global::SmartHotel.Properties.Resources.selector;
            this.SelectorMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectorMenu.Location = new System.Drawing.Point(0, 218);
            this.SelectorMenu.Name = "SelectorMenu";
            this.SelectorMenu.Size = new System.Drawing.Size(10, 91);
            this.SelectorMenu.TabIndex = 8;
            // 
            // NewDishBtn
            // 
            this.NewDishBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.NewDishBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NewDishBtn.FlatAppearance.BorderSize = 0;
            this.NewDishBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NewDishBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewDishBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.NewDishBtn.Image = global::SmartHotel.Properties.Resources.reserve;
            this.NewDishBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NewDishBtn.Location = new System.Drawing.Point(0, 310);
            this.NewDishBtn.Name = "NewDishBtn";
            this.NewDishBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.NewDishBtn.Size = new System.Drawing.Size(213, 90);
            this.NewDishBtn.TabIndex = 3;
            this.NewDishBtn.Text = "Заказ";
            this.NewDishBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.NewDishBtn.UseVisualStyleBackColor = false;
            this.NewDishBtn.Click += new System.EventHandler(this.NewDishBtn_Click);
            // 
            // GraphBtn
            // 
            this.GraphBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(23)))), ((int)(((byte)(49)))));
            this.GraphBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GraphBtn.FlatAppearance.BorderSize = 0;
            this.GraphBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GraphBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GraphBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.GraphBtn.Image = global::SmartHotel.Properties.Resources.graph1;
            this.GraphBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GraphBtn.Location = new System.Drawing.Point(0, 218);
            this.GraphBtn.Name = "GraphBtn";
            this.GraphBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.GraphBtn.Size = new System.Drawing.Size(213, 90);
            this.GraphBtn.TabIndex = 1;
            this.GraphBtn.Text = "График";
            this.GraphBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.GraphBtn.UseVisualStyleBackColor = false;
            this.GraphBtn.Click += new System.EventHandler(this.GraphBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SmartHotel.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // _graphRestaurantControl
            // 
            this._graphRestaurantControl.BackColor = System.Drawing.Color.Transparent;
            this._graphRestaurantControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_graphRestaurantControl.BackgroundImage")));
            this._graphRestaurantControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._graphRestaurantControl.Location = new System.Drawing.Point(238, 61);
            this._graphRestaurantControl.Name = "_graphRestaurantControl";
            this._graphRestaurantControl.Size = new System.Drawing.Size(852, 621);
            this._graphRestaurantControl.TabIndex = 8;
            // 
            // _newDishControl
            // 
            this._newDishControl.BackColor = System.Drawing.Color.Transparent;
            this._newDishControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_newDishControl.BackgroundImage")));
            this._newDishControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._newDishControl.Location = new System.Drawing.Point(238, 61);
            this._newDishControl.Name = "_newDishControl";
            this._newDishControl.Size = new System.Drawing.Size(852, 621);
            this._newDishControl.TabIndex = 9;
            // 
            // RestaurantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(1112, 694);
            this.Controls.Add(this._newDishControl);
            this.Controls.Add(this._graphRestaurantControl);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RestaurantForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RestaurantForm";
            this.panelHeader.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button MinimizeButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel SelectorMenu;
        private System.Windows.Forms.Button NewDishBtn;
        private System.Windows.Forms.Button GraphBtn;
        private RestaurantControl.GraphRestaurantControl _graphRestaurantControl;
        private RestaurantControl.NewDishControl _newDishControl;
    }
}