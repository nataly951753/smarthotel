﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class ManyRoomsForm : Form
    {
        SmartHotelDatabaseEntities db;
        public ManyRoomsForm(SmartHotelDatabaseEntities db_)
        {
            InitializeComponent();
            db = db_;
            typeComboBox.DataSource = db.TypeRooms.Select(x => x.Type).ToList();

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private int BeforeAndFrom()
        {
            try
            {
                int from = int.Parse(numberFromTextbox.Text);
                int before = int.Parse(numberBeforeTextBox.Text);
                return before - from;
            }
            catch
            {
                return -1;
            }
        }
        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (floortextBox.Text != "" && cost_textBox.Text != "" && count_adult_textBox.Text != "" && count_children_textBox.Text != "")
            {
                double cost;
                try
                {
                    cost = Double.Parse(cost_textBox.Text);
                    return cost;
                }
                catch
                {
                    return -1;
                }
            }
            return -1;
        }
        //проверка на уникальность 
        private bool NumberIsOriginal(string number)
        {
            Rooms r = db.Rooms.FirstOrDefault(x => (x.Number == number));
            return (r == null);
        }
        private void AddManyRoomsBtn_Click(object sender, EventArgs e)
        {

            if (BeforeAndFrom() > 0)
            {
                double _c = TextBoxesHaveText();
                if (_c > 0)//если текстовые поля заполнены
                {
                    int before = int.Parse(numberBeforeTextBox.Text);
                    int from = int.Parse(numberFromTextbox.Text);
                    int type = db.TypeRooms.FirstOrDefault(x => x.Type == typeComboBox.Text).Id;
                    for (int i = from; i <= before; i++)
                    {
                        if (NumberIsOriginal(i.ToString()))
                        {
                            db.Rooms.Add(new Rooms { Number = i.ToString(), Floor = floortextBox.Text, Type = type, NumberOfAdults = count_adult_textBox.Text, NumberOfChildren = count_children_textBox.Text, Cost = _c, Description = descriptionTextBox.Text, Breakfast = breakfastcheckBox.Checked, Lunch = lunchcheckBox.Checked, Dinner = dinnercheckBox.Checked });
                            db.SaveChanges();//сохраняем изменения бд      
                        }
                    }
                    SmartHotel.MessageClass.AddSuccessfully();
                    this.Close();
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            else
            {
                SmartHotel.MessageClass.RoomManyError();
            }
        }
    }
}
