﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;

namespace SmartHotel
{
    public partial class ArrivalInfoForm : Form
    {
        public ArrivalInfoForm(PlanningRooms pl)
        {
            InitializeComponent();
            FullName_label.Text = pl.FullName;
            Adult_label.Text = pl.NumberOfAdult;
            Children_label.Text = pl.NumberOfChildren;
            DescriptionTextBox.Text = pl.FoodPreferences;
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
