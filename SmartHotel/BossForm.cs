﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;


namespace SmartHotel
{
    public partial class BossForm : Form
    {
        public Button btnNow;//выбранная в меню кнопка
        public BossForm()
        {
            InitializeComponent();
            btnNow = HotelInformationBtn;//выбираем текущей кнопкой первую
            _hotelInformationControl.BringToFront();//перемещаем элемент на передний план
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void HotelInformationBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(HotelInformationBtn);
            _hotelInformationControl.BringToFront();
        }

        private void EmployerBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(EmployerBtn);
            _employerControl.BringToFront();
        }

        private void TypeNumberBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(TypeNumberBtn);
            _typeNumberControl.BringToFront();
        }

        private void PlanningBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(PlanningBtn);
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                //проверка на существование типов номеров
                if (db.TypeRooms.FirstOrDefault() != null)
                {
                    _roomsControl.ShowForm();
                    _roomsControl.BringToFront();
                }
                else
                {
                    SmartHotel.MessageClass.ShowPlanningRoomError();
                    ChangeMenuBtn(TypeNumberBtn);//переход на элемент Типы номеров
                    _typeNumberControl.BringToFront();
                }
            }
        }

        private void ConferencHallBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(ConferencHallBtn);
            _conferencHallControl.BringToFront();
        }

        private void RestaurantBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(RestaurantBtn);
            _typeDishesControl.BringToFront();
        }

        private void ServiceTypeBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(ServiceTypeBtn);
            _typeServiceControl.BringToFront();
        }
        //изменение текущей кнопки
        private void ChangeMenuBtn(Button changeBtn)
        {
            SelectorMenu.Top = changeBtn.Top;
            changeBtn.BackColor = Color.FromArgb(60, 23, 49);
            btnNow.BackColor = Color.FromArgb(54, 21, 38);
            btnNow = changeBtn;
        }

        
    }
}
