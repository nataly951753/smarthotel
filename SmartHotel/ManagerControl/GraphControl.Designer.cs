﻿namespace SmartHotel
{
    partial class GraphControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.GraphGridView = new System.Windows.Forms.DataGridView();
            this.InfoRoomBtn = new System.Windows.Forms.Button();
            this.ArrivalBtn = new System.Windows.Forms.Button();
            this.ResorveBtn = new System.Windows.Forms.Button();
            this.labelMonth = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GraphGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(145, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 43);
            this.label1.TabIndex = 56;
            this.label1.Text = "График";
            // 
            // GraphGridView
            // 
            this.GraphGridView.AllowUserToAddRows = false;
            this.GraphGridView.AllowUserToDeleteRows = false;
            this.GraphGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.GraphGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GraphGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GraphGridView.ColumnHeadersHeight = 30;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GraphGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.GraphGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.GraphGridView.Location = new System.Drawing.Point(46, 68);
            this.GraphGridView.MultiSelect = false;
            this.GraphGridView.Name = "GraphGridView";
            this.GraphGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GraphGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GraphGridView.RowHeadersVisible = false;
            this.GraphGridView.RowHeadersWidth = 65;
            this.GraphGridView.RowTemplate.Height = 30;
            this.GraphGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GraphGridView.Size = new System.Drawing.Size(756, 431);
            this.GraphGridView.TabIndex = 59;
            // 
            // InfoRoomBtn
            // 
            this.InfoRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.InfoRoomBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.InfoRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.InfoRoomBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoRoomBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.InfoRoomBtn.Location = new System.Drawing.Point(46, 526);
            this.InfoRoomBtn.Name = "InfoRoomBtn";
            this.InfoRoomBtn.Size = new System.Drawing.Size(168, 44);
            this.InfoRoomBtn.TabIndex = 155;
            this.InfoRoomBtn.Text = "Информ. номер";
            this.InfoRoomBtn.UseVisualStyleBackColor = false;
            this.InfoRoomBtn.Click += new System.EventHandler(this.InfoRoomBtn_Click);
            // 
            // ArrivalBtn
            // 
            this.ArrivalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ArrivalBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ArrivalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ArrivalBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ArrivalBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ArrivalBtn.Location = new System.Drawing.Point(644, 526);
            this.ArrivalBtn.Name = "ArrivalBtn";
            this.ArrivalBtn.Size = new System.Drawing.Size(156, 44);
            this.ArrivalBtn.TabIndex = 157;
            this.ArrivalBtn.Text = "Заселение";
            this.ArrivalBtn.UseVisualStyleBackColor = false;
            this.ArrivalBtn.Click += new System.EventHandler(this.ArrivalBtn_Click);
            // 
            // ResorveBtn
            // 
            this.ResorveBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ResorveBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ResorveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResorveBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResorveBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ResorveBtn.Location = new System.Drawing.Point(482, 526);
            this.ResorveBtn.Name = "ResorveBtn";
            this.ResorveBtn.Size = new System.Drawing.Size(156, 44);
            this.ResorveBtn.TabIndex = 158;
            this.ResorveBtn.Text = "Бронь";
            this.ResorveBtn.UseVisualStyleBackColor = false;
            this.ResorveBtn.Click += new System.EventHandler(this.ResorveBtn_Click);
            // 
            // labelMonth
            // 
            this.labelMonth.AutoSize = true;
            this.labelMonth.BackColor = System.Drawing.Color.Transparent;
            this.labelMonth.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.labelMonth.Location = new System.Drawing.Point(527, 21);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Size = new System.Drawing.Size(123, 43);
            this.labelMonth.TabIndex = 159;
            this.labelMonth.Text = "Месяц";
            // 
            // GraphControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.labelMonth);
            this.Controls.Add(this.ResorveBtn);
            this.Controls.Add(this.ArrivalBtn);
            this.Controls.Add(this.InfoRoomBtn);
            this.Controls.Add(this.GraphGridView);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "GraphControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.GraphGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView GraphGridView;
        private System.Windows.Forms.Button InfoRoomBtn;
        private System.Windows.Forms.Button ArrivalBtn;
        private System.Windows.Forms.Button ResorveBtn;
        private System.Windows.Forms.Label labelMonth;
    }
}
