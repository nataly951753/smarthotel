﻿namespace SmartHotel
{
    partial class PlanningHallControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ExpenseBtn = new System.Windows.Forms.Button();
            this.materialDivider7 = new MaterialSkin.Controls.MaterialDivider();
            this.ExpenseTextBox = new System.Windows.Forms.TextBox();
            this.materialDivider5 = new MaterialSkin.Controls.MaterialDivider();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.materialDivider6 = new MaterialSkin.Controls.MaterialDivider();
            this.count_adult_textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.arrivalmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label6 = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.person_phone_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DeletePlHallBtn = new System.Windows.Forms.Button();
            this.ChangePlHallBtn = new System.Windows.Forms.Button();
            this.AddPlHallBtn = new System.Windows.Forms.Button();
            this.HallGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameComboBox = new System.Windows.Forms.ComboBox();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.person_name_TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.StartHoursComboBox = new System.Windows.Forms.ComboBox();
            this.StartMinComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.FinishMinComboBox = new System.Windows.Forms.ComboBox();
            this.FinishHourComboBox = new System.Windows.Forms.ComboBox();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.HallGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // ExpenseBtn
            // 
            this.ExpenseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExpenseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExpenseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExpenseBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExpenseBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ExpenseBtn.Location = new System.Drawing.Point(414, 312);
            this.ExpenseBtn.Name = "ExpenseBtn";
            this.ExpenseBtn.Size = new System.Drawing.Size(141, 44);
            this.ExpenseBtn.TabIndex = 187;
            this.ExpenseBtn.Text = "Счёт";
            this.ExpenseBtn.UseVisualStyleBackColor = false;
            this.ExpenseBtn.Click += new System.EventHandler(this.ExpenseBtn_Click);
            // 
            // materialDivider7
            // 
            this.materialDivider7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider7.Depth = 0;
            this.materialDivider7.Location = new System.Drawing.Point(557, 351);
            this.materialDivider7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider7.Name = "materialDivider7";
            this.materialDivider7.Size = new System.Drawing.Size(240, 1);
            this.materialDivider7.TabIndex = 182;
            this.materialDivider7.Text = "materialDivider7";
            // 
            // ExpenseTextBox
            // 
            this.ExpenseTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ExpenseTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ExpenseTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExpenseTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExpenseTextBox.Location = new System.Drawing.Point(557, 319);
            this.ExpenseTextBox.Name = "ExpenseTextBox";
            this.ExpenseTextBox.Size = new System.Drawing.Size(240, 32);
            this.ExpenseTextBox.TabIndex = 181;
            this.ExpenseTextBox.Text = "0";
            this.ExpenseTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // materialDivider5
            // 
            this.materialDivider5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider5.Depth = 0;
            this.materialDivider5.Location = new System.Drawing.Point(414, 305);
            this.materialDivider5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider5.Name = "materialDivider5";
            this.materialDivider5.Size = new System.Drawing.Size(383, 1);
            this.materialDivider5.TabIndex = 180;
            this.materialDivider5.Text = "materialDivider5";
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DescriptionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DescriptionTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DescriptionTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DescriptionTextBox.Location = new System.Drawing.Point(414, 197);
            this.DescriptionTextBox.Multiline = true;
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(383, 109);
            this.DescriptionTextBox.TabIndex = 179;
            // 
            // materialDivider6
            // 
            this.materialDivider6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider6.Depth = 0;
            this.materialDivider6.Location = new System.Drawing.Point(546, 145);
            this.materialDivider6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider6.Name = "materialDivider6";
            this.materialDivider6.Size = new System.Drawing.Size(251, 1);
            this.materialDivider6.TabIndex = 175;
            this.materialDivider6.Text = "materialDivider6";
            // 
            // count_adult_textBox
            // 
            this.count_adult_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_adult_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_adult_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_adult_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_adult_textBox.Location = new System.Drawing.Point(546, 113);
            this.count_adult_textBox.Name = "count_adult_textBox";
            this.count_adult_textBox.Size = new System.Drawing.Size(251, 32);
            this.count_adult_textBox.TabIndex = 172;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label9.Location = new System.Drawing.Point(409, 116);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 32);
            this.label9.TabIndex = 174;
            this.label9.Text = "Взрослых";
            // 
            // arrivalmonthCalendar
            // 
            this.arrivalmonthCalendar.Location = new System.Drawing.Point(57, 197);
            this.arrivalmonthCalendar.MaxSelectionCount = 1;
            this.arrivalmonthCalendar.Name = "arrivalmonthCalendar";
            this.arrivalmonthCalendar.TabIndex = 170;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(50, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 32);
            this.label6.TabIndex = 168;
            this.label6.Text = "Дата";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(541, 107);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(255, 1);
            this.materialDivider1.TabIndex = 164;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // person_phone_textBox
            // 
            this.person_phone_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.person_phone_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.person_phone_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.person_phone_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.person_phone_textBox.Location = new System.Drawing.Point(542, 76);
            this.person_phone_textBox.Name = "person_phone_textBox";
            this.person_phone_textBox.Size = new System.Drawing.Size(255, 32);
            this.person_phone_textBox.TabIndex = 162;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(408, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 32);
            this.label2.TabIndex = 163;
            this.label2.Text = "Телефон";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(51, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 32);
            this.label8.TabIndex = 161;
            this.label8.Text = "Зал";
            // 
            // DeletePlHallBtn
            // 
            this.DeletePlHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeletePlHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeletePlHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeletePlHallBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeletePlHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeletePlHallBtn.Location = new System.Drawing.Point(352, 366);
            this.DeletePlHallBtn.Name = "DeletePlHallBtn";
            this.DeletePlHallBtn.Size = new System.Drawing.Size(146, 44);
            this.DeletePlHallBtn.TabIndex = 156;
            this.DeletePlHallBtn.Text = "Удалить";
            this.DeletePlHallBtn.UseVisualStyleBackColor = false;
            this.DeletePlHallBtn.Click += new System.EventHandler(this.DeletePlHallBtn_Click);
            // 
            // ChangePlHallBtn
            // 
            this.ChangePlHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ChangePlHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangePlHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangePlHallBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangePlHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ChangePlHallBtn.Location = new System.Drawing.Point(504, 366);
            this.ChangePlHallBtn.Name = "ChangePlHallBtn";
            this.ChangePlHallBtn.Size = new System.Drawing.Size(146, 44);
            this.ChangePlHallBtn.TabIndex = 155;
            this.ChangePlHallBtn.Text = "Изменить";
            this.ChangePlHallBtn.UseVisualStyleBackColor = false;
            this.ChangePlHallBtn.Click += new System.EventHandler(this.ChangePlHallBtn_Click);
            // 
            // AddPlHallBtn
            // 
            this.AddPlHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddPlHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddPlHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddPlHallBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddPlHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddPlHallBtn.Location = new System.Drawing.Point(656, 366);
            this.AddPlHallBtn.Name = "AddPlHallBtn";
            this.AddPlHallBtn.Size = new System.Drawing.Size(146, 44);
            this.AddPlHallBtn.TabIndex = 154;
            this.AddPlHallBtn.Text = "Бронь";
            this.AddPlHallBtn.UseVisualStyleBackColor = false;
            this.AddPlHallBtn.Click += new System.EventHandler(this.AddPlHallBtn_Click);
            // 
            // HallGridView
            // 
            this.HallGridView.AllowUserToAddRows = false;
            this.HallGridView.AllowUserToDeleteRows = false;
            this.HallGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.HallGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HallGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.HallGridView.ColumnHeadersHeight = 30;
            this.HallGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HallGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.HallGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.HallGridView.Location = new System.Drawing.Point(56, 416);
            this.HallGridView.MultiSelect = false;
            this.HallGridView.Name = "HallGridView";
            this.HallGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HallGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.HallGridView.RowHeadersVisible = false;
            this.HallGridView.RowHeadersWidth = 65;
            this.HallGridView.RowTemplate.Height = 30;
            this.HallGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HallGridView.Size = new System.Drawing.Size(746, 160);
            this.HallGridView.TabIndex = 160;
            this.HallGridView.DoubleClick += new System.EventHandler(this.HallGridView_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ФИО";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 174;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Телефон";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Зал";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Начало";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 170;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Конец";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Номер заказа";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // nameComboBox
            // 
            this.nameComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nameComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nameComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.nameComboBox.FormattingEnabled = true;
            this.nameComboBox.Location = new System.Drawing.Point(154, 113);
            this.nameComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.nameComboBox.Name = "nameComboBox";
            this.nameComboBox.Size = new System.Drawing.Size(244, 35);
            this.nameComboBox.TabIndex = 152;
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(154, 107);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(243, 1);
            this.materialDivider4.TabIndex = 159;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // person_name_TextBox
            // 
            this.person_name_TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.person_name_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.person_name_TextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.person_name_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.person_name_TextBox.Location = new System.Drawing.Point(155, 76);
            this.person_name_TextBox.Name = "person_name_TextBox";
            this.person_name_TextBox.Size = new System.Drawing.Size(243, 32);
            this.person_name_TextBox.TabIndex = 153;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(51, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 32);
            this.label5.TabIndex = 158;
            this.label5.Text = "ФИО";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(279, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 43);
            this.label1.TabIndex = 157;
            this.label1.Text = "Конференц-залы";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(235, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 32);
            this.label3.TabIndex = 188;
            this.label3.Text = "Время";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(409, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 32);
            this.label4.TabIndex = 189;
            this.label4.Text = "Требования";
            // 
            // StartHoursComboBox
            // 
            this.StartHoursComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.StartHoursComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StartHoursComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartHoursComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartHoursComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.StartHoursComboBox.FormattingEnabled = true;
            this.StartHoursComboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.StartHoursComboBox.Location = new System.Drawing.Point(241, 233);
            this.StartHoursComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.StartHoursComboBox.Name = "StartHoursComboBox";
            this.StartHoursComboBox.Size = new System.Drawing.Size(73, 35);
            this.StartHoursComboBox.TabIndex = 190;
            // 
            // StartMinComboBox
            // 
            this.StartMinComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.StartMinComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StartMinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartMinComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.StartMinComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.StartMinComboBox.FormattingEnabled = true;
            this.StartMinComboBox.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.StartMinComboBox.Location = new System.Drawing.Point(324, 233);
            this.StartMinComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.StartMinComboBox.Name = "StartMinComboBox";
            this.StartMinComboBox.Size = new System.Drawing.Size(73, 35);
            this.StartMinComboBox.TabIndex = 191;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label7.Location = new System.Drawing.Point(235, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 32);
            this.label7.TabIndex = 192;
            this.label7.Text = "Начало";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label10.Location = new System.Drawing.Point(235, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 32);
            this.label10.TabIndex = 195;
            this.label10.Text = "Конец";
            // 
            // FinishMinComboBox
            // 
            this.FinishMinComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FinishMinComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FinishMinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FinishMinComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FinishMinComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.FinishMinComboBox.FormattingEnabled = true;
            this.FinishMinComboBox.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.FinishMinComboBox.Location = new System.Drawing.Point(324, 316);
            this.FinishMinComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.FinishMinComboBox.Name = "FinishMinComboBox";
            this.FinishMinComboBox.Size = new System.Drawing.Size(73, 35);
            this.FinishMinComboBox.TabIndex = 194;
            // 
            // FinishHourComboBox
            // 
            this.FinishHourComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FinishHourComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FinishHourComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FinishHourComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FinishHourComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.FinishHourComboBox.FormattingEnabled = true;
            this.FinishHourComboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.FinishHourComboBox.Location = new System.Drawing.Point(241, 316);
            this.FinishHourComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.FinishHourComboBox.Name = "FinishHourComboBox";
            this.FinishHourComboBox.Size = new System.Drawing.Size(73, 35);
            this.FinishHourComboBox.TabIndex = 193;
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(37, 62);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(778, 536);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 198;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // PlanningHallControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.FinishMinComboBox);
            this.Controls.Add(this.FinishHourComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.StartMinComboBox);
            this.Controls.Add(this.StartHoursComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ExpenseBtn);
            this.Controls.Add(this.materialDivider7);
            this.Controls.Add(this.ExpenseTextBox);
            this.Controls.Add(this.materialDivider5);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.materialDivider6);
            this.Controls.Add(this.count_adult_textBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.arrivalmonthCalendar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.person_phone_textBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.DeletePlHallBtn);
            this.Controls.Add(this.ChangePlHallBtn);
            this.Controls.Add(this.AddPlHallBtn);
            this.Controls.Add(this.HallGridView);
            this.Controls.Add(this.nameComboBox);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.person_name_TextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "PlanningHallControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.HallGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExpenseBtn;
        private MaterialSkin.Controls.MaterialDivider materialDivider7;
        private System.Windows.Forms.TextBox ExpenseTextBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider5;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider6;
        private System.Windows.Forms.TextBox count_adult_textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MonthCalendar arrivalmonthCalendar;
        private System.Windows.Forms.Label label6;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox person_phone_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button DeletePlHallBtn;
        private System.Windows.Forms.Button ChangePlHallBtn;
        private System.Windows.Forms.Button AddPlHallBtn;
        private System.Windows.Forms.DataGridView HallGridView;
        private System.Windows.Forms.ComboBox nameComboBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox person_name_TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox StartHoursComboBox;
        private System.Windows.Forms.ComboBox StartMinComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox FinishMinComboBox;
        private System.Windows.Forms.ComboBox FinishHourComboBox;
        private System.Windows.Forms.PictureBox preloader;
    }
}
