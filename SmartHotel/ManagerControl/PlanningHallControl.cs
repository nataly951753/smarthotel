﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class PlanningHallControl : UserControl
    {
        PlanningConferenceHall _plan_hall;
        public PlanningHallControl()
        {
            InitializeComponent();
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                nameComboBox.DataSource = db.ConferenceHall.Select(x => x.Name).ToList();
                StartHoursComboBox.SelectedIndex = 0;
                StartMinComboBox.SelectedIndex = 0;
                FinishHourComboBox.SelectedIndex = 0;
                FinishMinComboBox.SelectedIndex = 0;
                ShowHallDataGrid();
                HallGridView.Columns[5].Visible = false;
            }
        }
        
        //отображение в DataGrid
        private void ShowHallDataGrid()
        {
            HallGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningConferenceHall.ToList())
                {
                    HallGridView.Rows.Add();
                    HallGridView[0, i].Value = item.FullName;
                    HallGridView[1, i].Value = item.ContactNumber;
                    HallGridView[2, i].Value = item.ConferenceHall1.Name;
                    HallGridView[3, i].Value = item.TimeOfArrival.ToString();
                    HallGridView[4, i].Value = item.TimeOfEviction.ToString();
                    HallGridView[5, i].Value = item.Id;
                    i++;
                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            person_name_TextBox.Text = "";
            person_phone_textBox.Text = "";
            ExpenseTextBox.Text = "0";
            count_adult_textBox.Text = "";
            DescriptionTextBox.Text = "";
            StartHoursComboBox.SelectedIndex = 0;
            StartMinComboBox.SelectedIndex = 0;
            FinishHourComboBox.SelectedIndex = 0;
            FinishMinComboBox.SelectedIndex = 0;
        }

        //проверка заполненности текстовых полей
        private bool TextBoxesHaveText()
        {
            return (person_name_TextBox.Text != "" && person_phone_textBox.Text != "" && count_adult_textBox.Text != "");
        }
        //возвращает время начала с выбранной датой в календаре
        private DateTime GetStartTime()
        {
            DateTime calendar = arrivalmonthCalendar.SelectionRange.Start;
            return new DateTime(calendar.Year, calendar.Month, calendar.Day, int.Parse(StartHoursComboBox.Text), int.Parse(StartMinComboBox.Text), 0);
        }
        //возвращает время конца с выбранной датой в календаре

        private DateTime GetFinishTime()
        {
            DateTime calendar = arrivalmonthCalendar.SelectionRange.Start;
            return new DateTime(calendar.Year, calendar.Month, calendar.Day, int.Parse(FinishHourComboBox.Text), int.Parse(FinishMinComboBox.Text), 0);

        }
        //возвращает разницу времени в часах
        private double StartAndFinishTime()
        {
            DateTime start = GetStartTime();
            DateTime finish = GetFinishTime();
            return finish.Subtract(start).TotalHours;
        }
        //проверка даты и времени
        private double CanResorve(int id)
        {
            double hours = StartAndFinishTime();
            //проверка позже ли конец начала
            if (hours > 0)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    PlanningConferenceHall pCH;
                    DateTime start = GetStartTime();
                    DateTime finish = GetFinishTime();
                    //проверяем есть ли в бд на выбранный зал заказ , время НАЧАЛА которого попадает в выбранные рамки
                    pCH = db.PlanningConferenceHall.FirstOrDefault(x => ((x.ConferenceHall1.Name == nameComboBox.Text) && (x.TimeOfArrival.CompareTo(start) >= 0) && (x.TimeOfArrival.CompareTo(finish) < 0)));
                    if (pCH == null || pCH.Id == id)
                    {
                        //проверяем есть ли в бд на выбранный зал заказ , время КОНЦА которого попадает в выбранные рамки
                        pCH = db.PlanningConferenceHall.FirstOrDefault(x => ((x.ConferenceHall1.Name == nameComboBox.Text) && (x.TimeOfEviction.CompareTo(start) > 0) && (x.TimeOfEviction.CompareTo(finish) <= 0)));
                        if (pCH == null || pCH.Id == id)
                        {
                            //проверяем не попадёт ли заказ в уже выбранные временные рамки другого заказа
                            pCH = db.PlanningConferenceHall.FirstOrDefault(x => ((x.ConferenceHall1.Name == nameComboBox.Text) && (x.TimeOfArrival.CompareTo(start) < 0) && (x.TimeOfEviction.CompareTo(finish) > 0)));
                            if (pCH == null || pCH.Id == id)
                            {
                                return hours;
                            }
                            else
                            {
                                SmartHotel.MessageClass.MessageWaitHall(pCH);
                            }
                        }
                        else
                        {
                            SmartHotel.MessageClass.MessageWaitHall(pCH);
                        }

                    }
                    else
                    {
                        SmartHotel.MessageClass.MessageWaitHall(pCH);
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.MessageWrongTime();
            }
            return -1;
        }
        private double SetExpense(int id)
        {
            
            double time = CanResorve(id);
            //проверяем время
            if (time > 0)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    double cost = db.ConferenceHall.FirstOrDefault(x => x.Name == nameComboBox.Text).Cost;//стоимость зала почасово
                    cost *= time;//кол-во часов на стоимость
                    ExpenseTextBox.Text = cost.ToString();
                    return cost;
                }
            }
            ExpenseTextBox.Text = "0";
            return 0;
        }
        private void AddPlHallBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            if (TextBoxesHaveText())//если текстовые поля заполнены
            {
                double cost = SetExpense(-1);
                //проверяем даты и рассчитываем счёт
                if (cost > 0)
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        DateTime start = GetStartTime();
                        DateTime finish = GetFinishTime();
                        int number = db.ConferenceHall.FirstOrDefault(x => x.Name == nameComboBox.Text).Id;//номер зала
                        db.PlanningConferenceHall.Add(new PlanningConferenceHall
                        {
                            ConferenceHall = number,
                            ContactNumber = person_phone_textBox.Text,
                            FullName = person_name_TextBox.Text,
                            TimeOfArrival = start,
                            TimeOfEviction = finish,
                            NumberOfAdult = count_adult_textBox.Text,
                            Description = DescriptionTextBox.Text,
                            Expense = cost
                        });
                        db.SaveChanges();//сохраняем изменения бд      
                        ShowHallDataGrid();
                        SmartHotel.MessageClass.AddSuccessfully();
                        ClearTextBox();
                        _plan_hall = null;
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.Error();
            }
            preloader.Visible = false;
        }

        private void HallGridView_DoubleClick(object sender, EventArgs e)
        {
            if (HallGridView.RowCount > 0)
            {
                if (HallGridView.CurrentRow.Index != -1)
                {
                    ClearTextBox();
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        string id = HallGridView[5, HallGridView.CurrentRow.Index].Value.ToString();
                        _plan_hall = db.PlanningConferenceHall.FirstOrDefault(x => x.Id.ToString() == id);
                    }
                    person_name_TextBox.Text = _plan_hall.FullName;
                    person_phone_textBox.Text = _plan_hall.ContactNumber;
                    count_adult_textBox.Text = _plan_hall.NumberOfAdult;
                    DescriptionTextBox.Text = _plan_hall.Description;
                    nameComboBox.Text = _plan_hall.ConferenceHall1.Name;
                    arrivalmonthCalendar.SetDate(_plan_hall.TimeOfArrival);
                    ExpenseTextBox.Text = _plan_hall.Expense.ToString();
                    StartHoursComboBox.Text = _plan_hall.TimeOfArrival.Hour.ToString();
                    StartMinComboBox.Text = _plan_hall.TimeOfArrival.Minute.ToString();
                    FinishHourComboBox.Text = _plan_hall.TimeOfEviction.Hour.ToString();
                    FinishMinComboBox.Text = _plan_hall.TimeOfEviction.Minute.ToString();

                }
            }
        }

        private void ChangePlHallBtn_Click(object sender, EventArgs e)
        {
            if (_plan_hall != null)//если выбран из перечня 
            {
                if (TextBoxesHaveText())//если текстовые поля заполнены
                {
                    double _c = SetExpense(_plan_hall.Id);
                    if (_c > 0)//если текстовые поля заполнены
                    {
                        using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                        {
                            int hall_id = db.ConferenceHall.FirstOrDefault(x => x.Name == nameComboBox.Text).Id;
                            PlanningConferenceHall plan_new = db.PlanningConferenceHall.Find(_plan_hall.Id);
                            //заполняем структуру данных
                            plan_new.ConferenceHall = hall_id;
                            plan_new.FullName = person_name_TextBox.Text;
                            plan_new.ContactNumber = person_phone_textBox.Text;
                            plan_new.NumberOfAdult = count_adult_textBox.Text;
                            plan_new.Description = DescriptionTextBox.Text;
                            plan_new.Expense = Double.Parse(ExpenseTextBox.Text);
                            plan_new.TimeOfArrival = GetStartTime();
                            plan_new.TimeOfEviction = GetFinishTime();
                            //изменение
                            db.Entry(plan_new).State = EntityState.Modified;
                            db.SaveChanges();//сохранение
                            ShowHallDataGrid();//перерисовка DataGrid
                            SmartHotel.MessageClass.ChangeSuccessfully();
                            ClearTextBox();
                        }
                    }
                    _plan_hall = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void ExpenseBtn_Click(object sender, EventArgs e)
        {
            int id;
            if (_plan_hall == null) id = -1;
            else id = _plan_hall.Id;
            double _c = SetExpense(id);
            if (_c >= 0)
                ExpenseTextBox.Text = _c.ToString();
        }

        private void DeletePlHallBtn_Click(object sender, EventArgs e)
        {
            if (HallGridView.RowCount > 0)
            {

                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string number = HallGridView[5, HallGridView.CurrentRow.Index].Value.ToString();
                    //удаляем элемент, название которого соответствует выбранному
                    db.PlanningConferenceHall.Remove(db.PlanningConferenceHall.FirstOrDefault(x => x.Id.ToString() == number));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowHallDataGrid();//отображаем изменения в DataGrid
                    _plan_hall = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }
    }
}
