﻿namespace SmartHotel
{
    partial class ArrivalControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.evictionmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.arrivalmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.prepaid_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.person_phone_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DeleteArrivalBtn = new System.Windows.Forms.Button();
            this.ChangeArrivalBtn = new System.Windows.Forms.Button();
            this.AddArrivalBtn = new System.Windows.Forms.Button();
            this.ArrivalGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberComboBox = new System.Windows.Forms.ComboBox();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.person_name_TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.count_children_textBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.materialDivider6 = new MaterialSkin.Controls.MaterialDivider();
            this.count_adult_textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.materialDivider5 = new MaterialSkin.Controls.MaterialDivider();
            this.FoodPreferencesTextBox = new System.Windows.Forms.TextBox();
            this.materialDivider7 = new MaterialSkin.Controls.MaterialDivider();
            this.ExpenseTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.materialDivider8 = new MaterialSkin.Controls.MaterialDivider();
            this.ExtraPlaceTextBox = new System.Windows.Forms.TextBox();
            this.ExpenseBtn = new System.Windows.Forms.Button();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // evictionmonthCalendar
            // 
            this.evictionmonthCalendar.BackColor = System.Drawing.Color.DarkRed;
            this.evictionmonthCalendar.Location = new System.Drawing.Point(233, 187);
            this.evictionmonthCalendar.MaxSelectionCount = 1;
            this.evictionmonthCalendar.Name = "evictionmonthCalendar";
            this.evictionmonthCalendar.TabIndex = 134;
            // 
            // arrivalmonthCalendar
            // 
            this.arrivalmonthCalendar.Location = new System.Drawing.Point(57, 187);
            this.arrivalmonthCalendar.MaxSelectionCount = 1;
            this.arrivalmonthCalendar.Name = "arrivalmonthCalendar";
            this.arrivalmonthCalendar.TabIndex = 133;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(229, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 32);
            this.label4.TabIndex = 132;
            this.label4.Text = "Выезд";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(50, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 32);
            this.label6.TabIndex = 131;
            this.label6.Text = "Заезд";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(540, 137);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(255, 1);
            this.materialDivider2.TabIndex = 130;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // prepaid_textBox
            // 
            this.prepaid_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.prepaid_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.prepaid_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prepaid_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.prepaid_textBox.Location = new System.Drawing.Point(541, 106);
            this.prepaid_textBox.Name = "prepaid_textBox";
            this.prepaid_textBox.ReadOnly = true;
            this.prepaid_textBox.Size = new System.Drawing.Size(255, 32);
            this.prepaid_textBox.TabIndex = 128;
            this.prepaid_textBox.Text = "0";
            this.prepaid_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(409, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 32);
            this.label3.TabIndex = 129;
            this.label3.Text = "Аванс";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(541, 97);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(255, 1);
            this.materialDivider1.TabIndex = 127;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // person_phone_textBox
            // 
            this.person_phone_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.person_phone_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.person_phone_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.person_phone_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.person_phone_textBox.Location = new System.Drawing.Point(542, 66);
            this.person_phone_textBox.Name = "person_phone_textBox";
            this.person_phone_textBox.Size = new System.Drawing.Size(255, 32);
            this.person_phone_textBox.TabIndex = 125;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(408, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 32);
            this.label2.TabIndex = 126;
            this.label2.Text = "Телефон";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(51, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 32);
            this.label8.TabIndex = 124;
            this.label8.Text = "Номер";
            // 
            // DeleteArrivalBtn
            // 
            this.DeleteArrivalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeleteArrivalBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteArrivalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteArrivalBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteArrivalBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeleteArrivalBtn.Location = new System.Drawing.Point(352, 380);
            this.DeleteArrivalBtn.Name = "DeleteArrivalBtn";
            this.DeleteArrivalBtn.Size = new System.Drawing.Size(146, 44);
            this.DeleteArrivalBtn.TabIndex = 119;
            this.DeleteArrivalBtn.Text = "Удалить";
            this.DeleteArrivalBtn.UseVisualStyleBackColor = false;
            this.DeleteArrivalBtn.Click += new System.EventHandler(this.DeleteArrivalBtn_Click);
            // 
            // ChangeArrivalBtn
            // 
            this.ChangeArrivalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ChangeArrivalBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangeArrivalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeArrivalBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeArrivalBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ChangeArrivalBtn.Location = new System.Drawing.Point(504, 380);
            this.ChangeArrivalBtn.Name = "ChangeArrivalBtn";
            this.ChangeArrivalBtn.Size = new System.Drawing.Size(146, 44);
            this.ChangeArrivalBtn.TabIndex = 118;
            this.ChangeArrivalBtn.Text = "Изменить";
            this.ChangeArrivalBtn.UseVisualStyleBackColor = false;
            this.ChangeArrivalBtn.Click += new System.EventHandler(this.ChangeArrivalBtn_Click);
            // 
            // AddArrivalBtn
            // 
            this.AddArrivalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddArrivalBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddArrivalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddArrivalBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddArrivalBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddArrivalBtn.Location = new System.Drawing.Point(656, 380);
            this.AddArrivalBtn.Name = "AddArrivalBtn";
            this.AddArrivalBtn.Size = new System.Drawing.Size(146, 44);
            this.AddArrivalBtn.TabIndex = 117;
            this.AddArrivalBtn.Text = "Добавить";
            this.AddArrivalBtn.UseVisualStyleBackColor = false;
            this.AddArrivalBtn.Click += new System.EventHandler(this.AddArrivalBtn_Click);
            // 
            // ArrivalGridView
            // 
            this.ArrivalGridView.AllowUserToAddRows = false;
            this.ArrivalGridView.AllowUserToDeleteRows = false;
            this.ArrivalGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.ArrivalGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ArrivalGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ArrivalGridView.ColumnHeadersHeight = 30;
            this.ArrivalGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ArrivalGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ArrivalGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ArrivalGridView.Location = new System.Drawing.Point(56, 430);
            this.ArrivalGridView.MultiSelect = false;
            this.ArrivalGridView.Name = "ArrivalGridView";
            this.ArrivalGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ArrivalGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ArrivalGridView.RowHeadersVisible = false;
            this.ArrivalGridView.RowHeadersWidth = 65;
            this.ArrivalGridView.RowTemplate.Height = 30;
            this.ArrivalGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ArrivalGridView.Size = new System.Drawing.Size(746, 160);
            this.ArrivalGridView.TabIndex = 123;
            this.ArrivalGridView.DoubleClick += new System.EventHandler(this.ArrivalGridView_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ФИО";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 174;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Телефон";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Номер";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Дата заезда";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 170;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Дата выезда";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Номер заказа";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // numberComboBox
            // 
            this.numberComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numberComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.numberComboBox.FormattingEnabled = true;
            this.numberComboBox.Location = new System.Drawing.Point(154, 103);
            this.numberComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.numberComboBox.Name = "numberComboBox";
            this.numberComboBox.Size = new System.Drawing.Size(244, 35);
            this.numberComboBox.TabIndex = 115;
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(154, 97);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(243, 1);
            this.materialDivider4.TabIndex = 122;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // person_name_TextBox
            // 
            this.person_name_TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.person_name_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.person_name_TextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.person_name_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.person_name_TextBox.Location = new System.Drawing.Point(155, 66);
            this.person_name_TextBox.Name = "person_name_TextBox";
            this.person_name_TextBox.Size = new System.Drawing.Size(243, 32);
            this.person_name_TextBox.TabIndex = 116;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(51, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 32);
            this.label5.TabIndex = 121;
            this.label5.Text = "ФИО";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(329, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 43);
            this.label1.TabIndex = 120;
            this.label1.Text = "Заселение";
            // 
            // materialDivider3
            // 
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(723, 178);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(74, 1);
            this.materialDivider3.TabIndex = 140;
            this.materialDivider3.Text = "materialDivider3";
            // 
            // count_children_textBox
            // 
            this.count_children_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_children_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_children_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_children_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_children_textBox.Location = new System.Drawing.Point(723, 146);
            this.count_children_textBox.Name = "count_children_textBox";
            this.count_children_textBox.Size = new System.Drawing.Size(74, 32);
            this.count_children_textBox.TabIndex = 136;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label7.Location = new System.Drawing.Point(625, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 32);
            this.label7.TabIndex = 139;
            this.label7.Text = "Детей";
            // 
            // materialDivider6
            // 
            this.materialDivider6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider6.Depth = 0;
            this.materialDivider6.Location = new System.Drawing.Point(546, 178);
            this.materialDivider6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider6.Name = "materialDivider6";
            this.materialDivider6.Size = new System.Drawing.Size(73, 1);
            this.materialDivider6.TabIndex = 138;
            this.materialDivider6.Text = "materialDivider6";
            // 
            // count_adult_textBox
            // 
            this.count_adult_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_adult_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_adult_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_adult_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_adult_textBox.Location = new System.Drawing.Point(546, 146);
            this.count_adult_textBox.Name = "count_adult_textBox";
            this.count_adult_textBox.Size = new System.Drawing.Size(73, 32);
            this.count_adult_textBox.TabIndex = 135;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label9.Location = new System.Drawing.Point(409, 149);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 32);
            this.label9.TabIndex = 137;
            this.label9.Text = "Взрослых";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label10.Location = new System.Drawing.Point(408, 228);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(334, 32);
            this.label10.TabIndex = 141;
            this.label10.Text = "Вкусовые предпочтения";
            // 
            // materialDivider5
            // 
            this.materialDivider5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider5.Depth = 0;
            this.materialDivider5.Location = new System.Drawing.Point(414, 295);
            this.materialDivider5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider5.Name = "materialDivider5";
            this.materialDivider5.Size = new System.Drawing.Size(383, 1);
            this.materialDivider5.TabIndex = 143;
            this.materialDivider5.Text = "materialDivider5";
            // 
            // FoodPreferencesTextBox
            // 
            this.FoodPreferencesTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FoodPreferencesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FoodPreferencesTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FoodPreferencesTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.FoodPreferencesTextBox.Location = new System.Drawing.Point(414, 263);
            this.FoodPreferencesTextBox.Name = "FoodPreferencesTextBox";
            this.FoodPreferencesTextBox.Size = new System.Drawing.Size(383, 32);
            this.FoodPreferencesTextBox.TabIndex = 142;
            // 
            // materialDivider7
            // 
            this.materialDivider7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider7.Depth = 0;
            this.materialDivider7.Location = new System.Drawing.Point(557, 341);
            this.materialDivider7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider7.Name = "materialDivider7";
            this.materialDivider7.Size = new System.Drawing.Size(240, 1);
            this.materialDivider7.TabIndex = 146;
            this.materialDivider7.Text = "materialDivider7";
            // 
            // ExpenseTextBox
            // 
            this.ExpenseTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ExpenseTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ExpenseTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExpenseTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExpenseTextBox.Location = new System.Drawing.Point(557, 309);
            this.ExpenseTextBox.Name = "ExpenseTextBox";
            this.ExpenseTextBox.Size = new System.Drawing.Size(240, 32);
            this.ExpenseTextBox.TabIndex = 145;
            this.ExpenseTextBox.Text = "0";
            this.ExpenseTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label12.Location = new System.Drawing.Point(408, 189);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(154, 32);
            this.label12.TabIndex = 148;
            this.label12.Text = "Доп. место";
            // 
            // materialDivider8
            // 
            this.materialDivider8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider8.Depth = 0;
            this.materialDivider8.Location = new System.Drawing.Point(557, 222);
            this.materialDivider8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider8.Name = "materialDivider8";
            this.materialDivider8.Size = new System.Drawing.Size(239, 1);
            this.materialDivider8.TabIndex = 150;
            this.materialDivider8.Text = "materialDivider8";
            // 
            // ExtraPlaceTextBox
            // 
            this.ExtraPlaceTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ExtraPlaceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ExtraPlaceTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExtraPlaceTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExtraPlaceTextBox.Location = new System.Drawing.Point(558, 191);
            this.ExtraPlaceTextBox.Name = "ExtraPlaceTextBox";
            this.ExtraPlaceTextBox.Size = new System.Drawing.Size(239, 32);
            this.ExtraPlaceTextBox.TabIndex = 149;
            this.ExtraPlaceTextBox.Text = "0";
            this.ExtraPlaceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ExpenseBtn
            // 
            this.ExpenseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExpenseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExpenseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExpenseBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExpenseBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ExpenseBtn.Location = new System.Drawing.Point(414, 302);
            this.ExpenseBtn.Name = "ExpenseBtn";
            this.ExpenseBtn.Size = new System.Drawing.Size(141, 44);
            this.ExpenseBtn.TabIndex = 151;
            this.ExpenseBtn.Text = "Счёт";
            this.ExpenseBtn.UseVisualStyleBackColor = false;
            this.ExpenseBtn.Click += new System.EventHandler(this.ExpenseBtn_Click);
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(44, 56);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(758, 534);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 153;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // ArrivalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.ExpenseBtn);
            this.Controls.Add(this.materialDivider8);
            this.Controls.Add(this.ExtraPlaceTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.materialDivider7);
            this.Controls.Add(this.ExpenseTextBox);
            this.Controls.Add(this.materialDivider5);
            this.Controls.Add(this.FoodPreferencesTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.materialDivider3);
            this.Controls.Add(this.count_children_textBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.materialDivider6);
            this.Controls.Add(this.count_adult_textBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.evictionmonthCalendar);
            this.Controls.Add(this.arrivalmonthCalendar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.prepaid_textBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.person_phone_textBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.DeleteArrivalBtn);
            this.Controls.Add(this.ChangeArrivalBtn);
            this.Controls.Add(this.AddArrivalBtn);
            this.Controls.Add(this.ArrivalGridView);
            this.Controls.Add(this.numberComboBox);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.person_name_TextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "ArrivalControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.ArrivalGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar evictionmonthCalendar;
        private System.Windows.Forms.MonthCalendar arrivalmonthCalendar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox prepaid_textBox;
        private System.Windows.Forms.Label label3;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox person_phone_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button DeleteArrivalBtn;
        private System.Windows.Forms.Button ChangeArrivalBtn;
        private System.Windows.Forms.Button AddArrivalBtn;
        private System.Windows.Forms.DataGridView ArrivalGridView;
        private System.Windows.Forms.ComboBox numberComboBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox person_name_TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private System.Windows.Forms.TextBox count_children_textBox;
        private System.Windows.Forms.Label label7;
        private MaterialSkin.Controls.MaterialDivider materialDivider6;
        private System.Windows.Forms.TextBox count_adult_textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private MaterialSkin.Controls.MaterialDivider materialDivider5;
        private System.Windows.Forms.TextBox FoodPreferencesTextBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider7;
        private System.Windows.Forms.TextBox ExpenseTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.Label label12;
        private MaterialSkin.Controls.MaterialDivider materialDivider8;
        private System.Windows.Forms.TextBox ExtraPlaceTextBox;
        private System.Windows.Forms.Button ExpenseBtn;
        private System.Windows.Forms.PictureBox preloader;
    }
}
