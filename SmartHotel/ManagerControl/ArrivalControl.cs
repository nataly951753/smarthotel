﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class ArrivalControl : UserControl
    {
        PlanningRooms _plan_rooms;

        public ArrivalControl()
        {
            InitializeComponent();
            ArrivalGridView.Columns[5].Visible = false;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                numberComboBox.DataSource = db.Rooms.Select(x => x.Number).ToList();
            }
            ShowForm();
        }
        //заполнение данными из Графика
        public void SetQuick(string num, DateTime timeStart)
        {
            numberComboBox.Text = num;
            arrivalmonthCalendar.SetDate(timeStart);
        }
        //передача данных из бронирования
        public void SetReservation(PlanningRooms pl)
        {
            if (pl != null)
            {
                _plan_rooms = pl;
                person_name_TextBox.Text = _plan_rooms.FullName;
                person_phone_textBox.Text = _plan_rooms.ContactNumber;
                prepaid_textBox.Text = _plan_rooms.PrepaidExpense.ToString();
                numberComboBox.Text = _plan_rooms.Rooms.Number;
                ExtraPlaceTextBox.Text = "0";
                ExpenseTextBox.Text = "0";
                count_adult_textBox.Text = "";
                count_children_textBox.Text = "";
                FoodPreferencesTextBox.Text = "";
                arrivalmonthCalendar.SetDate(_plan_rooms.DateOfArrival);
                evictionmonthCalendar.SetDate(_plan_rooms.DateOfEviction);
            }
            else
                ClearTextBox();

        }
        public void ShowForm()
        {
                ShowArrivalDataGrid();            
        }
        //отображение в DataGrid
        private void ShowArrivalDataGrid()
        {
            ArrivalGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningRooms.ToList())
                {
                    if (item.Arrival)//флаг заселения
                    {
                        ArrivalGridView.Rows.Add();
                        ArrivalGridView[0, i].Value = item.FullName;
                        ArrivalGridView[1, i].Value = item.ContactNumber;
                        ArrivalGridView[2, i].Value = item.Rooms.Number;
                        ArrivalGridView[3, i].Value = item.DateOfArrival.ToShortDateString();
                        ArrivalGridView[4, i].Value = item.DateOfEviction.ToShortDateString();
                        ArrivalGridView[5, i].Value = item.Id;
                        i++;
                    }
                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            person_name_TextBox.Text = "";
            person_phone_textBox.Text = "";
            prepaid_textBox.Text = "0";
            ExtraPlaceTextBox.Text = "0";
            ExpenseTextBox.Text = "0";
            count_adult_textBox.Text = "";
            count_children_textBox.Text = "";
            FoodPreferencesTextBox.Text = "";
        }

        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (person_name_TextBox.Text != "" && person_phone_textBox.Text != "" && prepaid_textBox.Text != "" && count_adult_textBox.Text != "" && count_children_textBox.Text != "")
            {
                double cost;
                try
                {
                    cost = Double.Parse(ExtraPlaceTextBox.Text);
                    return cost;
                }
                catch
                {
                    SmartHotel.MessageClass.MessageCost();
                    return -1;
                }
            }
            return -1;
        }
        private bool RightDate(int id)
        {
            //проверка позже ли выезд въезда
            if (evictionmonthCalendar.SelectionRange.Start.CompareTo(arrivalmonthCalendar.SelectionRange.Start) > 0)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    PlanningRooms p;
                    //проверяем есть ли в бд на выбранный номер заказ , дата ВЪЕЗДА которого попадает в выбранные рамки
                    p = db.PlanningRooms.FirstOrDefault(x => ((x.Rooms.Number == numberComboBox.Text) && (x.DateOfArrival.CompareTo(arrivalmonthCalendar.SelectionRange.Start) >= 0) && (x.DateOfArrival.CompareTo(evictionmonthCalendar.SelectionRange.Start) < 0)));
                    if (p == null || p.Id == id)
                    {
                        //проверяем есть ли в бд на выбранный номер заказ , дата ВЫЕЗДА которого попадает в выбранные рамки
                        p = db.PlanningRooms.FirstOrDefault(x => ((x.Rooms.Number == numberComboBox.Text) && (x.DateOfEviction.CompareTo(arrivalmonthCalendar.SelectionRange.Start) > 0) && (x.DateOfEviction.CompareTo(evictionmonthCalendar.SelectionRange.Start) <= 0)));
                        if (p == null || p.Id == id)
                        {
                            //проверяем не попадёт ли заказ в уже выбранные временные рамки другого заказа
                            p = db.PlanningRooms.FirstOrDefault(x => ((x.Rooms.Number == numberComboBox.Text) && (x.DateOfArrival.CompareTo(arrivalmonthCalendar.SelectionRange.Start) < 0) && (x.DateOfEviction.CompareTo(evictionmonthCalendar.SelectionRange.Start) > 0)));
                            if (p == null || p.Id == id)
                            {
                                return true;
                            }
                            else
                            {
                                SmartHotel.MessageClass.MessageWaitNumber(p);
                            }
                        }
                        else
                        {
                            SmartHotel.MessageClass.MessageWaitNumber(p);
                        }
                    }
                    else
                    {
                        SmartHotel.MessageClass.MessageWaitNumber(p);
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.MessageWrongDataNumber();
            }
            return false;
        }
        private bool SetExpense(double _c)
        {
            int id;
            if (_plan_rooms == null) id = -1;
            else id = _plan_rooms.Id;
            //проверяем даты
            if (RightDate(id))
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    double cost = db.Rooms.FirstOrDefault(x => x.Number == numberComboBox.Text).Cost;//стоимость номера в сутки
                    int time = evictionmonthCalendar.SelectionRange.Start.Subtract(arrivalmonthCalendar.SelectionRange.Start).Days;
                    cost *= time;//кол-во дней на стоимость
                    _c *= time;
                    cost += _c;//стоимость дополнительного места
                    cost -= Double.Parse(prepaid_textBox.Text);//аванс
                    ExpenseTextBox.Text = cost.ToString();
                }
                return true;
            }
            ExpenseTextBox.Text = "0";
            return false;
        }
        private void AddArrivalBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            double _c = TextBoxesHaveText();//дополнительное место
            if (_c >= 0)//если текстовые поля заполнены
            {
                //проверяем даты и рассчитываем счёт
                if (SetExpense(_c))
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        int number = db.Rooms.FirstOrDefault(x => x.Number == numberComboBox.Text).Id;//номер комнаты
                        if (_plan_rooms == null)//новый клиент
                            db.PlanningRooms.Add(new PlanningRooms { Number = number, ContactNumber = person_phone_textBox.Text, FullName = person_name_TextBox.Text, PrepaidExpense = Double.Parse(prepaid_textBox.Text), Arrival = true, DateOfArrival = arrivalmonthCalendar.SelectionRange.Start, DateOfEviction = evictionmonthCalendar.SelectionRange.Start, NumberOfAdult = count_adult_textBox.Text, NumberOfChildren = count_children_textBox.Text, FoodPreferences = FoodPreferencesTextBox.Text,ExtraSeat=_c });
                        else//бронь
                        {
                            int id = _plan_rooms.Id;
                            _plan_rooms = db.PlanningRooms.FirstOrDefault(x => x.Id == id);
                            _plan_rooms.Number = number;
                            _plan_rooms.FullName = person_name_TextBox.Text;
                            _plan_rooms.ContactNumber = person_phone_textBox.Text;
                            _plan_rooms.Arrival = true;
                            _plan_rooms.NumberOfAdult = count_adult_textBox.Text;
                            _plan_rooms.NumberOfChildren = count_children_textBox.Text;
                            _plan_rooms.FoodPreferences = FoodPreferencesTextBox.Text;
                            _plan_rooms.PrepaidExpense = Double.Parse(prepaid_textBox.Text);
                            _plan_rooms.DateOfArrival = arrivalmonthCalendar.SelectionRange.Start;
                            _plan_rooms.DateOfEviction = evictionmonthCalendar.SelectionRange.Start;
                            _plan_rooms.ExtraSeat = _c;

                            //изменение
                            db.Entry(_plan_rooms).State = EntityState.Modified;
                        }
                        db.SaveChanges();//сохраняем изменения бд      
                        ShowArrivalDataGrid();
                        SmartHotel.MessageClass.AddSuccessfully();
                        ClearTextBox();
                        _plan_rooms = null;
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.Error();
            }
            preloader.Visible = false;
        }

        private void ChangeArrivalBtn_Click(object sender, EventArgs e)
        {
            if (_plan_rooms != null && _plan_rooms.Arrival)//если выбран из перечня заселённый
            {
                double _c = TextBoxesHaveText();
                if (_c >= 0)//если текстовые поля заполнены
                {
                    //проверяем даты
                    if (SetExpense(_c))
                    {
                        using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                        {
                            int number = db.Rooms.FirstOrDefault(x => x.Number == numberComboBox.Text).Id;
                            PlanningRooms plan_new = db.PlanningRooms.Find(_plan_rooms.Id);
                            //заполняем структуру данных
                            plan_new.Number = number;
                            plan_new.FullName = person_name_TextBox.Text;
                            plan_new.ContactNumber = person_phone_textBox.Text;
                            plan_new.Arrival = true;
                            plan_new.NumberOfAdult = count_adult_textBox.Text;
                            plan_new.NumberOfChildren = count_children_textBox.Text;
                            plan_new.FoodPreferences = FoodPreferencesTextBox.Text;
                            plan_new.PrepaidExpense = Double.Parse(prepaid_textBox.Text);
                            plan_new.DateOfArrival = arrivalmonthCalendar.SelectionRange.Start;
                            plan_new.DateOfEviction = evictionmonthCalendar.SelectionRange.Start;
                            plan_new.ExtraSeat = _c;
                            //изменение
                            db.Entry(plan_new).State = EntityState.Modified;
                            db.SaveChanges();//сохранение
                            ShowArrivalDataGrid();//перерисовка DataGrid
                            SmartHotel.MessageClass.ChangeSuccessfully();
                            ClearTextBox();
                        }
                    }
                    _plan_rooms = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteArrivalBtn_Click(object sender, EventArgs e)
        {
            if (ArrivalGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string number = ArrivalGridView[5, ArrivalGridView.CurrentRow.Index].Value.ToString();
                    //удаляем элемент, название которого соответствует выбранному
                    db.PlanningRooms.Remove(db.PlanningRooms.FirstOrDefault(x => x.Id.ToString() == number));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowArrivalDataGrid();//отображаем изменения в DataGrid
                    _plan_rooms = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }

        private void printBtn_Click(object sender, EventArgs e)
        {
            
        }

        private void ArrivalGridView_DoubleClick(object sender, EventArgs e)
        {
            if (ArrivalGridView.RowCount > 0)
            {
                if (ArrivalGridView.CurrentRow.Index != -1)
                {
                    ClearTextBox();
                    string id = ArrivalGridView[5, ArrivalGridView.CurrentRow.Index].Value.ToString();

                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        _plan_rooms = db.PlanningRooms.FirstOrDefault(x => x.Id.ToString() == id);
                        person_name_TextBox.Text = _plan_rooms.FullName;
                        person_phone_textBox.Text = _plan_rooms.ContactNumber;
                        prepaid_textBox.Text = _plan_rooms.PrepaidExpense.ToString();
                        count_adult_textBox.Text = _plan_rooms.NumberOfAdult;
                        count_children_textBox.Text = _plan_rooms.NumberOfChildren;
                        FoodPreferencesTextBox.Text = _plan_rooms.FoodPreferences;
                        numberComboBox.Text = _plan_rooms.Rooms.Number;
                        ExtraPlaceTextBox.Text = _plan_rooms.ExtraSeat.ToString();
                        arrivalmonthCalendar.SetDate(_plan_rooms.DateOfArrival);
                        evictionmonthCalendar.SetDate(_plan_rooms.DateOfEviction);
                    }
                        SetExpense(TextBoxesHaveText());
                    
                }
            }
        }

        private void ExpenseBtn_Click(object sender, EventArgs e)
        {
            double _c = TextBoxesHaveText();
            if (_c >= 0)
            SetExpense(_c);
        }
    }
}
