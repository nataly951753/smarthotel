﻿namespace SmartHotel.ManagerControl
{
    partial class ServiceControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label7 = new System.Windows.Forms.Label();
            this.CleaningMinComboBox = new System.Windows.Forms.ComboBox();
            this.CleaningHoursComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AddCleaningBtn = new System.Windows.Forms.Button();
            this.cleaningmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AddRepairsBtn = new System.Windows.Forms.Button();
            this.numberCleaningComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.repairsTimeMinComboBox = new System.Windows.Forms.ComboBox();
            this.repairsTimeHourComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.repairsmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numberRepairsComboBox = new System.Windows.Forms.ComboBox();
            this.materialDivider5 = new MaterialSkin.Controls.MaterialDivider();
            this.DescriptionCleanTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.descriptRepairsTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label7.Location = new System.Drawing.Point(441, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 32);
            this.label7.TabIndex = 226;
            this.label7.Text = "Ремонт";
            // 
            // CleaningMinComboBox
            // 
            this.CleaningMinComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CleaningMinComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CleaningMinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CleaningMinComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CleaningMinComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.CleaningMinComboBox.FormattingEnabled = true;
            this.CleaningMinComboBox.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.CleaningMinComboBox.Location = new System.Drawing.Point(318, 304);
            this.CleaningMinComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.CleaningMinComboBox.Name = "CleaningMinComboBox";
            this.CleaningMinComboBox.Size = new System.Drawing.Size(73, 35);
            this.CleaningMinComboBox.TabIndex = 225;
            // 
            // CleaningHoursComboBox
            // 
            this.CleaningHoursComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CleaningHoursComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CleaningHoursComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CleaningHoursComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CleaningHoursComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.CleaningHoursComboBox.FormattingEnabled = true;
            this.CleaningHoursComboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.CleaningHoursComboBox.Location = new System.Drawing.Point(235, 304);
            this.CleaningHoursComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.CleaningHoursComboBox.Name = "CleaningHoursComboBox";
            this.CleaningHoursComboBox.Size = new System.Drawing.Size(73, 35);
            this.CleaningHoursComboBox.TabIndex = 224;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(232, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 32);
            this.label3.TabIndex = 222;
            this.label3.Text = "Время";
            // 
            // AddCleaningBtn
            // 
            this.AddCleaningBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddCleaningBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddCleaningBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddCleaningBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddCleaningBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddCleaningBtn.Location = new System.Drawing.Point(56, 533);
            this.AddCleaningBtn.Name = "AddCleaningBtn";
            this.AddCleaningBtn.Size = new System.Drawing.Size(206, 44);
            this.AddCleaningBtn.TabIndex = 220;
            this.AddCleaningBtn.Text = "Назначить уборку";
            this.AddCleaningBtn.UseVisualStyleBackColor = false;
            this.AddCleaningBtn.Click += new System.EventHandler(this.AddCleaningBtn_Click);
            // 
            // cleaningmonthCalendar
            // 
            this.cleaningmonthCalendar.Location = new System.Drawing.Point(56, 227);
            this.cleaningmonthCalendar.MaxSelectionCount = 1;
            this.cleaningmonthCalendar.Name = "cleaningmonthCalendar";
            this.cleaningmonthCalendar.TabIndex = 212;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(49, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 32);
            this.label6.TabIndex = 211;
            this.label6.Text = "Дата";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(48, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 32);
            this.label8.TabIndex = 207;
            this.label8.Text = "Номер";
            // 
            // AddRepairsBtn
            // 
            this.AddRepairsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddRepairsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddRepairsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddRepairsBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddRepairsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddRepairsBtn.Location = new System.Drawing.Point(451, 533);
            this.AddRepairsBtn.Name = "AddRepairsBtn";
            this.AddRepairsBtn.Size = new System.Drawing.Size(224, 44);
            this.AddRepairsBtn.TabIndex = 200;
            this.AddRepairsBtn.Text = "Назначить ремонт";
            this.AddRepairsBtn.UseVisualStyleBackColor = false;
            this.AddRepairsBtn.Click += new System.EventHandler(this.AddRepairsBtn_Click);
            // 
            // numberCleaningComboBox
            // 
            this.numberCleaningComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberCleaningComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numberCleaningComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberCleaningComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberCleaningComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.numberCleaningComboBox.FormattingEnabled = true;
            this.numberCleaningComboBox.Location = new System.Drawing.Point(167, 131);
            this.numberCleaningComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.numberCleaningComboBox.Name = "numberCleaningComboBox";
            this.numberCleaningComboBox.Size = new System.Drawing.Size(244, 35);
            this.numberCleaningComboBox.Sorted = true;
            this.numberCleaningComboBox.TabIndex = 198;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(48, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 32);
            this.label5.TabIndex = 204;
            this.label5.Text = "Уборка";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(349, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 43);
            this.label1.TabIndex = 203;
            this.label1.Text = "Сервис";
            // 
            // repairsTimeMinComboBox
            // 
            this.repairsTimeMinComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.repairsTimeMinComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.repairsTimeMinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.repairsTimeMinComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.repairsTimeMinComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.repairsTimeMinComboBox.FormattingEnabled = true;
            this.repairsTimeMinComboBox.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.repairsTimeMinComboBox.Location = new System.Drawing.Point(713, 304);
            this.repairsTimeMinComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.repairsTimeMinComboBox.Name = "repairsTimeMinComboBox";
            this.repairsTimeMinComboBox.Size = new System.Drawing.Size(73, 35);
            this.repairsTimeMinComboBox.TabIndex = 233;
            // 
            // repairsTimeHourComboBox
            // 
            this.repairsTimeHourComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.repairsTimeHourComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.repairsTimeHourComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.repairsTimeHourComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.repairsTimeHourComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.repairsTimeHourComboBox.FormattingEnabled = true;
            this.repairsTimeHourComboBox.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.repairsTimeHourComboBox.Location = new System.Drawing.Point(630, 304);
            this.repairsTimeHourComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.repairsTimeHourComboBox.Name = "repairsTimeHourComboBox";
            this.repairsTimeHourComboBox.Size = new System.Drawing.Size(73, 35);
            this.repairsTimeHourComboBox.TabIndex = 232;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(627, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 32);
            this.label2.TabIndex = 231;
            this.label2.Text = "Время";
            // 
            // repairsmonthCalendar
            // 
            this.repairsmonthCalendar.Location = new System.Drawing.Point(451, 227);
            this.repairsmonthCalendar.MaxSelectionCount = 1;
            this.repairsmonthCalendar.Name = "repairsmonthCalendar";
            this.repairsmonthCalendar.TabIndex = 230;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(444, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 32);
            this.label4.TabIndex = 229;
            this.label4.Text = "Дата";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label9.Location = new System.Drawing.Point(443, 134);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 32);
            this.label9.TabIndex = 228;
            this.label9.Text = "Номер";
            // 
            // numberRepairsComboBox
            // 
            this.numberRepairsComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberRepairsComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numberRepairsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberRepairsComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberRepairsComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.numberRepairsComboBox.FormattingEnabled = true;
            this.numberRepairsComboBox.Location = new System.Drawing.Point(562, 131);
            this.numberRepairsComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.numberRepairsComboBox.Name = "numberRepairsComboBox";
            this.numberRepairsComboBox.Size = new System.Drawing.Size(244, 35);
            this.numberRepairsComboBox.Sorted = true;
            this.numberRepairsComboBox.TabIndex = 227;
            // 
            // materialDivider5
            // 
            this.materialDivider5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider5.Depth = 0;
            this.materialDivider5.Location = new System.Drawing.Point(56, 526);
            this.materialDivider5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider5.Name = "materialDivider5";
            this.materialDivider5.Size = new System.Drawing.Size(355, 1);
            this.materialDivider5.TabIndex = 236;
            this.materialDivider5.Text = "materialDivider5";
            // 
            // DescriptionCleanTextBox
            // 
            this.DescriptionCleanTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DescriptionCleanTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DescriptionCleanTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DescriptionCleanTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DescriptionCleanTextBox.Location = new System.Drawing.Point(56, 431);
            this.DescriptionCleanTextBox.Multiline = true;
            this.DescriptionCleanTextBox.Name = "DescriptionCleanTextBox";
            this.DescriptionCleanTextBox.ReadOnly = true;
            this.DescriptionCleanTextBox.Size = new System.Drawing.Size(355, 96);
            this.DescriptionCleanTextBox.TabIndex = 235;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label10.Location = new System.Drawing.Point(45, 396);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(180, 32);
            this.label10.TabIndex = 234;
            this.label10.Text = "Примечание";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(451, 526);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(355, 1);
            this.materialDivider1.TabIndex = 239;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // descriptRepairsTextBox
            // 
            this.descriptRepairsTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.descriptRepairsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descriptRepairsTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptRepairsTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.descriptRepairsTextBox.Location = new System.Drawing.Point(451, 431);
            this.descriptRepairsTextBox.Multiline = true;
            this.descriptRepairsTextBox.Name = "descriptRepairsTextBox";
            this.descriptRepairsTextBox.ReadOnly = true;
            this.descriptRepairsTextBox.Size = new System.Drawing.Size(355, 96);
            this.descriptRepairsTextBox.TabIndex = 238;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label11.Location = new System.Drawing.Point(440, 396);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(180, 32);
            this.label11.TabIndex = 237;
            this.label11.Text = "Примечание";
            // 
            // ServiceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.descriptRepairsTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.materialDivider5);
            this.Controls.Add(this.DescriptionCleanTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.repairsTimeMinComboBox);
            this.Controls.Add(this.repairsTimeHourComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.repairsmonthCalendar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numberRepairsComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CleaningMinComboBox);
            this.Controls.Add(this.CleaningHoursComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AddCleaningBtn);
            this.Controls.Add(this.cleaningmonthCalendar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.AddRepairsBtn);
            this.Controls.Add(this.numberCleaningComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "ServiceControl";
            this.Size = new System.Drawing.Size(852, 621);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox CleaningMinComboBox;
        private System.Windows.Forms.ComboBox CleaningHoursComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button AddCleaningBtn;
        private System.Windows.Forms.MonthCalendar cleaningmonthCalendar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button AddRepairsBtn;
        private System.Windows.Forms.ComboBox numberCleaningComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox repairsTimeMinComboBox;
        private System.Windows.Forms.ComboBox repairsTimeHourComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MonthCalendar repairsmonthCalendar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox numberRepairsComboBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider5;
        private System.Windows.Forms.TextBox DescriptionCleanTextBox;
        private System.Windows.Forms.Label label10;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox descriptRepairsTextBox;
        private System.Windows.Forms.Label label11;
    }
}
