﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;
using SmartHotel.ManagerControl;

namespace SmartHotel
{
    public partial class GraphControl : UserControl
    {
        ReservationControl _controlR;
        ArrivalControl _controlA;
        EvictionControl _controlE;
        Color reservationColor = Color.LightYellow;
        Color arrivalColor = Color.ForestGreen;

        public GraphControl()
        {
            InitializeComponent();
            ShowDataGrid();//отображение графика в виде таблицы
            labelMonth.Text = String.Format("{0:MMMM}", DateTime.Now);//текущий месяц в заголовке
        }
        //(передаю ссылку на элемент из главной формы)

        public void SetControl(ReservationControl r, ArrivalControl a, EvictionControl e)
        {
            _controlR = r;
            _controlA = a;
            _controlE = e;
        }
        public void ShowForm()
        {
            ShowDataGrid();//отображение графика в виде таблицы
        }
        private void SetCell(int g, PlanningRooms plan, int i)
        {

            if (plan.Arrival)
                GraphGridView[g, i].Style.BackColor = arrivalColor;
            else
                GraphGridView[g, i].Style.BackColor = reservationColor;
        }
        private void ShowDataGrid()
        {
            //очистка
            GraphGridView.Rows.Clear();
            GraphGridView.Columns.Clear();
            int i = 0;//строки
            GraphGridView.Columns.Add("colNumber", "Номер");
            //заголовки колонок по дням текущего месяца
            for (int k = 1; k <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); k++)
            {
                GraphGridView.Columns.Add("col" + k.ToString(), k.ToString());
            }
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.Rooms.ToList())
                {
                    GraphGridView.Rows.Add();
                    GraphGridView[0, i].Value = item.Number;
                    //создаем пустые ячейки по кол-ву дней месяца
                    for (int k = 1; k <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); k++)
                    {
                        GraphGridView[k, i].Value = "";
                    }
                    //проверка на заказы на текущей номер(первый столбец строки) в текущем месяце
                    foreach (var plan in db.PlanningRooms.ToList())
                    {
                        //если номер из коллекции - номер в столбце
                        if (plan.Rooms.Number.ToString() == GraphGridView[0, i].Value.ToString())
                        {
                            //год заказа соответствует текущему
                            if (plan.DateOfArrival.Year == DateTime.Now.Year && plan.DateOfEviction.Year == DateTime.Now.Year)
                            {
                                //если месяц заказа - текущий месяц
                                if (plan.DateOfArrival.Month == DateTime.Now.Month && plan.DateOfEviction.Month == DateTime.Now.Month)
                                {
                                    //день выезда не закрашивается
                                    for (int g = plan.DateOfArrival.Day; g < plan.DateOfEviction.Day; g++)
                                    {
                                        if (g == plan.DateOfArrival.Day)
                                            GraphGridView[g, i].Value = plan.FullName;
                                        SetCell(g, plan, i);
                                    }

                                }
                                //если заезд раньше текущего месяца
                                else if (plan.DateOfArrival.Month < DateTime.Now.Month && plan.DateOfEviction.Month == DateTime.Now.Month)
                                {
                                    //день выезда не закрашивается

                                    for (int g = 1; g < plan.DateOfEviction.Day; g++)
                                    {
                                        if (g == 1)
                                            GraphGridView[g, i].Value = plan.FullName;
                                        SetCell(g, plan, i);
                                    }
                                }
                                //если выезд позже текущего месяца
                                else if (plan.DateOfArrival.Month == DateTime.Now.Month && plan.DateOfEviction.Month > DateTime.Now.Month)
                                {
                                    for (int g = plan.DateOfArrival.Day; g <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); g++)
                                    {
                                        if (g == plan.DateOfArrival.Day)
                                            GraphGridView[g, i].Value = plan.FullName;
                                        SetCell(g, plan, i);
                                    }
                                }
                                //если заезд раньше и выезд позже текущего месяца
                                else if (plan.DateOfArrival.Month < DateTime.Now.Month && plan.DateOfEviction.Month > DateTime.Now.Month)
                                {
                                    for (int g = 1; g <= DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); g++)
                                    {
                                        if (g == 1)
                                            GraphGridView[g, i].Value = plan.FullName;
                                        SetCell(g, plan, i);
                                    }
                                }
                            }
                        }
                    }
                    i++;
                }
            }
        }
        //информация о номере
        private void InfoRoomBtn_Click(object sender, EventArgs e)
        {
            Rooms room = null;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.Rooms.ToList())
                {
                    //если выбранная ячейка является номером комнаты
                    if (GraphGridView.CurrentCell.Value.ToString() == item.Number.ToString())
                    {
                        room = item;
                    }
                }
                if (room != null)
                {
                    RoomInfoForm form = new RoomInfoForm(room);
                    form.ShowDialog();
                }
                else
                    SmartHotel.MessageClass.RoomError();
            }
        }



        private void ResorveBtn_Click(object sender, EventArgs e)
        {
            //иконка пуста(не забронирована и не заселена) и не является номером

            if (GraphGridView.CurrentCell.Style.BackColor != reservationColor && GraphGridView.CurrentCell.Style.BackColor != arrivalColor && GraphGridView.CurrentCell.Value.ToString() == "")
            {
                _controlR.SetQuick(GraphGridView[0, GraphGridView.CurrentCell.RowIndex].Value.ToString(), new DateTime(DateTime.Now.Year, DateTime.Now.Month, int.Parse(GraphGridView.Columns[GraphGridView.CurrentCell.ColumnIndex].HeaderText.ToString())));
                SmartHotel.MessageClass.GoToReservation();
            }
            else
                SmartHotel.MessageClass.GraphError();

        }

        private void ArrivalBtn_Click(object sender, EventArgs e)
        {
            //если выбран зарезервированный заказ и первый день
            if (GraphGridView.CurrentCell.Style.BackColor == reservationColor && GraphGridView.CurrentCell.Value.ToString() != "")
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    int day = int.Parse(GraphGridView.Columns[GraphGridView.CurrentCell.ColumnIndex].HeaderText.ToString());
                    int month = DateTime.Now.Month;
                    int year = DateTime.Now.Year;
                    //передаем заказ, день которого совпадает с выбранным
                    _controlA.SetReservation(db.PlanningRooms.FirstOrDefault(x => x.DateOfArrival.Day == day && x.DateOfArrival.Month == month && x.DateOfArrival.Year == year));
                    SmartHotel.MessageClass.GoToArrival();
                }
            }
            else
                SmartHotel.MessageClass.GraphError();
        }

        //private void EvictionBtn_Click(object sender, EventArgs e)
        //{
        //    //если выбран заселенный заказ
        //    if (GraphGridView.CurrentCell.Style.BackColor == arrivalColor)
        //    {
        //        //_controlE.SetQuick(GraphGridView[0, GraphGridView.CurrentCell.RowIndex].Value.ToString(), new DateTime(DateTime.Now.Year, DateTime.Now.Month, int.Parse(GraphGridView.Columns[GraphGridView.CurrentCell.ColumnIndex].HeaderText.ToString())));
        //        SmartHotel.MessageClass.GoToArrival();
        //    }
        //    else
        //        SmartHotel.MessageClass.GraphError();
        //}
    }
}
