﻿namespace SmartHotel
{
    partial class ReservationControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label8 = new System.Windows.Forms.Label();
            this.DeleteReservationBtn = new System.Windows.Forms.Button();
            this.ChangeReservationBtn = new System.Windows.Forms.Button();
            this.AddReservationBtn = new System.Windows.Forms.Button();
            this.ReservationGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberComboBox = new System.Windows.Forms.ComboBox();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.person_name_TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.person_phone_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.prepaid_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.arrivalmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.evictionmonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.arrivalBtn = new System.Windows.Forms.Button();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ReservationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(51, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 32);
            this.label8.TabIndex = 98;
            this.label8.Text = "Номер";
            // 
            // DeleteReservationBtn
            // 
            this.DeleteReservationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeleteReservationBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteReservationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteReservationBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteReservationBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeleteReservationBtn.Location = new System.Drawing.Point(352, 321);
            this.DeleteReservationBtn.Name = "DeleteReservationBtn";
            this.DeleteReservationBtn.Size = new System.Drawing.Size(146, 44);
            this.DeleteReservationBtn.TabIndex = 89;
            this.DeleteReservationBtn.Text = "Удалить";
            this.DeleteReservationBtn.UseVisualStyleBackColor = false;
            this.DeleteReservationBtn.Click += new System.EventHandler(this.DeleteReservationBtn_Click);
            // 
            // ChangeReservationBtn
            // 
            this.ChangeReservationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ChangeReservationBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangeReservationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeReservationBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeReservationBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ChangeReservationBtn.Location = new System.Drawing.Point(504, 321);
            this.ChangeReservationBtn.Name = "ChangeReservationBtn";
            this.ChangeReservationBtn.Size = new System.Drawing.Size(146, 44);
            this.ChangeReservationBtn.TabIndex = 88;
            this.ChangeReservationBtn.Text = "Изменить";
            this.ChangeReservationBtn.UseVisualStyleBackColor = false;
            this.ChangeReservationBtn.Click += new System.EventHandler(this.ChangeReservationBtn_Click);
            // 
            // AddReservationBtn
            // 
            this.AddReservationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddReservationBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddReservationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddReservationBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddReservationBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddReservationBtn.Location = new System.Drawing.Point(656, 321);
            this.AddReservationBtn.Name = "AddReservationBtn";
            this.AddReservationBtn.Size = new System.Drawing.Size(146, 44);
            this.AddReservationBtn.TabIndex = 87;
            this.AddReservationBtn.Text = "Добавить";
            this.AddReservationBtn.UseVisualStyleBackColor = false;
            this.AddReservationBtn.Click += new System.EventHandler(this.AddReservationBtn_Click);
            // 
            // ReservationGridView
            // 
            this.ReservationGridView.AllowUserToAddRows = false;
            this.ReservationGridView.AllowUserToDeleteRows = false;
            this.ReservationGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.ReservationGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReservationGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ReservationGridView.ColumnHeadersHeight = 30;
            this.ReservationGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column10,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ReservationGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.ReservationGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ReservationGridView.Location = new System.Drawing.Point(46, 371);
            this.ReservationGridView.MultiSelect = false;
            this.ReservationGridView.Name = "ReservationGridView";
            this.ReservationGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ReservationGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ReservationGridView.RowHeadersVisible = false;
            this.ReservationGridView.RowHeadersWidth = 65;
            this.ReservationGridView.RowTemplate.Height = 30;
            this.ReservationGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ReservationGridView.Size = new System.Drawing.Size(756, 223);
            this.ReservationGridView.TabIndex = 95;
            this.ReservationGridView.DoubleClick += new System.EventHandler(this.ReservationGridView_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ФИО";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 124;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Телефон";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Номер";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Аванс";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 130;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Дата заезда";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 170;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Дата выезда";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Номер заказа";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // numberComboBox
            // 
            this.numberComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.numberComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.numberComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.numberComboBox.FormattingEnabled = true;
            this.numberComboBox.Location = new System.Drawing.Point(154, 103);
            this.numberComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.numberComboBox.Name = "numberComboBox";
            this.numberComboBox.Size = new System.Drawing.Size(244, 35);
            this.numberComboBox.TabIndex = 79;
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(154, 90);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(243, 1);
            this.materialDivider4.TabIndex = 94;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // person_name_TextBox
            // 
            this.person_name_TextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.person_name_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.person_name_TextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.person_name_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.person_name_TextBox.Location = new System.Drawing.Point(155, 59);
            this.person_name_TextBox.Name = "person_name_TextBox";
            this.person_name_TextBox.Size = new System.Drawing.Size(243, 32);
            this.person_name_TextBox.TabIndex = 86;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(51, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 32);
            this.label5.TabIndex = 93;
            this.label5.Text = "ФИО";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(300, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 43);
            this.label1.TabIndex = 90;
            this.label1.Text = "Бронирование";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(541, 90);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(255, 1);
            this.materialDivider1.TabIndex = 101;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // person_phone_textBox
            // 
            this.person_phone_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.person_phone_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.person_phone_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.person_phone_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.person_phone_textBox.Location = new System.Drawing.Point(542, 59);
            this.person_phone_textBox.Name = "person_phone_textBox";
            this.person_phone_textBox.Size = new System.Drawing.Size(255, 32);
            this.person_phone_textBox.TabIndex = 99;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(408, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 32);
            this.label2.TabIndex = 100;
            this.label2.Text = "Телефон";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(540, 137);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(255, 1);
            this.materialDivider2.TabIndex = 104;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // prepaid_textBox
            // 
            this.prepaid_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.prepaid_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.prepaid_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prepaid_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.prepaid_textBox.Location = new System.Drawing.Point(541, 106);
            this.prepaid_textBox.Name = "prepaid_textBox";
            this.prepaid_textBox.Size = new System.Drawing.Size(255, 32);
            this.prepaid_textBox.TabIndex = 102;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(409, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 32);
            this.label3.TabIndex = 103;
            this.label3.Text = "Аванс";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(407, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 32);
            this.label4.TabIndex = 109;
            this.label4.Text = "Выезд";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(50, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 32);
            this.label6.TabIndex = 106;
            this.label6.Text = "Заезд";
            // 
            // arrivalmonthCalendar
            // 
            this.arrivalmonthCalendar.Location = new System.Drawing.Point(155, 151);
            this.arrivalmonthCalendar.MaxSelectionCount = 1;
            this.arrivalmonthCalendar.Name = "arrivalmonthCalendar";
            this.arrivalmonthCalendar.TabIndex = 110;
            // 
            // evictionmonthCalendar
            // 
            this.evictionmonthCalendar.BackColor = System.Drawing.Color.DarkRed;
            this.evictionmonthCalendar.Location = new System.Drawing.Point(542, 151);
            this.evictionmonthCalendar.MaxSelectionCount = 1;
            this.evictionmonthCalendar.Name = "evictionmonthCalendar";
            this.evictionmonthCalendar.TabIndex = 111;
            // 
            // arrivalBtn
            // 
            this.arrivalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.arrivalBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.arrivalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.arrivalBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.arrivalBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.arrivalBtn.Location = new System.Drawing.Point(46, 321);
            this.arrivalBtn.Name = "arrivalBtn";
            this.arrivalBtn.Size = new System.Drawing.Size(146, 44);
            this.arrivalBtn.TabIndex = 112;
            this.arrivalBtn.Text = "Заселить";
            this.arrivalBtn.UseVisualStyleBackColor = false;
            this.arrivalBtn.Click += new System.EventHandler(this.arrivalBtn_Click);
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(46, 58);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(778, 536);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 152;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // ReservationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.arrivalBtn);
            this.Controls.Add(this.evictionmonthCalendar);
            this.Controls.Add(this.arrivalmonthCalendar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.prepaid_textBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.person_phone_textBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.DeleteReservationBtn);
            this.Controls.Add(this.ChangeReservationBtn);
            this.Controls.Add(this.AddReservationBtn);
            this.Controls.Add(this.ReservationGridView);
            this.Controls.Add(this.numberComboBox);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.person_name_TextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "ReservationControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.ReservationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button DeleteReservationBtn;
        private System.Windows.Forms.Button ChangeReservationBtn;
        private System.Windows.Forms.Button AddReservationBtn;
        private System.Windows.Forms.DataGridView ReservationGridView;
        private System.Windows.Forms.ComboBox numberComboBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox person_name_TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox person_phone_textBox;
        private System.Windows.Forms.Label label2;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox prepaid_textBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MonthCalendar arrivalmonthCalendar;
        private System.Windows.Forms.MonthCalendar evictionmonthCalendar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.Button arrivalBtn;
        private System.Windows.Forms.PictureBox preloader;
    }
}
