﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class ReservationControl : UserControl
    {
        PlanningRooms _plroom;
        ArrivalControl _control;
        public ReservationControl()
        {
            InitializeComponent();
            ReservationGridView.Columns[6].Visible = false;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                numberComboBox.DataSource = db.Rooms.Select(x => x.Number).ToList();
            }
        }
        //заполнение данными из Графика
        public void SetQuick(string num, DateTime timeStart)
        {
            numberComboBox.Text = num;
            arrivalmonthCalendar.SetDate(timeStart);
        }
        //подключение к бд и получение данных
        public void ShowForm()
        {
            
                ShowReservationDataGrid();
            
        }
        //заселение забронированного (передаю ссылку на элемент из главной формы)
        public void SetArrivalControl(ArrivalControl a)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                numberComboBox.DataSource = db.Rooms.Select(x => x.Number).ToList();
                _control = a;
            }
        }
        //отображение в DataGrid
        private void ShowReservationDataGrid()
        {
            ReservationGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningRooms.ToList())
                {
                    if (!item.Arrival)//флаг заселения
                    {

                        ReservationGridView.Rows.Add();
                        ReservationGridView[0, i].Value = item.FullName;
                        ReservationGridView[1, i].Value = item.ContactNumber;
                        ReservationGridView[2, i].Value = item.Rooms.Number;
                        ReservationGridView[3, i].Value = item.PrepaidExpense;
                        ReservationGridView[4, i].Value = item.DateOfArrival.ToShortDateString();
                        ReservationGridView[5, i].Value = item.DateOfEviction.ToShortDateString();
                        ReservationGridView[6, i].Value = item.Id;
                        i++;
                    }
                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            person_name_TextBox.Text = "";
            person_phone_textBox.Text = "";
            prepaid_textBox.Text = "";
        }

        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (person_name_TextBox.Text != "" && person_phone_textBox.Text != "" && prepaid_textBox.Text != "")
            {
                double cost;
                try
                {
                    cost = Double.Parse(prepaid_textBox.Text);
                    return cost;
                }
                catch
                {
                    SmartHotel.MessageClass.MessageCost();
                    return -1;
                }
            }
            return -1;
        }
        private bool RightDate(int id)
        {
            //проверка позже ли выезд въезда
            if (evictionmonthCalendar.SelectionRange.Start.CompareTo(arrivalmonthCalendar.SelectionRange.Start) > 0)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    PlanningRooms p;
                    //проверяем есть ли в бд на выбранный номер заказ , дата ВЪЕЗДА которого попадает в выбранные рамки
                    p = db.PlanningRooms.FirstOrDefault(x => ((x.Rooms.Number == numberComboBox.Text) && (x.DateOfArrival.CompareTo(arrivalmonthCalendar.SelectionRange.Start) >= 0) && (x.DateOfArrival.CompareTo(evictionmonthCalendar.SelectionRange.Start) < 0)));
                    if (p == null || p.Id == id)
                    {
                        //проверяем есть ли в бд на выбранный номер заказ , дата ВЫЕЗДА которого попадает в выбранные рамки
                        p = db.PlanningRooms.FirstOrDefault(x => ((x.Rooms.Number == numberComboBox.Text) && (x.DateOfEviction.CompareTo(arrivalmonthCalendar.SelectionRange.Start) > 0) && (x.DateOfEviction.CompareTo(evictionmonthCalendar.SelectionRange.Start) <= 0)));
                        if (p == null || p.Id == id)
                        {
                            //проверяем не попадёт ли заказ в уже выбранные временные рамки другого заказа
                            p = db.PlanningRooms.FirstOrDefault(x => ((x.Rooms.Number == numberComboBox.Text) && (x.DateOfArrival.CompareTo(arrivalmonthCalendar.SelectionRange.Start) < 0) && (x.DateOfEviction.CompareTo(evictionmonthCalendar.SelectionRange.Start) > 0)));
                            if (p == null || p.Id == id)
                            {
                                return true;
                            }
                            else
                            {
                                SmartHotel.MessageClass.MessageWaitNumber(p);
                            }
                        }
                        else
                        {
                            SmartHotel.MessageClass.MessageWaitNumber(p);
                        }
                    }
                    else
                    {
                        SmartHotel.MessageClass.MessageWaitNumber(p);
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.MessageWrongDataNumber();
            }
            return false;
        }
        private void AddReservationBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            double _c = TextBoxesHaveText();
            if (_c >= 0)//если текстовые поля заполнены
            {
                //проверяем даты
                if (RightDate(-1))
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        int number = db.Rooms.FirstOrDefault(x => x.Number == numberComboBox.Text).Id;
                        db.PlanningRooms.Add(new PlanningRooms { Number = number, ContactNumber = person_phone_textBox.Text, FullName = person_name_TextBox.Text, PrepaidExpense = _c, Arrival = false, DateOfArrival = arrivalmonthCalendar.SelectionRange.Start, DateOfEviction = evictionmonthCalendar.SelectionRange.Start, NumberOfAdult = "", NumberOfChildren = "", FoodPreferences = "" });
                        db.SaveChanges();//сохраняем изменения бд    
                    }
                    ShowReservationDataGrid();
                    MessageForm form = new MessageForm("Бронь добавлена");
                    form.ShowDialog();
                    ClearTextBox();
                    _plroom = null;
                }
            }
            else
            {
                SmartHotel.MessageClass.Error();
            }
            preloader.Visible = false;
        }

        private void ChangeReservationBtn_Click(object sender, EventArgs e)
        {
            if (_plroom != null)//если выбран из перечня
            {
                double _c = TextBoxesHaveText();
                if (_c >= 0)//если текстовые поля заполнены
                {
                    //проверяем даты
                    if (RightDate(_plroom.Id))
                    {
                        using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                        {
                            int number = db.Rooms.FirstOrDefault(x => x.Number == numberComboBox.Text).Id;
                            PlanningRooms plan_new = db.PlanningRooms.Find(_plroom.Id);

                            //заполняем структуру данных
                            plan_new.Number = number;
                            plan_new.FullName = person_name_TextBox.Text;
                            plan_new.ContactNumber = person_phone_textBox.Text;
                            plan_new.PrepaidExpense = _c;
                            plan_new.DateOfArrival = arrivalmonthCalendar.SelectionRange.Start;
                            plan_new.DateOfEviction = evictionmonthCalendar.SelectionRange.Start;
                            //изменение
                            db.Entry(plan_new).State = EntityState.Modified;
                            db.SaveChanges();//сохранение
                        }
                        ShowReservationDataGrid();//перерисовка DataGrid
                        SmartHotel.MessageClass.ChangeSuccessfully();
                        ClearTextBox();
                    }
                    _plroom = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteReservationBtn_Click(object sender, EventArgs e)
        {
            if (ReservationGridView.RowCount > 0)
            {
                preloader.Visible = true;
                string number = ReservationGridView[6, ReservationGridView.CurrentRow.Index].Value.ToString();
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    //удаляем элемент, название которого соответствует выбранному
                    db.PlanningRooms.Remove(db.PlanningRooms.FirstOrDefault(x => x.Id.ToString() == number));
                    db.SaveChanges();//сохраняем изменения бд     
                }
                ShowReservationDataGrid();//отображаем изменения в DataGrid
                _plroom = null;
                _control.SetReservation(null);
                SmartHotel.MessageClass.RemoveSuccessfully();
                preloader.Visible = false;
            }

        }

        private void ReservationGridView_DoubleClick(object sender, EventArgs e)
        {
            if (ReservationGridView.RowCount > 0)
            {
                if (ReservationGridView.CurrentRow.Index != -1)
                {
                    string id = ReservationGridView[6, ReservationGridView.CurrentRow.Index].Value.ToString();
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        _plroom = db.PlanningRooms.FirstOrDefault(x => x.Id.ToString() == id);
                        person_name_TextBox.Text = _plroom.FullName;
                        person_phone_textBox.Text = _plroom.ContactNumber;
                        prepaid_textBox.Text = _plroom.PrepaidExpense.ToString();
                        numberComboBox.Text = _plroom.Rooms.Number;
                        arrivalmonthCalendar.SetDate(_plroom.DateOfArrival);
                        evictionmonthCalendar.SetDate(_plroom.DateOfEviction);
                    }
                }
            }
        }

        private void arrivalBtn_Click(object sender, EventArgs e)
        {
            if (_plroom != null)//если выбран из перечня
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    _control.SetReservation(db.PlanningRooms.FirstOrDefault(x => x.Id == _plroom.Id));
                }
                SmartHotel.MessageClass.GoToArrival();
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }
    }
}
