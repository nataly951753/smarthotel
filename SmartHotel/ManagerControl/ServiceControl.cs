﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel.ManagerControl
{
    public partial class ServiceControl : UserControl
    {

        public ServiceControl()
        {
            InitializeComponent();
            CleaningHoursComboBox.SelectedIndex = 0;
            CleaningMinComboBox.SelectedIndex = 0;
            repairsTimeHourComboBox.SelectedIndex = 0;
            repairsTimeMinComboBox.SelectedIndex = 0;
            
        }
        public void ShowForm()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                numberCleaningComboBox.DataSource = db.PlanningRooms.Select(x => x.FullName).ToList();
                numberRepairsComboBox.DataSource = db.PlanningRooms.Select(x => x.FullName).ToList();

            }
        }
        //возвращает время с выбранной датой в календаре Cleaning
        private DateTime GetCleaningTime()
        {
            DateTime calendar = cleaningmonthCalendar.SelectionRange.Start;
            return new DateTime(calendar.Year, calendar.Month, calendar.Day, int.Parse(CleaningHoursComboBox.Text), int.Parse(CleaningMinComboBox.Text), 0);
        }
        //возвращает время  с выбранной датой в календаре Repairs

        private DateTime GetRepairsTime()
        {
            DateTime calendar = repairsmonthCalendar.SelectionRange.Start;
            return new DateTime(calendar.Year, calendar.Month, calendar.Day, int.Parse(repairsTimeHourComboBox.Text), int.Parse(repairsTimeMinComboBox.Text), 0);

        }
        //проверка даты и времени уборки
        private bool CanCleaning()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                PlanningService pService;
                DateTime time = GetCleaningTime();
                //проверяем есть ли в бд на выбранное время на выбранный номер заказ
                pService = db.PlanningService.FirstOrDefault(x => ((x.PlanningRooms.FullName.ToString() == numberCleaningComboBox.Text) && (x.Date.CompareTo(time) == 0)));
                return pService == null;
            }
        }
        //проверка даты и времени Repairs
        private bool CanRepairing()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                PlanningService pService;
                DateTime time = GetRepairsTime();
                //проверяем есть ли в бд на выбранное время на выбранный номер заказ
                pService = db.PlanningService.FirstOrDefault(x => ((x.PlanningRooms.FullName.ToString() == numberRepairsComboBox.Text) && (x.Date.CompareTo(time) == 0)));
                return pService == null;
            }
        }
        private void AddCleaningBtn_Click(object sender, EventArgs e)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                if (CanCleaning())
                {
                    DateTime date = GetCleaningTime();
                    int number = db.PlanningRooms.FirstOrDefault(x => x.FullName == numberCleaningComboBox.Text).Id;//номер 
                    int s = 1;//id услуги Уборки
                    db.PlanningService.Add(new PlanningService
                    {
                        Room = number,
                        Date = date,
                        Description = DescriptionCleanTextBox.Text,
                        Done = false,
                        Paid = false,
                        Service = s
                    });
                    db.SaveChanges();//сохраняем изменения бд 
                    SmartHotel.MessageClass.AddSuccessfully();
                }
                else
                    SmartHotel.MessageClass.TimeServiceError();
            }
        }

        private void AddRepairsBtn_Click(object sender, EventArgs e)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                if (CanRepairing())
                {
                    DateTime date = GetRepairsTime();
                    int number = db.PlanningRooms.FirstOrDefault(x => x.FullName == numberRepairsComboBox.Text).Id;//номер 
                    int s = 2;//id Услуги ремонта
                    db.PlanningService.Add(new PlanningService
                    {
                        Room = number,
                        Date = date,
                        Description = DescriptionCleanTextBox.Text,
                        Done = false,
                        Paid = false,
                        Service = s
                    });
                    db.SaveChanges();//сохраняем изменения бд 
                    SmartHotel.MessageClass.AddSuccessfully();
                }
                else
                    SmartHotel.MessageClass.TimeServiceError();
            }
        }
    }
}
