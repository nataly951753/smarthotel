﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;


namespace SmartHotel.ManagerControl
{
    public partial class EvictionControl : UserControl
    {
        PlanningRooms _plroom;
        public EvictionControl()
        {
            InitializeComponent();
        }
        //заполнение данными из Графика
        public void SetQuick(PlanningRooms pl)
        {
            if (pl != null)
            {
                _plroom = pl;
            }

        }
        public void ShowForm()
        {
            ShowUsersArrivalDataGrid();
        }
        //отображение в DataGrid
        private void ShowUsersArrivalDataGrid()
        {
            EvictionGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningRooms.ToList())
                {
                    if (item.Arrival)//флаг заселения
                    {
                        EvictionGridView.Rows.Add();
                        EvictionGridView[0, i].Value = item.FullName;
                        EvictionGridView[1, i].Value = item.ContactNumber;
                        EvictionGridView[2, i].Value = item.Rooms.Number;
                        EvictionGridView[3, i].Value = item.DateOfArrival.ToShortDateString();
                        EvictionGridView[4, i].Value = item.DateOfEviction.ToShortDateString();
                        EvictionGridView[5, i].Value = item.Id;
                        i++;
                    }
                }
            }
        }
        //datagrid with personal bill
        private void ShowPersonalBillArrivalDataGrid()
        {
            PersonalBilldataGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                //double sum = _plroom.PrepaidExpense;
                double sum = _plroom.Rooms.Cost;//стоимость номера в сутки
                int time = _plroom.DateOfEviction.Subtract(_plroom.DateOfArrival).Days;
                sum *= time;//кол-во дней на стоимость
                double es = _plroom.ExtraSeat * time;
                sum += es;//стоимость дополнительного места
                sum -= _plroom.PrepaidExpense;//аванс
                foreach (var item in db.PlanningService.ToList())
                {
                    if (item.PlanningRooms.Id==_plroom.Id && item.TypeService.Cost>0 && item.Done)//услуги оформленные на  выбранный номер и не бесплатные и выполненные
                    {
                        PersonalBilldataGridView.Rows.Add();
                        PersonalBilldataGridView[0, i].Value = item.TypeService.Name;
                        PersonalBilldataGridView[1, i].Value = item.TypeService.Cost.ToString();
                        PersonalBilldataGridView[2, i].Value = "1";
                        PersonalBilldataGridView[3, i].Value = item.Paid;
                        PersonalBilldataGridView[4, i].Value = item.Date.ToShortDateString();
                        PersonalBilldataGridView[5, i].Value = item.Description;
                        i++;
                        if (!item.Paid) sum += item.TypeService.Cost;//считаем общую сумму
                    }

                }
                
                foreach (var item in db.PlanningEating.ToList())
                {
                    if (item.PlanningRooms.Id == _plroom.Id)
                    {
                        PersonalBilldataGridView.Rows.Add();
                        PersonalBilldataGridView[0, i].Value = item.TypeDishes.Name;
                        PersonalBilldataGridView[1, i].Value = item.TypeDishes.Cost.ToString();
                        PersonalBilldataGridView[2, i].Value = item.Count.ToString();
                        PersonalBilldataGridView[3, i].Value = item.Paid;
                        PersonalBilldataGridView[4, i].Value = item.Date.ToShortDateString();
                        PersonalBilldataGridView[5, i].Value = item.Description;
                        i++;
                        if (!item.Paid) sum += item.Expense;//считаем общую сумму
                    }
                }
                ExpenseTextBox.Text = sum.ToString();
            }
        }
        
        private void EvictionGridView_DoubleClick(object sender, EventArgs e)
        {
            if (EvictionGridView.RowCount > 0)
            {
                if (EvictionGridView.CurrentRow.Index != -1)
                {
                    string id = EvictionGridView[5, EvictionGridView.CurrentRow.Index].Value.ToString();

                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        _plroom = db.PlanningRooms.FirstOrDefault(x => x.Id.ToString() == id);
                        ShowPersonalBillArrivalDataGrid();
                    }
                        

                }
            }
        }

        private void EvictionBtn_Click(object sender, EventArgs e)
        {
            if (_plroom != null)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string number = EvictionGridView[5, EvictionGridView.CurrentRow.Index].Value.ToString();
                    PlanningRooms pEviction = db.PlanningRooms.FirstOrDefault(x => x.Id.ToString() == number);
                    foreach (var item in db.PlanningService.ToList())
                    {
                        if (item.PlanningRooms.Id == pEviction.Id) db.PlanningService.Remove(item);
                    }
                    foreach (var item in db.PlanningEating.ToList())
                    {
                        if (item.PlanningRooms.Id == pEviction.Id) db.PlanningEating.Remove(item);
                    }
                    //удаляем элемент, id которого соответствует выбранному
                    db.PlanningRooms.Remove(pEviction);
                    db.SaveChanges();//сохраняем изменения бд    
                    ShowUsersArrivalDataGrid();//отображаем изменения в DataGrid
                    ShowPersonalBillArrivalDataGrid();
                }
                
                ExpenseTextBox.Text = "0";
            }
            else SmartHotel.MessageClass.ErrorClick();


            _plroom = null;
        }

        
    }
}
