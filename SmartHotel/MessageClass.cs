﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartHotel.EntityModels;

namespace SmartHotel
{
    public class MessageClass
    {
        public static void MessageCost()
        {
            MessageForm form = new MessageForm("Ошибка. Неверный формат стоимости. Пример: 1234,543");
            form.ShowDialog();
        }
        public static void MessageWaitNumber(PlanningRooms p)
        {
            MessageForm form = new MessageForm("Номер занят " + p.FullName + " " + p.DateOfArrival.ToShortDateString() + " " + p.DateOfEviction.ToShortDateString());
            form.ShowDialog();
        }
        public static void MessageWaitHall(PlanningConferenceHall pCH)
        {
            MessageForm form = new MessageForm("Зал занят " + pCH.FullName + " " + pCH.TimeOfArrival.ToString() + " - " + pCH.TimeOfEviction.ToShortTimeString());

            form.ShowDialog();
        }
        public static void MessageWrongDataNumber()
        {
            MessageForm form = new MessageForm("Выезд должен быть позднее заезда");
            form.ShowDialog();
        }
        public static void MessageWrongTime()
        {
            MessageForm form = new MessageForm("Конец должен быть позднее начала");
            form.ShowDialog();
        }
        public static void AddSuccessfully()
        {
            MessageForm form = new MessageForm("Добавлено успешно");
            form.ShowDialog();
        }
        public static void Error()
        {
            MessageForm form = new MessageForm("Ошибка. Введите данные и повторите попытку");
            form.ShowDialog();
        }
        public static void ChangeSuccessfully()
        {
            MessageForm form = new MessageForm("Изменения успешно внесены");
            form.ShowDialog();
        }
        public static void ErrorClick()
        {
            MessageForm form = new MessageForm("Ошибка. Выберите двойным кликом  в перечне");
            form.ShowDialog();
        }
        public static void RemoveSuccessfully()
        {
            MessageForm form = new MessageForm("Удалено успешно");
            form.ShowDialog();
        }
        public static void GoToArrival()
        {
            MessageForm form = new MessageForm("Удачно. Перейдите на Заселение");
            form.ShowDialog();
        }
        public static void GoToReservation()
        {
            MessageForm form = new MessageForm("Удачно. Перейдите на Бронь");
            form.ShowDialog();
        }
        public static void GoToNew()
        {
            MessageForm form = new MessageForm("Удачно. Перейдите на Заказ");
            form.ShowDialog();
        }
        public static void LoginIs()
        {
            MessageForm form = new MessageForm("Ошибка. Такой логин уже существует");
            form.ShowDialog();
        }
        public static void NumberIs()
        {
            MessageForm form = new MessageForm("Ошибка. Такой номер уже существует");
            form.ShowDialog();
        }
        public static void IsNotBoss()
        {
            MessageForm form = new MessageForm("Не допустимое изменение. Не осталось сотрудника BOSS");
            form.ShowDialog();
        }
        public static void CanNotRemoveBoss()
        {
            MessageForm form = new MessageForm("Для удаления BOSS, добавьте еще одного сотрудника BOSS");
            form.ShowDialog();
        }
        public static void CanNotRemoveType()
        {
            MessageForm form = new MessageForm("Ошибка. Существуют комнаты с выбранным типом. Удалите сначала их");
            form.ShowDialog();
        }
        public static void HotelInformationSave()
        {
            MessageForm form = new MessageForm("Информация об отеле успешно изменена");
            form.ShowDialog();
        }
        public static void RoomError()
        {
            MessageForm form = new MessageForm("Ошибка. Выберите ячейку с номером");
            form.ShowDialog();
        }
        public static void EmptyCellError()
        {
            MessageForm form = new MessageForm("Ошибка. Выберите пустую ячейку");
            form.ShowDialog();
        }
        public static void GraphError()
        {
            MessageForm form = new MessageForm("Ошибка. Белые - свободны, желтые - забронированы, зеленые - заселены");
            form.ShowDialog();
        }
        public static void RoomManyError()
        {
            MessageForm form = new MessageForm("Ошибка. Номера могут быть только цифрами. От меньшего к большему");
            form.ShowDialog();
        }
        public static void ShowPlanningRoomError()
        {
            MessageForm form = new MessageForm("Добавьте хотя бы один тип номеров");
            form.ShowDialog();
        }
        public static void TimeServiceError()
        {
            MessageForm form = new MessageForm("Ошибка. В это время в номере ведутся работы");
            form.ShowDialog();
        }
        public static void ShowPlanningServiceError()
        {
            MessageForm form = new MessageForm("Все номера пусты. Добавьте бронь");
            form.ShowDialog();
        }
    }
}
