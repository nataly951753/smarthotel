﻿namespace SmartHotel
{
    partial class ManyRoomsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ExitButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.materialDivider7 = new MaterialSkin.Controls.MaterialDivider();
            this.cost_textBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dinnercheckBox = new System.Windows.Forms.CheckBox();
            this.lunchcheckBox = new System.Windows.Forms.CheckBox();
            this.breakfastcheckBox = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.count_children_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.materialDivider6 = new MaterialSkin.Controls.MaterialDivider();
            this.count_adult_textBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.floortextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.AddManyRoomsBtn = new System.Windows.Forms.Button();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.numberFromTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialDivider5 = new MaterialSkin.Controls.MaterialDivider();
            this.numberBeforeTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dragControl1 = new SmartHotel.DragControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.pictureBox1.BackgroundImage = global::SmartHotel.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 27);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.BackgroundImage = global::SmartHotel.Properties.Resources.Exit;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.Location = new System.Drawing.Point(788, 3);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(20, 20);
            this.ExitButton.TabIndex = 2;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.ExitButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 27);
            this.panel1.TabIndex = 2;
            // 
            // materialDivider7
            // 
            this.materialDivider7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider7.Depth = 0;
            this.materialDivider7.Location = new System.Drawing.Point(187, 337);
            this.materialDivider7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider7.Name = "materialDivider7";
            this.materialDivider7.Size = new System.Drawing.Size(134, 1);
            this.materialDivider7.TabIndex = 106;
            this.materialDivider7.Text = "materialDivider7";
            // 
            // cost_textBox
            // 
            this.cost_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cost_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cost_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cost_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.cost_textBox.Location = new System.Drawing.Point(187, 305);
            this.cost_textBox.Name = "cost_textBox";
            this.cost_textBox.Size = new System.Drawing.Size(134, 32);
            this.cost_textBox.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label11.Location = new System.Drawing.Point(22, 303);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 32);
            this.label11.TabIndex = 105;
            this.label11.Text = "Стоимость";
            // 
            // dinnercheckBox
            // 
            this.dinnercheckBox.AutoSize = true;
            this.dinnercheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.dinnercheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.dinnercheckBox.FlatAppearance.BorderSize = 5;
            this.dinnercheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.dinnercheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dinnercheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dinnercheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.dinnercheckBox.Location = new System.Drawing.Point(743, 307);
            this.dinnercheckBox.Name = "dinnercheckBox";
            this.dinnercheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.dinnercheckBox.Size = new System.Drawing.Size(26, 25);
            this.dinnercheckBox.TabIndex = 86;
            this.dinnercheckBox.UseVisualStyleBackColor = false;
            // 
            // lunchcheckBox
            // 
            this.lunchcheckBox.AutoSize = true;
            this.lunchcheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.lunchcheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.lunchcheckBox.FlatAppearance.BorderSize = 5;
            this.lunchcheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.lunchcheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lunchcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lunchcheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.lunchcheckBox.Location = new System.Drawing.Point(608, 307);
            this.lunchcheckBox.Name = "lunchcheckBox";
            this.lunchcheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.lunchcheckBox.Size = new System.Drawing.Size(26, 25);
            this.lunchcheckBox.TabIndex = 85;
            this.lunchcheckBox.UseVisualStyleBackColor = false;
            // 
            // breakfastcheckBox
            // 
            this.breakfastcheckBox.AutoSize = true;
            this.breakfastcheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.breakfastcheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.breakfastcheckBox.FlatAppearance.BorderSize = 5;
            this.breakfastcheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.breakfastcheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.breakfastcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.breakfastcheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.breakfastcheckBox.Location = new System.Drawing.Point(469, 308);
            this.breakfastcheckBox.Name = "breakfastcheckBox";
            this.breakfastcheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.breakfastcheckBox.Size = new System.Drawing.Size(26, 25);
            this.breakfastcheckBox.TabIndex = 84;
            this.breakfastcheckBox.UseVisualStyleBackColor = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label10.Location = new System.Drawing.Point(640, 303);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 32);
            this.label10.TabIndex = 104;
            this.label10.Text = "Ужин";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label9.Location = new System.Drawing.Point(506, 303);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 32);
            this.label9.TabIndex = 103;
            this.label9.Text = "Обед";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(337, 303);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 32);
            this.label6.TabIndex = 102;
            this.label6.Text = "Завтрак";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(634, 272);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(134, 1);
            this.materialDivider2.TabIndex = 101;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // count_children_textBox
            // 
            this.count_children_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_children_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_children_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_children_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_children_textBox.Location = new System.Drawing.Point(634, 240);
            this.count_children_textBox.Name = "count_children_textBox";
            this.count_children_textBox.Size = new System.Drawing.Size(134, 32);
            this.count_children_textBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(444, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 32);
            this.label3.TabIndex = 100;
            this.label3.Text = "Кол-во детей";
            // 
            // materialDivider6
            // 
            this.materialDivider6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider6.Depth = 0;
            this.materialDivider6.Location = new System.Drawing.Point(263, 273);
            this.materialDivider6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider6.Name = "materialDivider6";
            this.materialDivider6.Size = new System.Drawing.Size(134, 1);
            this.materialDivider6.TabIndex = 99;
            this.materialDivider6.Text = "materialDivider6";
            // 
            // count_adult_textBox
            // 
            this.count_adult_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_adult_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_adult_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_adult_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_adult_textBox.Location = new System.Drawing.Point(263, 241);
            this.count_adult_textBox.Name = "count_adult_textBox";
            this.count_adult_textBox.Size = new System.Drawing.Size(134, 32);
            this.count_adult_textBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(21, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(236, 32);
            this.label4.TabIndex = 98;
            this.label4.Text = "Кол-во взрослых";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(493, 178);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 32);
            this.label8.TabIndex = 97;
            this.label8.Text = "Тип";
            // 
            // materialDivider3
            // 
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(106, 208);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(134, 1);
            this.materialDivider3.TabIndex = 96;
            this.materialDivider3.Text = "materialDivider3";
            // 
            // floortextBox
            // 
            this.floortextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.floortextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.floortextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.floortextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.floortextBox.Location = new System.Drawing.Point(106, 176);
            this.floortextBox.Name = "floortextBox";
            this.floortextBox.Size = new System.Drawing.Size(134, 32);
            this.floortextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label7.Location = new System.Drawing.Point(21, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 32);
            this.label7.TabIndex = 95;
            this.label7.Text = "Этаж";
            // 
            // AddManyRoomsBtn
            // 
            this.AddManyRoomsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddManyRoomsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddManyRoomsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddManyRoomsBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddManyRoomsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddManyRoomsBtn.Location = new System.Drawing.Point(277, 416);
            this.AddManyRoomsBtn.Name = "AddManyRoomsBtn";
            this.AddManyRoomsBtn.Size = new System.Drawing.Size(214, 44);
            this.AddManyRoomsBtn.TabIndex = 9;
            this.AddManyRoomsBtn.Text = "Добавить";
            this.AddManyRoomsBtn.UseVisualStyleBackColor = false;
            this.AddManyRoomsBtn.Click += new System.EventHandler(this.AddManyRoomsBtn_Click);
            // 
            // typeComboBox
            // 
            this.typeComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.typeComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.typeComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(562, 174);
            this.typeComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(206, 35);
            this.typeComboBox.TabIndex = 4;
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(186, 398);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(582, 1);
            this.materialDivider4.TabIndex = 94;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.descriptionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descriptionTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.descriptionTextBox.Location = new System.Drawing.Point(186, 366);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(582, 32);
            this.descriptionTextBox.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(21, 366);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 32);
            this.label5.TabIndex = 93;
            this.label5.Text = "Описание";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(315, 145);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(134, 1);
            this.materialDivider1.TabIndex = 92;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // numberFromTextbox
            // 
            this.numberFromTextbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberFromTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numberFromTextbox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberFromTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.numberFromTextbox.Location = new System.Drawing.Point(315, 113);
            this.numberFromTextbox.Name = "numberFromTextbox";
            this.numberFromTextbox.Size = new System.Drawing.Size(134, 32);
            this.numberFromTextbox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(21, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 32);
            this.label2.TabIndex = 91;
            this.label2.Text = "Номер";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(184, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(401, 43);
            this.label1.TabIndex = 108;
            this.label1.Text = "Распределение номеров";
            // 
            // materialDivider5
            // 
            this.materialDivider5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider5.Depth = 0;
            this.materialDivider5.Location = new System.Drawing.Point(634, 145);
            this.materialDivider5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider5.Name = "materialDivider5";
            this.materialDivider5.Size = new System.Drawing.Size(134, 1);
            this.materialDivider5.TabIndex = 110;
            this.materialDivider5.Text = "materialDivider5";
            // 
            // numberBeforeTextBox
            // 
            this.numberBeforeTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberBeforeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numberBeforeTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberBeforeTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.numberBeforeTextBox.Location = new System.Drawing.Point(634, 113);
            this.numberBeforeTextBox.Name = "numberBeforeTextBox";
            this.numberBeforeTextBox.Size = new System.Drawing.Size(134, 32);
            this.numberBeforeTextBox.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label12.Location = new System.Drawing.Point(257, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 32);
            this.label12.TabIndex = 111;
            this.label12.Text = "от";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label13.Location = new System.Drawing.Point(564, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 32);
            this.label13.TabIndex = 112;
            this.label13.Text = "до";
            // 
            // dragControl1
            // 
            this.dragControl1.selectControl = this.panel1;
            // 
            // ManyRoomsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 472);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.materialDivider5);
            this.Controls.Add(this.numberBeforeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.materialDivider7);
            this.Controls.Add(this.cost_textBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dinnercheckBox);
            this.Controls.Add(this.lunchcheckBox);
            this.Controls.Add(this.breakfastcheckBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.count_children_textBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialDivider6);
            this.Controls.Add(this.count_adult_textBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.materialDivider3);
            this.Controls.Add(this.floortextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AddManyRoomsBtn);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.numberFromTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ManyRoomsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ManyRoomsForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Panel panel1;
        private MaterialSkin.Controls.MaterialDivider materialDivider7;
        private System.Windows.Forms.TextBox cost_textBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox dinnercheckBox;
        private System.Windows.Forms.CheckBox lunchcheckBox;
        private System.Windows.Forms.CheckBox breakfastcheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox count_children_textBox;
        private System.Windows.Forms.Label label3;
        private MaterialSkin.Controls.MaterialDivider materialDivider6;
        private System.Windows.Forms.TextBox count_adult_textBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private System.Windows.Forms.TextBox floortextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button AddManyRoomsBtn;
        private System.Windows.Forms.ComboBox typeComboBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label label5;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox numberFromTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialDivider materialDivider5;
        private System.Windows.Forms.TextBox numberBeforeTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private DragControl dragControl1;
    }
}