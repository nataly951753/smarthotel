﻿namespace SmartHotel
{
    partial class BossForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BossForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.ServiceTypeBtn = new System.Windows.Forms.Button();
            this.RestaurantBtn = new System.Windows.Forms.Button();
            this.ConferencHallBtn = new System.Windows.Forms.Button();
            this.PlanningBtn = new System.Windows.Forms.Button();
            this.TypeNumberBtn = new System.Windows.Forms.Button();
            this.EmployerBtn = new System.Windows.Forms.Button();
            this.HotelInformationBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.MinimizeButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.dragControl1 = new SmartHotel.DragControl();
            this._roomsControl = new SmartHotel.RoomsControl();
            this._conferencHallControl = new SmartHotel.ConferencHallControl();
            this._typeNumberControl = new SmartHotel.TypeNumberControl();
            this._employerControl = new SmartHotel.EmployerControl();
            this._hotelInformationControl = new SmartHotel.HotelInformationControl();
            this._typeDishesControl = new SmartHotel.BossControl.TypeDishesControl();
            this._typeServiceControl = new SmartHotel.BossControl.TypeServiceControl();
            this.SelectorMenu = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.SelectorMenu);
            this.panel1.Controls.Add(this.ServiceTypeBtn);
            this.panel1.Controls.Add(this.RestaurantBtn);
            this.panel1.Controls.Add(this.ConferencHallBtn);
            this.panel1.Controls.Add(this.PlanningBtn);
            this.panel1.Controls.Add(this.TypeNumberBtn);
            this.panel1.Controls.Add(this.EmployerBtn);
            this.panel1.Controls.Add(this.HotelInformationBtn);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 694);
            this.panel1.TabIndex = 0;
            // 
            // ServiceTypeBtn
            // 
            this.ServiceTypeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ServiceTypeBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ServiceTypeBtn.FlatAppearance.BorderSize = 0;
            this.ServiceTypeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ServiceTypeBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ServiceTypeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ServiceTypeBtn.Image = global::SmartHotel.Properties.Resources.serviceH;
            this.ServiceTypeBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ServiceTypeBtn.Location = new System.Drawing.Point(0, 574);
            this.ServiceTypeBtn.Name = "ServiceTypeBtn";
            this.ServiceTypeBtn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.ServiceTypeBtn.Size = new System.Drawing.Size(213, 79);
            this.ServiceTypeBtn.TabIndex = 8;
            this.ServiceTypeBtn.Text = "Услуги";
            this.ServiceTypeBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ServiceTypeBtn.UseVisualStyleBackColor = false;
            this.ServiceTypeBtn.Click += new System.EventHandler(this.ServiceTypeBtn_Click);
            // 
            // RestaurantBtn
            // 
            this.RestaurantBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.RestaurantBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RestaurantBtn.FlatAppearance.BorderSize = 0;
            this.RestaurantBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestaurantBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RestaurantBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.RestaurantBtn.Image = global::SmartHotel.Properties.Resources.dishes;
            this.RestaurantBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RestaurantBtn.Location = new System.Drawing.Point(0, 492);
            this.RestaurantBtn.Name = "RestaurantBtn";
            this.RestaurantBtn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.RestaurantBtn.Size = new System.Drawing.Size(213, 79);
            this.RestaurantBtn.TabIndex = 7;
            this.RestaurantBtn.Text = "Меню";
            this.RestaurantBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RestaurantBtn.UseVisualStyleBackColor = false;
            this.RestaurantBtn.Click += new System.EventHandler(this.RestaurantBtn_Click);
            // 
            // ConferencHallBtn
            // 
            this.ConferencHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ConferencHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ConferencHallBtn.FlatAppearance.BorderSize = 0;
            this.ConferencHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConferencHallBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConferencHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ConferencHallBtn.Image = global::SmartHotel.Properties.Resources.ConferenceHallIcon;
            this.ConferencHallBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ConferencHallBtn.Location = new System.Drawing.Point(-3, 411);
            this.ConferencHallBtn.Name = "ConferencHallBtn";
            this.ConferencHallBtn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.ConferencHallBtn.Size = new System.Drawing.Size(213, 79);
            this.ConferencHallBtn.TabIndex = 6;
            this.ConferencHallBtn.Text = "Конференц-зал";
            this.ConferencHallBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ConferencHallBtn.UseVisualStyleBackColor = false;
            this.ConferencHallBtn.Click += new System.EventHandler(this.ConferencHallBtn_Click);
            // 
            // PlanningBtn
            // 
            this.PlanningBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.PlanningBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlanningBtn.FlatAppearance.BorderSize = 0;
            this.PlanningBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlanningBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PlanningBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.PlanningBtn.Image = global::SmartHotel.Properties.Resources.TypeNumberIcon;
            this.PlanningBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.PlanningBtn.Location = new System.Drawing.Point(0, 329);
            this.PlanningBtn.Name = "PlanningBtn";
            this.PlanningBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.PlanningBtn.Size = new System.Drawing.Size(213, 79);
            this.PlanningBtn.TabIndex = 5;
            this.PlanningBtn.Text = "Планирование";
            this.PlanningBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.PlanningBtn.UseVisualStyleBackColor = false;
            this.PlanningBtn.Click += new System.EventHandler(this.PlanningBtn_Click);
            // 
            // TypeNumberBtn
            // 
            this.TypeNumberBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.TypeNumberBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TypeNumberBtn.FlatAppearance.BorderSize = 0;
            this.TypeNumberBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TypeNumberBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TypeNumberBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.TypeNumberBtn.Image = global::SmartHotel.Properties.Resources.NumberIcon;
            this.TypeNumberBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.TypeNumberBtn.Location = new System.Drawing.Point(-1, 248);
            this.TypeNumberBtn.Name = "TypeNumberBtn";
            this.TypeNumberBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.TypeNumberBtn.Size = new System.Drawing.Size(213, 79);
            this.TypeNumberBtn.TabIndex = 4;
            this.TypeNumberBtn.Text = "Типы номеров";
            this.TypeNumberBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.TypeNumberBtn.UseVisualStyleBackColor = false;
            this.TypeNumberBtn.Click += new System.EventHandler(this.TypeNumberBtn_Click);
            // 
            // EmployerBtn
            // 
            this.EmployerBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.EmployerBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EmployerBtn.FlatAppearance.BorderSize = 0;
            this.EmployerBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EmployerBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EmployerBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.EmployerBtn.Image = global::SmartHotel.Properties.Resources.EmployerIcon;
            this.EmployerBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.EmployerBtn.Location = new System.Drawing.Point(-1, 165);
            this.EmployerBtn.Name = "EmployerBtn";
            this.EmployerBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.EmployerBtn.Size = new System.Drawing.Size(213, 79);
            this.EmployerBtn.TabIndex = 3;
            this.EmployerBtn.Text = "Сотрудники";
            this.EmployerBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.EmployerBtn.UseVisualStyleBackColor = false;
            this.EmployerBtn.Click += new System.EventHandler(this.EmployerBtn_Click);
            // 
            // HotelInformationBtn
            // 
            this.HotelInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(23)))), ((int)(((byte)(49)))));
            this.HotelInformationBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HotelInformationBtn.FlatAppearance.BorderSize = 0;
            this.HotelInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HotelInformationBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HotelInformationBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.HotelInformationBtn.Image = global::SmartHotel.Properties.Resources.HotelIcon1;
            this.HotelInformationBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.HotelInformationBtn.Location = new System.Drawing.Point(0, 83);
            this.HotelInformationBtn.Name = "HotelInformationBtn";
            this.HotelInformationBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.HotelInformationBtn.Size = new System.Drawing.Size(213, 79);
            this.HotelInformationBtn.TabIndex = 1;
            this.HotelInformationBtn.Text = "Отель";
            this.HotelInformationBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.HotelInformationBtn.UseVisualStyleBackColor = false;
            this.HotelInformationBtn.Click += new System.EventHandler(this.HotelInformationBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SmartHotel.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(24)))), ((int)(((byte)(32)))));
            this.panelHeader.Controls.Add(this.MinimizeButton);
            this.panelHeader.Controls.Add(this.ExitButton);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(213, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(899, 29);
            this.panelHeader.TabIndex = 1;
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.BackgroundImage = global::SmartHotel.Properties.Resources.Mini;
            this.MinimizeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MinimizeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.Location = new System.Drawing.Point(842, 5);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(20, 20);
            this.MinimizeButton.TabIndex = 4;
            this.MinimizeButton.UseVisualStyleBackColor = false;
            this.MinimizeButton.Click += new System.EventHandler(this.MinimizeButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.BackgroundImage = global::SmartHotel.Properties.Resources.Exit;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.Location = new System.Drawing.Point(871, 4);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(20, 20);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // dragControl1
            // 
            this.dragControl1.selectControl = this.panelHeader;
            // 
            // _roomsControl
            // 
            this._roomsControl.BackColor = System.Drawing.Color.Transparent;
            this._roomsControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_roomsControl.BackgroundImage")));
            this._roomsControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._roomsControl.Location = new System.Drawing.Point(234, 50);
            this._roomsControl.Name = "_roomsControl";
            this._roomsControl.Size = new System.Drawing.Size(852, 621);
            this._roomsControl.TabIndex = 6;
            // 
            // _conferencHallControl
            // 
            this._conferencHallControl.BackColor = System.Drawing.Color.Transparent;
            this._conferencHallControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_conferencHallControl.BackgroundImage")));
            this._conferencHallControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._conferencHallControl.Location = new System.Drawing.Point(234, 50);
            this._conferencHallControl.Name = "_conferencHallControl";
            this._conferencHallControl.Size = new System.Drawing.Size(852, 621);
            this._conferencHallControl.TabIndex = 5;
            // 
            // _typeNumberControl
            // 
            this._typeNumberControl.BackColor = System.Drawing.Color.Transparent;
            this._typeNumberControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_typeNumberControl.BackgroundImage")));
            this._typeNumberControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._typeNumberControl.Location = new System.Drawing.Point(234, 50);
            this._typeNumberControl.Name = "_typeNumberControl";
            this._typeNumberControl.Size = new System.Drawing.Size(852, 621);
            this._typeNumberControl.TabIndex = 4;
            // 
            // _employerControl
            // 
            this._employerControl.BackColor = System.Drawing.Color.Transparent;
            this._employerControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_employerControl.BackgroundImage")));
            this._employerControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._employerControl.Location = new System.Drawing.Point(234, 50);
            this._employerControl.Name = "_employerControl";
            this._employerControl.Size = new System.Drawing.Size(852, 621);
            this._employerControl.TabIndex = 3;
            // 
            // _hotelInformationControl
            // 
            this._hotelInformationControl.BackColor = System.Drawing.Color.Transparent;
            this._hotelInformationControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_hotelInformationControl.BackgroundImage")));
            this._hotelInformationControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._hotelInformationControl.Location = new System.Drawing.Point(234, 50);
            this._hotelInformationControl.Name = "_hotelInformationControl";
            this._hotelInformationControl.Size = new System.Drawing.Size(852, 621);
            this._hotelInformationControl.TabIndex = 2;
            // 
            // _typeDishesControl
            // 
            this._typeDishesControl.BackColor = System.Drawing.Color.Transparent;
            this._typeDishesControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_typeDishesControl.BackgroundImage")));
            this._typeDishesControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._typeDishesControl.Location = new System.Drawing.Point(234, 50);
            this._typeDishesControl.Name = "_typeDishesControl";
            this._typeDishesControl.Size = new System.Drawing.Size(852, 621);
            this._typeDishesControl.TabIndex = 7;
            // 
            // _typeServiceControl
            // 
            this._typeServiceControl.BackColor = System.Drawing.Color.Transparent;
            this._typeServiceControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_typeServiceControl.BackgroundImage")));
            this._typeServiceControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._typeServiceControl.Location = new System.Drawing.Point(234, 50);
            this._typeServiceControl.Name = "_typeServiceControl";
            this._typeServiceControl.Size = new System.Drawing.Size(852, 621);
            this._typeServiceControl.TabIndex = 8;
            // 
            // SelectorMenu
            // 
            this.SelectorMenu.BackgroundImage = global::SmartHotel.Properties.Resources.selector;
            this.SelectorMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectorMenu.Location = new System.Drawing.Point(0, 83);
            this.SelectorMenu.Name = "SelectorMenu";
            this.SelectorMenu.Size = new System.Drawing.Size(10, 80);
            this.SelectorMenu.TabIndex = 9;
            // 
            // BossForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(1112, 694);
            this.Controls.Add(this._typeServiceControl);
            this.Controls.Add(this._typeDishesControl);
            this.Controls.Add(this._roomsControl);
            this.Controls.Add(this._conferencHallControl);
            this.Controls.Add(this._typeNumberControl);
            this.Controls.Add(this._employerControl);
            this.Controls.Add(this._hotelInformationControl);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BossForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BossForm";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button MinimizeButton;
        private System.Windows.Forms.Button HotelInformationBtn;
        private System.Windows.Forms.Button ConferencHallBtn;
        private System.Windows.Forms.Button PlanningBtn;
        private System.Windows.Forms.Button TypeNumberBtn;
        private System.Windows.Forms.Button EmployerBtn;
        private HotelInformationControl _hotelInformationControl;
        private EmployerControl _employerControl;
        private TypeNumberControl _typeNumberControl;
        private ConferencHallControl _conferencHallControl;
        private RoomsControl _roomsControl;
        private DragControl dragControl1;
        private System.Windows.Forms.Button ServiceTypeBtn;
        private System.Windows.Forms.Button RestaurantBtn;
        private BossControl.TypeDishesControl _typeDishesControl;
        private BossControl.TypeServiceControl _typeServiceControl;
        private System.Windows.Forms.Panel SelectorMenu;
    }
}