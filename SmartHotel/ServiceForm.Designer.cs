﻿namespace SmartHotel
{
    partial class ServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceForm));
            this.panelHeader = new System.Windows.Forms.Panel();
            this.MinimizeButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SelectorMenu = new System.Windows.Forms.Panel();
            this.NewServiceBtn = new System.Windows.Forms.Button();
            this.GraphBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dragControl1 = new SmartHotel.DragControl();
            this._newServiceControl = new SmartHotel.ServiceControl.NewServiceControl();
            this.graphServiceControl1 = new SmartHotel.ServiceControl.GraphServiceControl();
            this.panelHeader.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(24)))), ((int)(((byte)(32)))));
            this.panelHeader.Controls.Add(this.MinimizeButton);
            this.panelHeader.Controls.Add(this.ExitButton);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(213, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(899, 29);
            this.panelHeader.TabIndex = 5;
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.BackgroundImage = global::SmartHotel.Properties.Resources.Mini;
            this.MinimizeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MinimizeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.Location = new System.Drawing.Point(842, 5);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(20, 20);
            this.MinimizeButton.TabIndex = 4;
            this.MinimizeButton.UseVisualStyleBackColor = false;
            this.MinimizeButton.Click += new System.EventHandler(this.MinimizeButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.BackgroundImage = global::SmartHotel.Properties.Resources.Exit;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.Location = new System.Drawing.Point(871, 4);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(20, 20);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.SelectorMenu);
            this.panel1.Controls.Add(this.NewServiceBtn);
            this.panel1.Controls.Add(this.GraphBtn);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 694);
            this.panel1.TabIndex = 4;
            // 
            // SelectorMenu
            // 
            this.SelectorMenu.BackgroundImage = global::SmartHotel.Properties.Resources.selector;
            this.SelectorMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectorMenu.Location = new System.Drawing.Point(0, 218);
            this.SelectorMenu.Name = "SelectorMenu";
            this.SelectorMenu.Size = new System.Drawing.Size(10, 91);
            this.SelectorMenu.TabIndex = 8;
            // 
            // NewServiceBtn
            // 
            this.NewServiceBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.NewServiceBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NewServiceBtn.FlatAppearance.BorderSize = 0;
            this.NewServiceBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NewServiceBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NewServiceBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.NewServiceBtn.Image = global::SmartHotel.Properties.Resources.reserve;
            this.NewServiceBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.NewServiceBtn.Location = new System.Drawing.Point(0, 310);
            this.NewServiceBtn.Name = "NewServiceBtn";
            this.NewServiceBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.NewServiceBtn.Size = new System.Drawing.Size(213, 90);
            this.NewServiceBtn.TabIndex = 3;
            this.NewServiceBtn.Text = "Заказ";
            this.NewServiceBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.NewServiceBtn.UseVisualStyleBackColor = false;
            this.NewServiceBtn.Click += new System.EventHandler(this.NewServiceBtn_Click);
            // 
            // GraphBtn
            // 
            this.GraphBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(23)))), ((int)(((byte)(49)))));
            this.GraphBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GraphBtn.FlatAppearance.BorderSize = 0;
            this.GraphBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GraphBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GraphBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.GraphBtn.Image = global::SmartHotel.Properties.Resources.graph1;
            this.GraphBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GraphBtn.Location = new System.Drawing.Point(0, 218);
            this.GraphBtn.Name = "GraphBtn";
            this.GraphBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.GraphBtn.Size = new System.Drawing.Size(213, 90);
            this.GraphBtn.TabIndex = 1;
            this.GraphBtn.Text = "График";
            this.GraphBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.GraphBtn.UseVisualStyleBackColor = false;
            this.GraphBtn.Click += new System.EventHandler(this.GraphBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SmartHotel.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // dragControl1
            // 
            this.dragControl1.selectControl = this.panelHeader;
            // 
            // _newServiceControl
            // 
            this._newServiceControl.BackColor = System.Drawing.Color.Transparent;
            this._newServiceControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_newServiceControl.BackgroundImage")));
            this._newServiceControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._newServiceControl.Location = new System.Drawing.Point(237, 51);
            this._newServiceControl.Name = "_newServiceControl";
            this._newServiceControl.Size = new System.Drawing.Size(852, 621);
            this._newServiceControl.TabIndex = 6;
            // 
            // graphServiceControl1
            // 
            this.graphServiceControl1.BackColor = System.Drawing.Color.Transparent;
            this.graphServiceControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("graphServiceControl1.BackgroundImage")));
            this.graphServiceControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.graphServiceControl1.Location = new System.Drawing.Point(237, 51);
            this.graphServiceControl1.Name = "graphServiceControl1";
            this.graphServiceControl1.Size = new System.Drawing.Size(852, 621);
            this.graphServiceControl1.TabIndex = 7;
            // 
            // ServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(1112, 694);
            this.Controls.Add(this.graphServiceControl1);
            this.Controls.Add(this._newServiceControl);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ServiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ServiceForm";
            this.panelHeader.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button MinimizeButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel SelectorMenu;
        private System.Windows.Forms.Button NewServiceBtn;
        private System.Windows.Forms.Button GraphBtn;
        private DragControl dragControl1;
        private ServiceControl.NewServiceControl _newServiceControl;
        private ServiceControl.GraphServiceControl graphServiceControl1;
    }
}