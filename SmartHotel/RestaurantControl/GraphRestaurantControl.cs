﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;

namespace SmartHotel.RestaurantControl
{ 
    public partial class GraphRestaurantControl : UserControl
    {
        NewDishControl _controlND;

        public GraphRestaurantControl()
        {
            InitializeComponent();
            ShowDataGrid();//отображение графика в виде таблицы
            labelDay.Text = DateTime.Now.ToString("dd MMMM");//текущий день в заголовке
        }
        public void SetControl(NewDishControl n)
        {
            _controlND = n;
        }
        public void ShowForm()
        {
            ShowDataGrid();//отображение графика в виде таблицы
        }
        //дата входит в интервал
        public  bool IsInRange( DateTime dateToCheck, DateTime startDate, DateTime endDate)
        {
            return dateToCheck >= startDate && dateToCheck < endDate;
        }
        private void ShowDataGrid()
        {
            //очистка
            GraphGridView.Rows.Clear();
            int i = 0;//строки
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var plan in db.PlanningRooms.ToList())
                {
                    //текущая дата входит в интервал заказа и номер заселен
                    if (IsInRange(DateTime.Now,plan.DateOfArrival, plan.DateOfEviction) && plan.Arrival)
                    {
                        GraphGridView.Rows.Add();
                        GraphGridView[0, i].Value = plan.FullName;
                        GraphGridView[1, i].Value = plan.Rooms.Breakfast;
                        GraphGridView[2, i].Value = plan.Rooms.Lunch;
                        GraphGridView[3, i].Value = plan.Rooms.Dinner;
                        i++;
                    }
                }
            }
        }
        private void AddDishBtn_Click(object sender, EventArgs e)
        {
            //иконка является номером

            if (GraphGridView.CurrentCell.ColumnIndex == 0)
            {
                _controlND.SetQuick(GraphGridView[0, GraphGridView.CurrentCell.RowIndex].Value.ToString());
                SmartHotel.MessageClass.GoToNew();
            }
            else
                SmartHotel.MessageClass.RoomError();
        }

        private void InfoRoomBtn_Click(object sender, EventArgs e)
        {
            PlanningRooms room = null;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningRooms.ToList())
                {
                    //если выбранная ячейка является номером комнаты
                    if (GraphGridView.CurrentCell.Value.ToString() == item.FullName.ToString())
                    {
                        room = item;
                    }
                }
                if (room != null)
                {
                    ArrivalInfoForm form = new ArrivalInfoForm(room);
                    form.ShowDialog();
                }
                else
                    SmartHotel.MessageClass.RoomError();
            }
        }
    }
}
