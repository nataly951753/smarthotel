﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel.RestaurantControl
{
    public partial class NewDishControl : UserControl
    {
        PlanningEating pl;
        bool flagUpdateCombo=true;
        public NewDishControl()
        {

            InitializeComponent();
            ShowForm();
            ShowDataGrid();

        }
        //заполнение данными из Графика
        public void SetQuick(string num)
        {
            numberComboBox.Text = num;
            DishmonthCalendar.SetDate(DateTime.Now);
            flagUpdateCombo = false;
        }
        public void ShowForm()
        {
            if (flagUpdateCombo)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    //номера, которые заселены
                    numberComboBox.DataSource = db.PlanningRooms.Where(x => x.Arrival).Select(x => x.FullName).ToList();
                    //названия блюд
                    Dish_comboBox.DataSource = db.TypeDishes.Select(x => x.Name).ToList();
                    ShowDataGrid();
                }
            }
            else
                flagUpdateCombo = true;
        }
        //отображение в DataGrid
        private void ShowDataGrid()
        {
            DishGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningEating.ToList())
                {

                    DishGridView.Rows.Add();
                    DishGridView[0, i].Value = item.PlanningRooms.FullName;
                    DishGridView[1, i].Value = item.TypeDishes.Name;
                    DishGridView[2, i].Value = item.Date.ToShortDateString();
                    DishGridView[3, i].Value = item.Paid;
                    DishGridView[4, i].Value = item.Id;
                    i++;

                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            count_textBox.Text = "1";
            DescriptionTextBox.Text = "";
            paidcheckBox.Checked = false;
            ExpenseTextBox.Text = "0";
        }

        //проверка заполненности текстовых полей
        private int TextBoxesHaveText()
        {
            int count;
            try
            {
                count = int.Parse(count_textBox.Text);
                return count;
            }
            catch
            {
                SmartHotel.MessageClass.MessageCost();
                return -1;
            }

        }
        private void SetExpense(int _c)
        {

            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                double cost = db.TypeDishes.FirstOrDefault(x => x.Name == Dish_comboBox.Text).Cost;//стоимость блюда за порцию                    
                cost *= _c;//кол-во порций на стоимость
                ExpenseTextBox.Text = cost.ToString();
            }
        }
        private void AddBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            int _c = TextBoxesHaveText();//колво порций
            if (_c >= 0)//если порций больше 0
            {
                SetExpense(_c);
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    int number = db.PlanningRooms.FirstOrDefault(x => x.FullName == numberComboBox.Text).Id;//номер комнаты
                    int dish = db.TypeDishes.FirstOrDefault(x => x.Name == Dish_comboBox.Text).Id;//блюдо

                    db.PlanningEating.Add(new PlanningEating { Room = number, Dish = dish, Description = DescriptionTextBox.Text, Expense = Double.Parse(ExpenseTextBox.Text), Paid = paidcheckBox.Checked, Date = DishmonthCalendar.SelectionRange.Start, Count = _c });

                    db.SaveChanges();//сохраняем изменения бд      
                    ShowDataGrid();
                    SmartHotel.MessageClass.AddSuccessfully();
                    ClearTextBox();
                    pl = null;
                }

            }
            else
            {
                SmartHotel.MessageClass.Error();
            }
            preloader.Visible = false;
        }

        private void ChangeBtn_Click(object sender, EventArgs e)
        {
            if (pl != null)//если выбран из перечня 
            {
                int _c = TextBoxesHaveText();
                if (_c > 0)
                {
                    SetExpense(_c);
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        int number = db.PlanningRooms.FirstOrDefault(x => x.FullName == numberComboBox.Text).Id;//номер комнаты

                        int dish = db.TypeDishes.FirstOrDefault(x => x.Name == Dish_comboBox.Text).Id;//блюдо
                        PlanningEating plan_new = db.PlanningEating.Find(pl.Id);
                        //заполняем структуру данных
                        plan_new.Room = number;
                        plan_new.Dish = dish;
                        plan_new.Paid = paidcheckBox.Checked;
                        plan_new.Description = DescriptionTextBox.Text;
                        plan_new.Count = int.Parse(count_textBox.Text);
                        plan_new.Expense = _c;
                        plan_new.Date = DishmonthCalendar.SelectionRange.Start;
                        //изменение
                        db.Entry(plan_new).State = EntityState.Modified;
                        db.SaveChanges();//сохранение
                        ShowDataGrid();//перерисовка DataGrid
                        SmartHotel.MessageClass.ChangeSuccessfully();
                        ClearTextBox();
                    }

                    pl = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (DishGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string number = DishGridView[4, DishGridView.CurrentRow.Index].Value.ToString();
                    //удаляем элемент, номер которого соответствует выбранному
                    db.PlanningEating.Remove(db.PlanningEating.FirstOrDefault(x => x.Id.ToString() == number));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowDataGrid();//отображаем изменения в DataGrid
                    pl = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }

        private void ExpenseBtn_Click(object sender, EventArgs e)
        {
            SetExpense(TextBoxesHaveText());
        }

        private void DishGridView_DoubleClick(object sender, EventArgs e)
        {
            if (DishGridView.RowCount > 0)
            {
                if (DishGridView.CurrentRow.Index != -1)
                {
                    ClearTextBox();
                    string id = DishGridView[4, DishGridView.CurrentRow.Index].Value.ToString();

                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        pl = db.PlanningEating.FirstOrDefault(x => x.Id.ToString() == id);
                        count_textBox.Text = pl.Count.ToString();
                        ExpenseTextBox.Text = pl.Expense.ToString();
                        paidcheckBox.Checked = pl.Paid;
                        DescriptionTextBox.Text = pl.Description;
                        numberComboBox.Text = pl.PlanningRooms.FullName;
                        Dish_comboBox.Text = pl.TypeDishes.Name;
                        DishmonthCalendar.SetDate(pl.Date);
                    }
                    SetExpense(TextBoxesHaveText());
                }
            }
        }
    }
}
