﻿namespace SmartHotel
{
    partial class ManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManagerForm));
            this.panelHeader = new System.Windows.Forms.Panel();
            this.MinimizeButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            //this.ServiceHotelBtn = new System.Windows.Forms.Button();
            this.ConferencHallBtn = new System.Windows.Forms.Button();
            this.EvictionBtn = new System.Windows.Forms.Button();
            this.ArrivalBtn = new System.Windows.Forms.Button();
            this.ReservationBtn = new System.Windows.Forms.Button();
            this.GraphBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dragControl1 = new SmartHotel.DragControl();
            this._planningHallControl = new SmartHotel.PlanningHallControl();
            this._arrivalControl = new SmartHotel.ArrivalControl();
            this._graphControl = new SmartHotel.GraphControl();
            this._reservationControl = new SmartHotel.ReservationControl();
            this._evictionControl = new SmartHotel.ManagerControl.EvictionControl();
            this._serviceControl = new SmartHotel.ManagerControl.ServiceControl();
            this.SelectorMenu = new System.Windows.Forms.Panel();
            this.panelHeader.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(24)))), ((int)(((byte)(32)))));
            this.panelHeader.Controls.Add(this.MinimizeButton);
            this.panelHeader.Controls.Add(this.ExitButton);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(213, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(899, 29);
            this.panelHeader.TabIndex = 3;
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.BackgroundImage = global::SmartHotel.Properties.Resources.Mini;
            this.MinimizeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MinimizeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.MinimizeButton.Location = new System.Drawing.Point(842, 5);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(20, 20);
            this.MinimizeButton.TabIndex = 4;
            this.MinimizeButton.UseVisualStyleBackColor = false;
            this.MinimizeButton.Click += new System.EventHandler(this.MinimizeButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.BackgroundImage = global::SmartHotel.Properties.Resources.Exit;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.Location = new System.Drawing.Point(871, 4);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(20, 20);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.SelectorMenu);
            //this.panel1.Controls.Add(this.ServiceHotelBtn);
            this.panel1.Controls.Add(this.ConferencHallBtn);
            this.panel1.Controls.Add(this.EvictionBtn);
            this.panel1.Controls.Add(this.ArrivalBtn);
            this.panel1.Controls.Add(this.ReservationBtn);
            this.panel1.Controls.Add(this.GraphBtn);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 694);
            this.panel1.TabIndex = 2;
            // 
            // ServiceHotelBtn
            // 
            //this.ServiceHotelBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            //this.ServiceHotelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            //this.ServiceHotelBtn.FlatAppearance.BorderSize = 0;
            //this.ServiceHotelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            //this.ServiceHotelBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            //this.ServiceHotelBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            //this.ServiceHotelBtn.Image = global::SmartHotel.Properties.Resources.serviceH;
            //this.ServiceHotelBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            //this.ServiceHotelBtn.Location = new System.Drawing.Point(0, 574);
            //this.ServiceHotelBtn.Name = "ServiceHotelBtn";
            //this.ServiceHotelBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            //this.ServiceHotelBtn.Size = new System.Drawing.Size(213, 90);
            //this.ServiceHotelBtn.TabIndex = 7;
            //this.ServiceHotelBtn.Text = "Сервис";
            //this.ServiceHotelBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            //this.ServiceHotelBtn.UseVisualStyleBackColor = false;
            //this.ServiceHotelBtn.Click += new System.EventHandler(this.ServiceHotelBtn_Click);
            // 
            // ConferencHallBtn
            // 
            this.ConferencHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ConferencHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ConferencHallBtn.FlatAppearance.BorderSize = 0;
            this.ConferencHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConferencHallBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConferencHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ConferencHallBtn.Image = global::SmartHotel.Properties.Resources.ConferenceHallIcon;
            this.ConferencHallBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ConferencHallBtn.Location = new System.Drawing.Point(-3, 481);
            this.ConferencHallBtn.Name = "ConferencHallBtn";
            this.ConferencHallBtn.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.ConferencHallBtn.Size = new System.Drawing.Size(213, 90);
            this.ConferencHallBtn.TabIndex = 6;
            this.ConferencHallBtn.Text = "Конференц-зал";
            this.ConferencHallBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ConferencHallBtn.UseVisualStyleBackColor = false;
            this.ConferencHallBtn.Click += new System.EventHandler(this.ConferencHallBtn_Click);
            // 
            // EvictionBtn
            // 
            this.EvictionBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.EvictionBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EvictionBtn.FlatAppearance.BorderSize = 0;
            this.EvictionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EvictionBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EvictionBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.EvictionBtn.Image = global::SmartHotel.Properties.Resources.eviction;
            this.EvictionBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.EvictionBtn.Location = new System.Drawing.Point(0, 389);
            this.EvictionBtn.Name = "EvictionBtn";
            this.EvictionBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.EvictionBtn.Size = new System.Drawing.Size(213, 90);
            this.EvictionBtn.TabIndex = 5;
            this.EvictionBtn.Text = "Выселение";
            this.EvictionBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.EvictionBtn.UseVisualStyleBackColor = false;
            this.EvictionBtn.Click += new System.EventHandler(this.EvictionBtn_Click);
            // 
            // ArrivalBtn
            // 
            this.ArrivalBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ArrivalBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ArrivalBtn.FlatAppearance.BorderSize = 0;
            this.ArrivalBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ArrivalBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ArrivalBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ArrivalBtn.Image = global::SmartHotel.Properties.Resources.arrival;
            this.ArrivalBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ArrivalBtn.Location = new System.Drawing.Point(-1, 298);
            this.ArrivalBtn.Name = "ArrivalBtn";
            this.ArrivalBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.ArrivalBtn.Size = new System.Drawing.Size(213, 90);
            this.ArrivalBtn.TabIndex = 4;
            this.ArrivalBtn.Text = "Заселение";
            this.ArrivalBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ArrivalBtn.UseVisualStyleBackColor = false;
            this.ArrivalBtn.Click += new System.EventHandler(this.ArrivalBtn_Click);
            // 
            // ReservationBtn
            // 
            this.ReservationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ReservationBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReservationBtn.FlatAppearance.BorderSize = 0;
            this.ReservationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReservationBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReservationBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ReservationBtn.Image = global::SmartHotel.Properties.Resources.reserve;
            this.ReservationBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ReservationBtn.Location = new System.Drawing.Point(-1, 206);
            this.ReservationBtn.Name = "ReservationBtn";
            this.ReservationBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.ReservationBtn.Size = new System.Drawing.Size(213, 90);
            this.ReservationBtn.TabIndex = 3;
            this.ReservationBtn.Text = "Бронь";
            this.ReservationBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ReservationBtn.UseVisualStyleBackColor = false;
            this.ReservationBtn.Click += new System.EventHandler(this.ReservationBtn_Click);
            // 
            // GraphBtn
            // 
            this.GraphBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(23)))), ((int)(((byte)(49)))));
            this.GraphBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GraphBtn.FlatAppearance.BorderSize = 0;
            this.GraphBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GraphBtn.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GraphBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.GraphBtn.Image = global::SmartHotel.Properties.Resources.graph1;
            this.GraphBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GraphBtn.Location = new System.Drawing.Point(0, 114);
            this.GraphBtn.Name = "GraphBtn";
            this.GraphBtn.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.GraphBtn.Size = new System.Drawing.Size(213, 90);
            this.GraphBtn.TabIndex = 1;
            this.GraphBtn.Text = "График";
            this.GraphBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.GraphBtn.UseVisualStyleBackColor = false;
            this.GraphBtn.Click += new System.EventHandler(this.GraphBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::SmartHotel.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(194, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // dragControl1
            // 
            this.dragControl1.selectControl = this.panelHeader;
            // 
            // _planningHallControl
            // 
            this._planningHallControl.BackColor = System.Drawing.Color.Transparent;
            this._planningHallControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_planningHallControl.BackgroundImage")));
            this._planningHallControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._planningHallControl.Location = new System.Drawing.Point(219, 46);
            this._planningHallControl.Name = "_planningHallControl";
            this._planningHallControl.Size = new System.Drawing.Size(852, 621);
            this._planningHallControl.TabIndex = 7;
            // 
            // _arrivalControl
            // 
            this._arrivalControl.BackColor = System.Drawing.Color.Transparent;
            this._arrivalControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_arrivalControl.BackgroundImage")));
            this._arrivalControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._arrivalControl.Location = new System.Drawing.Point(219, 46);
            this._arrivalControl.Name = "_arrivalControl";
            this._arrivalControl.Size = new System.Drawing.Size(852, 621);
            this._arrivalControl.TabIndex = 6;
            // 
            // _graphControl
            // 
            this._graphControl.BackColor = System.Drawing.Color.Transparent;
            this._graphControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_graphControl.BackgroundImage")));
            this._graphControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._graphControl.Location = new System.Drawing.Point(219, 46);
            this._graphControl.Name = "_graphControl";
            this._graphControl.Size = new System.Drawing.Size(852, 621);
            this._graphControl.TabIndex = 5;
            // 
            // _reservationControl
            // 
            this._reservationControl.BackColor = System.Drawing.Color.Transparent;
            this._reservationControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_reservationControl.BackgroundImage")));
            this._reservationControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._reservationControl.Location = new System.Drawing.Point(219, 46);
            this._reservationControl.Name = "_reservationControl";
            this._reservationControl.Size = new System.Drawing.Size(852, 621);
            this._reservationControl.TabIndex = 4;
            // 
            // _evictionControl
            // 
            this._evictionControl.BackColor = System.Drawing.Color.Transparent;
            this._evictionControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_evictionControl.BackgroundImage")));
            this._evictionControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._evictionControl.Location = new System.Drawing.Point(219, 46);
            this._evictionControl.Name = "_evictionControl";
            this._evictionControl.Size = new System.Drawing.Size(852, 621);
            this._evictionControl.TabIndex = 8;
            // 
            // _serviceControl
            // 
            this._serviceControl.BackColor = System.Drawing.Color.Transparent;
            this._serviceControl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("_serviceControl.BackgroundImage")));
            this._serviceControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this._serviceControl.Location = new System.Drawing.Point(219, 46);
            this._serviceControl.Name = "_serviceControl";
            this._serviceControl.Size = new System.Drawing.Size(852, 621);
            this._serviceControl.TabIndex = 9;
            // 
            // SelectorMenu
            // 
            this.SelectorMenu.BackgroundImage = global::SmartHotel.Properties.Resources.selector;
            this.SelectorMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectorMenu.Location = new System.Drawing.Point(0, 114);
            this.SelectorMenu.Name = "SelectorMenu";
            this.SelectorMenu.Size = new System.Drawing.Size(10, 91);
            this.SelectorMenu.TabIndex = 8;
            // 
            // ManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(1112, 694);
            this.Controls.Add(this._serviceControl);
            this.Controls.Add(this._evictionControl);
            this.Controls.Add(this._planningHallControl);
            this.Controls.Add(this._arrivalControl);
            this.Controls.Add(this._graphControl);
            this.Controls.Add(this._reservationControl);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ManagerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ManagerForm";
            this.panelHeader.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button MinimizeButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ConferencHallBtn;
        private System.Windows.Forms.Button EvictionBtn;
        private System.Windows.Forms.Button ArrivalBtn;
        private System.Windows.Forms.Button ReservationBtn;
        private System.Windows.Forms.Button GraphBtn;
        private ReservationControl _reservationControl;
        private GraphControl _graphControl;
        private ArrivalControl _arrivalControl;
        private PlanningHallControl _planningHallControl;
        private DragControl dragControl1;
        //private System.Windows.Forms.Button ServiceHotelBtn;
        private ManagerControl.EvictionControl _evictionControl;
        private ManagerControl.ServiceControl _serviceControl;
        private System.Windows.Forms.Panel SelectorMenu;
    }
}