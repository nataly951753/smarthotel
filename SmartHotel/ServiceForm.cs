﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class ServiceForm : Form
    {
        public Button btnNow;//выбранная в меню кнопка

        public ServiceForm()
        {
            InitializeComponent();
            btnNow = GraphBtn;
            graphServiceControl1.SetControl(_newServiceControl);
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ChangeMenuBtn(Button changeBtn)
        {
            SelectorMenu.Top = changeBtn.Top;
            changeBtn.BackColor = Color.FromArgb(60, 23, 49);
            btnNow.BackColor = Color.FromArgb(54, 21, 38);
            btnNow = changeBtn;
        }
        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void GraphBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(GraphBtn);
            graphServiceControl1.ShowForm();
            graphServiceControl1.BringToFront();
        }

        private void NewServiceBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(NewServiceBtn);
            _newServiceControl.ShowForm();
            _newServiceControl.BringToFront();
        }
    }
}
