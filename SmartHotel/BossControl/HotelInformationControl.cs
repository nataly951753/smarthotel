﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class HotelInformationControl : UserControl
    {
        HotelInformation hotel_info;
        public HotelInformationControl()
        {
            InitializeComponent();
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                hotel_info = db.HotelInformation.FirstOrDefault();
            if (hotel_info == null) ShowTextNone();//если нет информации об отеле
            else ShowTextInfo();
            }
        }
        
        //выбор изображения - логотипа
        private void LogoBtn_Click(object sender, EventArgs e)
        {
            if (LogoOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                LogoPictureBox.Image = Image.FromFile(LogoOpenFileDialog.FileName);
                LogoTextBox.Text = LogoOpenFileDialog.FileName;
            }
        }
        //сохранение данных в бд
        private void SaveDataBtn_Click(object sender, EventArgs e)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                if (TextBoxesHaveText())//если текстовые поля заполнены
                {
                    //анимация выполнения поверх кнопки
                    preloader.Visible = true;
                    //структура данных из текстовых полей

                    hotel_info = db.HotelInformation.FirstOrDefault();
                    if (hotel_info == null) hotel_info = new HotelInformation();
                    hotel_info.Name = nameHotelTextbox.Text;
                    hotel_info.Address = addressHotelTextBox.Text;
                    hotel_info.ContactNumber = phoneHotelTextBox.Text;
                    hotel_info.Email = emailHotelTextBox.Text;
                    hotel_info.Logo_src = LogoTextBox.Text;

                    if (db.HotelInformation.FirstOrDefault() == null)//если в бд не было записи - добавляем
                        db.HotelInformation.Add(hotel_info);
                    else
                        db.Entry(hotel_info).State = EntityState.Modified;//если запись в бд обновляется

                    db.SaveChanges();//сохраняем изменения бд    
                    SmartHotel.MessageClass.HotelInformationSave();
                    //выключаем анимацию выполнения
                    preloader.Visible = false;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
        }

        private bool TextBoxesHaveText()
        {
            return (emailHotelTextBox.Text != "" && nameHotelTextbox.Text != "" && addressHotelTextBox.Text != "" && phoneHotelTextBox.Text != "" && LogoTextBox.Text != "");
        }
        //если в бд нет информации об отеле, TextBox заполняются строками с просьбой ввести данные
        private void ShowTextNone()
        {
            emailHotelTextBox.Text = "Введите email";
            nameHotelTextbox.Text = "Введите название";
            addressHotelTextBox.Text = "Введите адрес";
            phoneHotelTextBox.Text = "Введите телефон";
        }
        //если в бд есть информация об отеле, TextBox заполняются данными
        private void ShowTextInfo()
        {
            emailHotelTextBox.Text = hotel_info.Email;
            nameHotelTextbox.Text = hotel_info.Name;
            addressHotelTextBox.Text = hotel_info.Address;
            phoneHotelTextBox.Text = hotel_info.ContactNumber;
            LogoTextBox.Text = hotel_info.Logo_src;
            LogoPictureBox.Image = Image.FromFile(hotel_info.Logo_src);
        }

        private void nameHotelTextbox_Enter(object sender, EventArgs e)
        {
            if (nameHotelTextbox.Text == "Введите название")
                nameHotelTextbox.Text = "";
        }

        private void addressHotelTextBox_Enter(object sender, EventArgs e)
        {
            if (addressHotelTextBox.Text == "Введите адрес")
                addressHotelTextBox.Text = "";
        }

        

        private void phoneHotelTextBox_Enter(object sender, EventArgs e)
        {
            if (phoneHotelTextBox.Text == "Введите телефон")
                phoneHotelTextBox.Text = "";
            
        }

        private void emailHotelTextBox_Enter(object sender, EventArgs e)
        {
            if (emailHotelTextBox.Text == "Введите email")
                emailHotelTextBox.Text = "";
        }
    }
}
