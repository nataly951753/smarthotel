﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class TypeNumberControl : UserControl
    {

        public TypeNumberControl()
        {
            InitializeComponent();
            using(SmartHotelDatabaseEntities db=new SmartHotelDatabaseEntities())
            {
                ShowTypeNumbersDataGrid();
            }
        }
        
        //отображение  в DataGrid
        private void ShowTypeNumbersDataGrid()
        {
            TypeNumberGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.TypeRooms.ToList())
                {
                    TypeNumberGridView.Rows.Add();
                    TypeNumberGridView[0, i].Value = item.Type;
                    i++;
                }
            }
        }
        private void DeleteEmployerBtn_Click(object sender, EventArgs e)
        {
            if (TypeNumberGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string type = TypeNumberGridView[0, TypeNumberGridView.CurrentRow.Index].Value.ToString();
                    if (db.Rooms.FirstOrDefault(x => x.TypeRooms.Type == type) == null)
                    {
                        //удаляем элемент, тип которого соответствует выбранному
                        db.TypeRooms.Remove(db.TypeRooms.FirstOrDefault(x => x.Type == type));
                        db.SaveChanges();//сохраняем изменения бд      
                        ShowTypeNumbersDataGrid();//отображаем изменения в DataGrid
                        SmartHotel.MessageClass.RemoveSuccessfully();
                    }
                    else
                    {
                        SmartHotel.MessageClass.CanNotRemoveType();
                    }
                }
                preloader.Visible = false;
            }
        }

        private void AddEmployerBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                if (TypeNumberTextbox.Text != "")//если текстовые поля заполнены
                {
                    db.TypeRooms.Add(new TypeRooms { Type = TypeNumberTextbox.Text });
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowTypeNumbersDataGrid();
                    SmartHotel.MessageClass.AddSuccessfully();
                    TypeNumberTextbox.Text = "";
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            preloader.Visible = false;
        }
    }
}
