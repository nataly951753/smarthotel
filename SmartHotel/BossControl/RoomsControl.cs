﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class RoomsControl : UserControl
    {
        Rooms room;
        public RoomsControl()
        {
            InitializeComponent();
        }
        public void ShowForm()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                //заполнение ComboBox типами комнат из бд
                typeComboBox.DataSource = db.TypeRooms.Select(x => x.Type).ToList();
                ShowRoomsDataGrid();
            }
        }
        //отображение в DataGrid
        private void ShowRoomsDataGrid()
        {
            RoomsGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.Rooms.ToList())
                {
                    RoomsGridView.Rows.Add();
                    RoomsGridView[0, i].Value = item.Number;
                    RoomsGridView[1, i].Value = item.Floor;
                    RoomsGridView[2, i].Value = item.TypeRooms.Type;
                    RoomsGridView[3, i].Value = item.Cost;
                    RoomsGridView[4, i].Value = item.NumberOfAdults;
                    RoomsGridView[5, i].Value = item.NumberOfChildren;
                    RoomsGridView[6, i].Value = item.Breakfast;
                    RoomsGridView[7, i].Value = item.Lunch;
                    RoomsGridView[8, i].Value = item.Dinner;
                    RoomsGridView[9, i].Value = item.Description;
                    i++;
                }
            }
        }

        //очистка текстовых полей
        private void ClearTextBox()
        {
            numberTextbox.Text = "";
            floortextBox.Text = "";
            cost_textBox.Text = "";
            count_adult_textBox.Text = "";
            count_children_textBox.Text = "";
            descriptionTextBox.Text = "";
        }

        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (numberTextbox.Text != "" && floortextBox.Text != "" && cost_textBox.Text != "" && count_adult_textBox.Text != "" && count_children_textBox.Text != "")
            {
                double cost;
                try
                {
                    cost = Double.Parse(cost_textBox.Text);
                    return cost;
                }
                catch
                {
                    return -1;
                }
            }
            return -1;
        }
        //проверка на уникальность 
        private bool NumberIsOriginal(int Id)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                Rooms r = db.Rooms.FirstOrDefault(x => (x.Number == numberTextbox.Text));
                return (r == null || r.Id == Id);//r.Id == Id - условие выполняемое при редактировании
            }
        }
        private void RoomsGridView_DoubleClick(object sender, EventArgs e)
        {
            if (RoomsGridView.RowCount > 0)
            {
                if (RoomsGridView.CurrentRow.Index != -1)
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        string number = RoomsGridView[0, RoomsGridView.CurrentRow.Index].Value.ToString();
                        room = db.Rooms.FirstOrDefault(x => x.Number == number);
                        numberTextbox.Text = room.Number;
                        floortextBox.Text = room.Floor;
                        count_adult_textBox.Text = room.NumberOfAdults;
                        count_children_textBox.Text = room.NumberOfChildren;
                        typeComboBox.Text = room.TypeRooms.Type;
                        cost_textBox.Text = room.Cost.ToString();
                        breakfastcheckBox.Checked = room.Breakfast;
                        lunchcheckBox.Checked = room.Lunch;
                        dinnercheckBox.Checked = room.Dinner;
                        descriptionTextBox.Text = room.Description;
                    }
                }
            }
        }

        private void DeleteRoomsBtn_Click(object sender, EventArgs e)
        {
            if (RoomsGridView.RowCount > 0)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    preloader.Visible = true;
                    string number = RoomsGridView[0, RoomsGridView.CurrentRow.Index].Value.ToString();
                    //удаляем элемент, название которого соответствует выбранному
                    db.Rooms.Remove(db.Rooms.FirstOrDefault(x => x.Number == number));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowRoomsDataGrid();//отображаем изменения в DataGrid
                    room = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                    preloader.Visible = false;
                }
            }
        }

        private void ChangeRoomsBtn_Click(object sender, EventArgs e)
        {
            if (room != null)//если выбран номер из перечня
            {
                double _c = TextBoxesHaveText();
                if (_c > 0)//если текстовые поля заполнены
                {
                    if (NumberIsOriginal(room.Id))//если логин оригинальный
                    {
                        using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                        {
                            Rooms room_new = db.Rooms.Find(room.Id);
                            int type = db.TypeRooms.FirstOrDefault(x => x.Type == typeComboBox.Text).Id;//получаем тип
                                                                                                        //заполняем структуру данных
                            room_new.Number = numberTextbox.Text;
                            room_new.Floor = floortextBox.Text;
                            room_new.NumberOfAdults = count_adult_textBox.Text;
                            room_new.NumberOfChildren = count_children_textBox.Text;
                            room_new.Cost = _c;
                            room_new.Breakfast = breakfastcheckBox.Checked;
                            room_new.Lunch = lunchcheckBox.Checked;
                            room_new.Dinner = dinnercheckBox.Checked;
                            room_new.Description = descriptionTextBox.Text;
                            room_new.Type = type;
                            //изменение
                            db.Entry(room_new).State = EntityState.Modified;
                            db.SaveChanges();//сохранение
                            ShowRoomsDataGrid();
                            SmartHotel.MessageClass.ChangeSuccessfully();
                            ClearTextBox();
                            room = null;
                        }

                    }
                    else
                    {
                        SmartHotel.MessageClass.NumberIs();
                    }
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void AddRoomsBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения 
            preloader.Visible = true;
            double _c = TextBoxesHaveText();
            if (_c > 0)//если текстовые поля заполнены
            {
                if (NumberIsOriginal(-1))
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        int type = db.TypeRooms.FirstOrDefault(x => x.Type == typeComboBox.Text).Id;
                        db.Rooms.Add(new Rooms { Number = numberTextbox.Text, Floor = floortextBox.Text, Type = type, NumberOfAdults = count_adult_textBox.Text, NumberOfChildren = count_children_textBox.Text, Cost = _c, Description = descriptionTextBox.Text, Breakfast = breakfastcheckBox.Checked, Lunch = lunchcheckBox.Checked, Dinner = dinnercheckBox.Checked });
                        db.SaveChanges();//сохраняем изменения бд      
                        ShowRoomsDataGrid();
                        SmartHotel.MessageClass.AddSuccessfully();
                        ClearTextBox();
                        room = null;//на случай, если до этого был выбран номер из перечня
                    }
                }
                else
                {
                    SmartHotel.MessageClass.NumberIs();
                }
            }
            else
            {
                SmartHotel.MessageClass.Error();
            }
            preloader.Visible = false;
        }

        private void ManyRoomsBtn_Click(object sender, EventArgs e)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                ManyRoomsForm form = new ManyRoomsForm(db);
                form.ShowDialog();
                ShowRoomsDataGrid();
            }
        }
    }
}
