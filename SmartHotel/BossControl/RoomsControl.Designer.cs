﻿namespace SmartHotel
{
    partial class RoomsControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DeleteRoomsBtn = new System.Windows.Forms.Button();
            this.ChangeRoomsBtn = new System.Windows.Forms.Button();
            this.AddRoomsBtn = new System.Windows.Forms.Button();
            this.RoomsGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.numberTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.floortextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.count_children_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.materialDivider6 = new MaterialSkin.Controls.MaterialDivider();
            this.count_adult_textBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.breakfastcheckBox = new System.Windows.Forms.CheckBox();
            this.lunchcheckBox = new System.Windows.Forms.CheckBox();
            this.dinnercheckBox = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.materialDivider7 = new MaterialSkin.Controls.MaterialDivider();
            this.cost_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ManyRoomsBtn = new System.Windows.Forms.Button();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // DeleteRoomsBtn
            // 
            this.DeleteRoomsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeleteRoomsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteRoomsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteRoomsBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteRoomsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeleteRoomsBtn.Location = new System.Drawing.Point(353, 253);
            this.DeleteRoomsBtn.Name = "DeleteRoomsBtn";
            this.DeleteRoomsBtn.Size = new System.Drawing.Size(146, 44);
            this.DeleteRoomsBtn.TabIndex = 13;
            this.DeleteRoomsBtn.Text = "Удалить";
            this.DeleteRoomsBtn.UseVisualStyleBackColor = false;
            this.DeleteRoomsBtn.Click += new System.EventHandler(this.DeleteRoomsBtn_Click);
            // 
            // ChangeRoomsBtn
            // 
            this.ChangeRoomsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ChangeRoomsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangeRoomsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeRoomsBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeRoomsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ChangeRoomsBtn.Location = new System.Drawing.Point(505, 253);
            this.ChangeRoomsBtn.Name = "ChangeRoomsBtn";
            this.ChangeRoomsBtn.Size = new System.Drawing.Size(146, 44);
            this.ChangeRoomsBtn.TabIndex = 12;
            this.ChangeRoomsBtn.Text = "Изменить";
            this.ChangeRoomsBtn.UseVisualStyleBackColor = false;
            this.ChangeRoomsBtn.Click += new System.EventHandler(this.ChangeRoomsBtn_Click);
            // 
            // AddRoomsBtn
            // 
            this.AddRoomsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddRoomsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddRoomsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddRoomsBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddRoomsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddRoomsBtn.Location = new System.Drawing.Point(657, 253);
            this.AddRoomsBtn.Name = "AddRoomsBtn";
            this.AddRoomsBtn.Size = new System.Drawing.Size(146, 44);
            this.AddRoomsBtn.TabIndex = 11;
            this.AddRoomsBtn.Text = "Добавить";
            this.AddRoomsBtn.UseVisualStyleBackColor = false;
            this.AddRoomsBtn.Click += new System.EventHandler(this.AddRoomsBtn_Click);
            // 
            // RoomsGridView
            // 
            this.RoomsGridView.AllowUserToAddRows = false;
            this.RoomsGridView.AllowUserToDeleteRows = false;
            this.RoomsGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.RoomsGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RoomsGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.RoomsGridView.ColumnHeadersHeight = 30;
            this.RoomsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column10,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.RoomsGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.RoomsGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.RoomsGridView.Location = new System.Drawing.Point(47, 303);
            this.RoomsGridView.MultiSelect = false;
            this.RoomsGridView.Name = "RoomsGridView";
            this.RoomsGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.RoomsGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.RoomsGridView.RowHeadersVisible = false;
            this.RoomsGridView.RowHeadersWidth = 65;
            this.RoomsGridView.RowTemplate.Height = 30;
            this.RoomsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RoomsGridView.Size = new System.Drawing.Size(756, 289);
            this.RoomsGridView.TabIndex = 49;
            this.RoomsGridView.DoubleClick += new System.EventHandler(this.RoomsGridView_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Название";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 124;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Этаж";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Тип";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Стоимость";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Width = 130;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Кол-во взрослых";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 170;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Кол-во детей";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 150;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Завтрак";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Обед";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Ужин";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Описание";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // typeComboBox
            // 
            this.typeComboBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.typeComboBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.typeComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(597, 81);
            this.typeComboBox.Margin = new System.Windows.Forms.Padding(0);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(206, 35);
            this.typeComboBox.TabIndex = 3;
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(220, 241);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(582, 1);
            this.materialDivider4.TabIndex = 46;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.descriptionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.descriptionTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.descriptionTextBox.Location = new System.Drawing.Point(220, 209);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(582, 32);
            this.descriptionTextBox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(55, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 32);
            this.label5.TabIndex = 45;
            this.label5.Text = "Описание";
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(161, 116);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(134, 1);
            this.materialDivider1.TabIndex = 41;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // numberTextbox
            // 
            this.numberTextbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.numberTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numberTextbox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numberTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.numberTextbox.Location = new System.Drawing.Point(161, 84);
            this.numberTextbox.Name = "numberTextbox";
            this.numberTextbox.Size = new System.Drawing.Size(134, 32);
            this.numberTextbox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(55, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 32);
            this.label2.TabIndex = 40;
            this.label2.Text = "Номер";
            // 
            // materialDivider3
            // 
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(388, 116);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(134, 1);
            this.materialDivider3.TabIndex = 55;
            this.materialDivider3.Text = "materialDivider3";
            // 
            // floortextBox
            // 
            this.floortextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.floortextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.floortextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.floortextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.floortextBox.Location = new System.Drawing.Point(388, 84);
            this.floortextBox.Name = "floortextBox";
            this.floortextBox.Size = new System.Drawing.Size(134, 32);
            this.floortextBox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label7.Location = new System.Drawing.Point(303, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 32);
            this.label7.TabIndex = 54;
            this.label7.Text = "Этаж";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(528, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 32);
            this.label8.TabIndex = 57;
            this.label8.Text = "Тип";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(668, 159);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(134, 1);
            this.materialDivider2.TabIndex = 63;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // count_children_textBox
            // 
            this.count_children_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_children_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_children_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_children_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_children_textBox.Location = new System.Drawing.Point(668, 127);
            this.count_children_textBox.Name = "count_children_textBox";
            this.count_children_textBox.Size = new System.Drawing.Size(134, 32);
            this.count_children_textBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(478, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 32);
            this.label3.TabIndex = 62;
            this.label3.Text = "Кол-во детей";
            // 
            // materialDivider6
            // 
            this.materialDivider6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider6.Depth = 0;
            this.materialDivider6.Location = new System.Drawing.Point(297, 160);
            this.materialDivider6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider6.Name = "materialDivider6";
            this.materialDivider6.Size = new System.Drawing.Size(134, 1);
            this.materialDivider6.TabIndex = 60;
            this.materialDivider6.Text = "materialDivider6";
            // 
            // count_adult_textBox
            // 
            this.count_adult_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.count_adult_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.count_adult_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.count_adult_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.count_adult_textBox.Location = new System.Drawing.Point(297, 128);
            this.count_adult_textBox.Name = "count_adult_textBox";
            this.count_adult_textBox.Size = new System.Drawing.Size(134, 32);
            this.count_adult_textBox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(55, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(236, 32);
            this.label4.TabIndex = 59;
            this.label4.Text = "Кол-во взрослых";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(370, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 32);
            this.label6.TabIndex = 64;
            this.label6.Text = "Завтрак";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label9.Location = new System.Drawing.Point(539, 168);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 32);
            this.label9.TabIndex = 65;
            this.label9.Text = "Обед";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label10.Location = new System.Drawing.Point(673, 168);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 32);
            this.label10.TabIndex = 66;
            this.label10.Text = "Ужин";
            // 
            // breakfastcheckBox
            // 
            this.breakfastcheckBox.AutoSize = true;
            this.breakfastcheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.breakfastcheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.breakfastcheckBox.FlatAppearance.BorderSize = 5;
            this.breakfastcheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.breakfastcheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.breakfastcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.breakfastcheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.breakfastcheckBox.Location = new System.Drawing.Point(502, 173);
            this.breakfastcheckBox.Name = "breakfastcheckBox";
            this.breakfastcheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.breakfastcheckBox.Size = new System.Drawing.Size(26, 25);
            this.breakfastcheckBox.TabIndex = 7;
            this.breakfastcheckBox.UseVisualStyleBackColor = false;
            // 
            // lunchcheckBox
            // 
            this.lunchcheckBox.AutoSize = true;
            this.lunchcheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.lunchcheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.lunchcheckBox.FlatAppearance.BorderSize = 5;
            this.lunchcheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.lunchcheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lunchcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lunchcheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.lunchcheckBox.Location = new System.Drawing.Point(641, 172);
            this.lunchcheckBox.Name = "lunchcheckBox";
            this.lunchcheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.lunchcheckBox.Size = new System.Drawing.Size(26, 25);
            this.lunchcheckBox.TabIndex = 8;
            this.lunchcheckBox.UseVisualStyleBackColor = false;
            // 
            // dinnercheckBox
            // 
            this.dinnercheckBox.AutoSize = true;
            this.dinnercheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.dinnercheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.dinnercheckBox.FlatAppearance.BorderSize = 5;
            this.dinnercheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.dinnercheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dinnercheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dinnercheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.dinnercheckBox.Location = new System.Drawing.Point(776, 172);
            this.dinnercheckBox.Name = "dinnercheckBox";
            this.dinnercheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.dinnercheckBox.Size = new System.Drawing.Size(26, 25);
            this.dinnercheckBox.TabIndex = 9;
            this.dinnercheckBox.UseVisualStyleBackColor = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label11.Location = new System.Drawing.Point(55, 168);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 32);
            this.label11.TabIndex = 73;
            this.label11.Text = "Стоимость";
            // 
            // materialDivider7
            // 
            this.materialDivider7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider7.Depth = 0;
            this.materialDivider7.Location = new System.Drawing.Point(220, 202);
            this.materialDivider7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider7.Name = "materialDivider7";
            this.materialDivider7.Size = new System.Drawing.Size(134, 1);
            this.materialDivider7.TabIndex = 75;
            this.materialDivider7.Text = "materialDivider7";
            // 
            // cost_textBox
            // 
            this.cost_textBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cost_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cost_textBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cost_textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.cost_textBox.Location = new System.Drawing.Point(220, 170);
            this.cost_textBox.Name = "cost_textBox";
            this.cost_textBox.Size = new System.Drawing.Size(134, 32);
            this.cost_textBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(240, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(401, 43);
            this.label1.TabIndex = 39;
            this.label1.Text = "Распределение номеров";
            // 
            // ManyRoomsBtn
            // 
            this.ManyRoomsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.ManyRoomsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ManyRoomsBtn.FlatAppearance.BorderSize = 0;
            this.ManyRoomsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ManyRoomsBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ManyRoomsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(23)))), ((int)(((byte)(49)))));
            this.ManyRoomsBtn.Location = new System.Drawing.Point(47, 253);
            this.ManyRoomsBtn.Name = "ManyRoomsBtn";
            this.ManyRoomsBtn.Size = new System.Drawing.Size(184, 44);
            this.ManyRoomsBtn.TabIndex = 77;
            this.ManyRoomsBtn.Text = "Создать несколько";
            this.ManyRoomsBtn.UseVisualStyleBackColor = false;
            this.ManyRoomsBtn.Click += new System.EventHandler(this.ManyRoomsBtn_Click);
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(36, 76);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(778, 518);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 78;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // RoomsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.materialDivider7);
            this.Controls.Add(this.cost_textBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dinnercheckBox);
            this.Controls.Add(this.lunchcheckBox);
            this.Controls.Add(this.breakfastcheckBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.count_children_textBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialDivider6);
            this.Controls.Add(this.count_adult_textBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.materialDivider3);
            this.Controls.Add(this.floortextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.DeleteRoomsBtn);
            this.Controls.Add(this.ChangeRoomsBtn);
            this.Controls.Add(this.AddRoomsBtn);
            this.Controls.Add(this.RoomsGridView);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.numberTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ManyRoomsBtn);
            this.DoubleBuffered = true;
            this.Name = "RoomsControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.RoomsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DeleteRoomsBtn;
        private System.Windows.Forms.Button ChangeRoomsBtn;
        private System.Windows.Forms.Button AddRoomsBtn;
        private System.Windows.Forms.DataGridView RoomsGridView;
        private System.Windows.Forms.ComboBox typeComboBox;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label label5;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox numberTextbox;
        private System.Windows.Forms.Label label2;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private System.Windows.Forms.TextBox floortextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox count_children_textBox;
        private System.Windows.Forms.Label label3;
        private MaterialSkin.Controls.MaterialDivider materialDivider6;
        private System.Windows.Forms.TextBox count_adult_textBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox breakfastcheckBox;
        private System.Windows.Forms.CheckBox lunchcheckBox;
        private System.Windows.Forms.CheckBox dinnercheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.Label label11;
        private MaterialSkin.Controls.MaterialDivider materialDivider7;
        private System.Windows.Forms.TextBox cost_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ManyRoomsBtn;
        private System.Windows.Forms.PictureBox preloader;
    }
}
