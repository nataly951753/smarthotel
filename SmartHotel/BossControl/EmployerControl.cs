﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class EmployerControl : UserControl
    {
        Users user;


        public EmployerControl()
        {
            InitializeComponent();
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                //заполнение ComboBox типами сотрудников из бд
                typeComboBox.DataSource = db.TypeUsers.Select(x => x.Post).ToList();
                ShowEmployersDataGrid();
            }
        }
        
        //отображение сотрудников в DataGrid
        private void ShowEmployersDataGrid()
        {
            EmployerGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.Users.ToList())
                {
                    EmployerGridView.Rows.Add();
                    EmployerGridView[0, i].Value = item.FullName;
                    EmployerGridView[1, i].Value = item.Post;
                    EmployerGridView[2, i].Value = item.TypeUsers.Post;
                    EmployerGridView[3, i].Value = item.Login;
                    EmployerGridView[4, i].Value = item.Password;
                    i++;
                }
            }
        }
        //добавление сотрудника
        private void AddEmployerBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                if (TextBoxesHaveText())//если текстовые поля заполнены
                {
                    if (LoginIsOriginal(-1))
                    {
                        int type = db.TypeUsers.FirstOrDefault(x => x.Post == typeComboBox.Text).Id;
                        db.Users.Add(new Users { FullName = nameEmployerTextbox.Text, Post = positionTextbox.Text, Login = loginTextBox.Text, Password = passwordTextBox.Text, Type = type });
                        db.SaveChanges();//сохраняем изменения бд      
                        ShowEmployersDataGrid();
                        SmartHotel.MessageClass.AddSuccessfully();
                        ClearTextBox();
                        user = null;//на случай, если до этого был выбран сотрудник из перечня
                    }
                    else
                    {
                        SmartHotel.MessageClass.LoginIs();
                    }
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            preloader.Visible = false;
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            nameEmployerTextbox.Text = "";
            positionTextbox.Text = "";
            loginTextBox.Text = "";
            passwordTextBox.Text = "";
        }

        //проверка заполненности текстовых полей
        private bool TextBoxesHaveText()
        {
            return (nameEmployerTextbox.Text != "" && positionTextbox.Text != "" && loginTextBox.Text != "" && passwordTextBox.Text != "");
        }
        //проверка на уникальность логина
        private bool LoginIsOriginal(int Id)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                Users u = db.Users.FirstOrDefault(x => (x.Login == loginTextBox.Text));
                return (u == null || u.Id == Id);
            }
        }
        //редактирование информации
        private void ChangeEmployerBtn_Click(object sender, EventArgs e)
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                if (user != null)//если выбран сотрудник из перечня
                {
                    if (TextBoxesHaveText())//если текстовые поля заполнены
                    {
                        if (LoginIsOriginal(user.Id))//если логин оригинальный
                        {

                            if (user.Type==1 && typeComboBox.Text != "BOSS" && !(db.Users.Count(x => x.Type == 1) > 1))
                            {
                                SmartHotel.MessageClass.IsNotBoss();
                            }
                            else
                            {
                                Users user_edit = db.Users.Find(user.Id);
                                int type = db.TypeUsers.FirstOrDefault(x => x.Post == typeComboBox.Text).Id;//получаем тип
                                                                                                            //заполняем структуру данных
                                user_edit.FullName = nameEmployerTextbox.Text;
                                user_edit.Post = positionTextbox.Text;
                                user_edit.Login = loginTextBox.Text;
                                user_edit.Password = passwordTextBox.Text;
                                user_edit.Type = type;
                                //изменение
                                db.Entry(user_edit).State = EntityState.Modified;
                                db.SaveChanges();//сохранение
                                ShowEmployersDataGrid();//перерисовка DataGrid
                                SmartHotel.MessageClass.ChangeSuccessfully();
                                ClearTextBox();
                                user = null;
                            }

                        }
                        else
                        {
                            SmartHotel.MessageClass.LoginIs();
                        }
                    }
                    else
                    {
                        SmartHotel.MessageClass.Error();
                    }
                }
                else
                {
                    SmartHotel.MessageClass.ErrorClick();
                }
            }
        }
        //удаление сотрудника
        private void DeleteEmployerBtn_Click(object sender, EventArgs e)
        {
            if (EmployerGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string login = EmployerGridView[3, EmployerGridView.CurrentRow.Index].Value.ToString();
                    //получаем пользователя, логин которого соответствует выбранному
                    Users u = db.Users.FirstOrDefault(x => x.Login == login);
                    //проверяем если сотрудник BOSS, должен быть еще один BOSS для удаления
                    if (u.Type == 1 && !(db.Users.Count(x => x.Type == 1) > 1))
                    {
                        SmartHotel.MessageClass.CanNotRemoveBoss();
                    }
                    else
                    {
                        db.Users.Remove(u);
                        db.SaveChanges();//сохраняем изменения бд      
                        ShowEmployersDataGrid();//отображаем изменения в DataGrid
                        user = null;//на случай, если до этого был выбран сотрудник из перечня
                        SmartHotel.MessageClass.RemoveSuccessfully();
                    }
                }
                preloader.Visible = false;
            }
        }

        private void EmployerGridView_DoubleClick(object sender, EventArgs e)
        {
            if (EmployerGridView.RowCount > 0)
            {
                if (EmployerGridView.CurrentRow.Index != -1)
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        string login = EmployerGridView[3, EmployerGridView.CurrentRow.Index].Value.ToString();
                        user = db.Users.FirstOrDefault(x => x.Login == login);
                        nameEmployerTextbox.Text = user.FullName;
                        positionTextbox.Text = user.Post;
                        loginTextBox.Text = user.Login;
                        passwordTextBox.Text = user.Password;
                        typeComboBox.Text = user.TypeUsers.Post;
                    }
                }
            }
        }
    }
}
