﻿namespace SmartHotel.BossControl
{
    partial class TypeDishesControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ChangeDishBtn = new System.Windows.Forms.Button();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.CostDishTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DeleteDishBtn = new System.Windows.Forms.Button();
            this.AddDishesBtn = new System.Windows.Forms.Button();
            this.TypeDishesGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.NameDishTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.ingridientsTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.weightTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.TypeDishesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // ChangeDishBtn
            // 
            this.ChangeDishBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ChangeDishBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangeDishBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeDishBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeDishBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ChangeDishBtn.Location = new System.Drawing.Point(506, 357);
            this.ChangeDishBtn.Name = "ChangeDishBtn";
            this.ChangeDishBtn.Size = new System.Drawing.Size(146, 44);
            this.ChangeDishBtn.TabIndex = 6;
            this.ChangeDishBtn.Text = "Изменить";
            this.ChangeDishBtn.UseVisualStyleBackColor = false;
            this.ChangeDishBtn.Click += new System.EventHandler(this.ChangeDishBtn_Click);
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(257, 195);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(548, 1);
            this.materialDivider2.TabIndex = 88;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // CostDishTextBox
            // 
            this.CostDishTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CostDishTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CostDishTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CostDishTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.CostDishTextBox.Location = new System.Drawing.Point(257, 163);
            this.CostDishTextBox.Name = "CostDishTextBox";
            this.CostDishTextBox.Size = new System.Drawing.Size(548, 32);
            this.CostDishTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(55, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 32);
            this.label3.TabIndex = 87;
            this.label3.Text = "Стоимость";
            // 
            // DeleteDishBtn
            // 
            this.DeleteDishBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeleteDishBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteDishBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteDishBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteDishBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeleteDishBtn.Location = new System.Drawing.Point(352, 357);
            this.DeleteDishBtn.Name = "DeleteDishBtn";
            this.DeleteDishBtn.Size = new System.Drawing.Size(146, 44);
            this.DeleteDishBtn.TabIndex = 7;
            this.DeleteDishBtn.Text = "Удалить";
            this.DeleteDishBtn.UseVisualStyleBackColor = false;
            this.DeleteDishBtn.Click += new System.EventHandler(this.DeleteDishBtn_Click);
            // 
            // AddDishesBtn
            // 
            this.AddDishesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddDishesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddDishesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddDishesBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddDishesBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddDishesBtn.Location = new System.Drawing.Point(658, 357);
            this.AddDishesBtn.Name = "AddDishesBtn";
            this.AddDishesBtn.Size = new System.Drawing.Size(146, 44);
            this.AddDishesBtn.TabIndex = 5;
            this.AddDishesBtn.Text = "Добавить";
            this.AddDishesBtn.UseVisualStyleBackColor = false;
            this.AddDishesBtn.Click += new System.EventHandler(this.AddDishesBtn_Click);
            // 
            // TypeDishesGridView
            // 
            this.TypeDishesGridView.AllowUserToAddRows = false;
            this.TypeDishesGridView.AllowUserToDeleteRows = false;
            this.TypeDishesGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.TypeDishesGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TypeDishesGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.TypeDishesGridView.ColumnHeadersHeight = 30;
            this.TypeDishesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TypeDishesGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.TypeDishesGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.TypeDishesGridView.Location = new System.Drawing.Point(49, 407);
            this.TypeDishesGridView.MultiSelect = false;
            this.TypeDishesGridView.Name = "TypeDishesGridView";
            this.TypeDishesGridView.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TypeDishesGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.TypeDishesGridView.RowHeadersVisible = false;
            this.TypeDishesGridView.RowHeadersWidth = 65;
            this.TypeDishesGridView.RowTemplate.Height = 30;
            this.TypeDishesGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TypeDishesGridView.Size = new System.Drawing.Size(756, 187);
            this.TypeDishesGridView.TabIndex = 83;
            this.TypeDishesGridView.DoubleClick += new System.EventHandler(this.TypeDishesGridView_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Название";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 180;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Стоимость";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 180;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Вес";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 180;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Ингредиенты";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 210;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(257, 139);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(548, 1);
            this.materialDivider1.TabIndex = 82;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // NameDishTextbox
            // 
            this.NameDishTextbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NameDishTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NameDishTextbox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameDishTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.NameDishTextbox.Location = new System.Drawing.Point(257, 107);
            this.NameDishTextbox.Name = "NameDishTextbox";
            this.NameDishTextbox.Size = new System.Drawing.Size(548, 32);
            this.NameDishTextbox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(55, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 32);
            this.label2.TabIndex = 81;
            this.label2.Text = "Название";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(354, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 43);
            this.label1.TabIndex = 80;
            this.label1.Text = "Меню";
            // 
            // materialDivider3
            // 
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(257, 310);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(548, 1);
            this.materialDivider3.TabIndex = 95;
            this.materialDivider3.Text = "materialDivider3";
            // 
            // ingridientsTextBox
            // 
            this.ingridientsTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ingridientsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ingridientsTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ingridientsTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ingridientsTextBox.Location = new System.Drawing.Point(257, 278);
            this.ingridientsTextBox.Name = "ingridientsTextBox";
            this.ingridientsTextBox.Size = new System.Drawing.Size(548, 32);
            this.ingridientsTextBox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(55, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(196, 32);
            this.label4.TabIndex = 94;
            this.label4.Text = "Ингредиенты";
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(257, 254);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(548, 1);
            this.materialDivider4.TabIndex = 92;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // weightTextBox
            // 
            this.weightTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.weightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.weightTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.weightTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.weightTextBox.Location = new System.Drawing.Point(257, 222);
            this.weightTextBox.Name = "weightTextBox";
            this.weightTextBox.Size = new System.Drawing.Size(548, 32);
            this.weightTextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(55, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 32);
            this.label5.TabIndex = 91;
            this.label5.Text = "Вес";
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(37, 76);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(778, 518);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 97;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // TypeDishesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.materialDivider3);
            this.Controls.Add(this.ingridientsTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.weightTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ChangeDishBtn);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.CostDishTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DeleteDishBtn);
            this.Controls.Add(this.AddDishesBtn);
            this.Controls.Add(this.TypeDishesGridView);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.NameDishTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "TypeDishesControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.TypeDishesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ChangeDishBtn;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox CostDishTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button DeleteDishBtn;
        private System.Windows.Forms.Button AddDishesBtn;
        private System.Windows.Forms.DataGridView TypeDishesGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox NameDishTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private System.Windows.Forms.TextBox ingridientsTextBox;
        private System.Windows.Forms.Label label4;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox weightTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox preloader;
    }
}
