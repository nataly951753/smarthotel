﻿namespace SmartHotel
{
    partial class HotelInformationControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.nameHotelTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.addressHotelTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.label4 = new System.Windows.Forms.Label();
            this.materialDivider4 = new MaterialSkin.Controls.MaterialDivider();
            this.emailHotelTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LogoPictureBox = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.materialDivider5 = new MaterialSkin.Controls.MaterialDivider();
            this.LogoTextBox = new System.Windows.Forms.TextBox();
            this.LogoBtn = new System.Windows.Forms.Button();
            this.SaveDataBtn = new System.Windows.Forms.Button();
            this.LogoOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.phoneHotelTextBox = new System.Windows.Forms.TextBox();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(226, 151);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(584, 1);
            this.materialDivider1.TabIndex = 10;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // nameHotelTextbox
            // 
            this.nameHotelTextbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nameHotelTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nameHotelTextbox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameHotelTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.nameHotelTextbox.Location = new System.Drawing.Point(226, 119);
            this.nameHotelTextbox.Name = "nameHotelTextbox";
            this.nameHotelTextbox.Size = new System.Drawing.Size(584, 32);
            this.nameHotelTextbox.TabIndex = 1;
            this.nameHotelTextbox.Enter += new System.EventHandler(this.nameHotelTextbox_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(60, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 32);
            this.label2.TabIndex = 8;
            this.label2.Text = "Название";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(236, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(389, 43);
            this.label1.TabIndex = 7;
            this.label1.Text = "Информация об отеле";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(226, 209);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(584, 1);
            this.materialDivider2.TabIndex = 13;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // addressHotelTextBox
            // 
            this.addressHotelTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.addressHotelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addressHotelTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addressHotelTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.addressHotelTextBox.Location = new System.Drawing.Point(226, 177);
            this.addressHotelTextBox.Name = "addressHotelTextBox";
            this.addressHotelTextBox.Size = new System.Drawing.Size(584, 32);
            this.addressHotelTextBox.TabIndex = 2;
            this.addressHotelTextBox.Enter += new System.EventHandler(this.addressHotelTextBox_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(60, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 32);
            this.label3.TabIndex = 11;
            this.label3.Text = "Адрес";
            // 
            // materialDivider3
            // 
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(226, 266);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(584, 1);
            this.materialDivider3.TabIndex = 16;
            this.materialDivider3.Text = "materialDivider3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(60, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 32);
            this.label4.TabIndex = 14;
            this.label4.Text = "Телефон";
            // 
            // materialDivider4
            // 
            this.materialDivider4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider4.Depth = 0;
            this.materialDivider4.Location = new System.Drawing.Point(226, 325);
            this.materialDivider4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider4.Name = "materialDivider4";
            this.materialDivider4.Size = new System.Drawing.Size(584, 1);
            this.materialDivider4.TabIndex = 19;
            this.materialDivider4.Text = "materialDivider4";
            // 
            // emailHotelTextBox
            // 
            this.emailHotelTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.emailHotelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.emailHotelTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.emailHotelTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.emailHotelTextBox.Location = new System.Drawing.Point(226, 293);
            this.emailHotelTextBox.Name = "emailHotelTextBox";
            this.emailHotelTextBox.Size = new System.Drawing.Size(584, 32);
            this.emailHotelTextBox.TabIndex = 4;
            this.emailHotelTextBox.Enter += new System.EventHandler(this.emailHotelTextBox_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(60, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 32);
            this.label5.TabIndex = 17;
            this.label5.Text = "Email";
            // 
            // LogoPictureBox
            // 
            this.LogoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.LogoPictureBox.Image = global::SmartHotel.Properties.Resources.Logo;
            this.LogoPictureBox.Location = new System.Drawing.Point(66, 361);
            this.LogoPictureBox.Name = "LogoPictureBox";
            this.LogoPictureBox.Size = new System.Drawing.Size(233, 157);
            this.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPictureBox.TabIndex = 20;
            this.LogoPictureBox.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(356, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 32);
            this.label6.TabIndex = 21;
            this.label6.Text = "Логотип";
            // 
            // materialDivider5
            // 
            this.materialDivider5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider5.Depth = 0;
            this.materialDivider5.Location = new System.Drawing.Point(362, 442);
            this.materialDivider5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider5.Name = "materialDivider5";
            this.materialDivider5.Size = new System.Drawing.Size(448, 1);
            this.materialDivider5.TabIndex = 23;
            this.materialDivider5.Text = "materialDivider5";
            // 
            // LogoTextBox
            // 
            this.LogoTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LogoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LogoTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogoTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.LogoTextBox.Location = new System.Drawing.Point(362, 410);
            this.LogoTextBox.Name = "LogoTextBox";
            this.LogoTextBox.ReadOnly = true;
            this.LogoTextBox.Size = new System.Drawing.Size(448, 32);
            this.LogoTextBox.TabIndex = 22;
            // 
            // LogoBtn
            // 
            this.LogoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.LogoBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LogoBtn.FlatAppearance.BorderSize = 0;
            this.LogoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LogoBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogoBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(23)))), ((int)(((byte)(49)))));
            this.LogoBtn.Location = new System.Drawing.Point(626, 460);
            this.LogoBtn.Name = "LogoBtn";
            this.LogoBtn.Size = new System.Drawing.Size(184, 44);
            this.LogoBtn.TabIndex = 5;
            this.LogoBtn.Text = "Обзор";
            this.LogoBtn.UseVisualStyleBackColor = false;
            this.LogoBtn.Click += new System.EventHandler(this.LogoBtn_Click);
            // 
            // SaveDataBtn
            // 
            this.SaveDataBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.SaveDataBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SaveDataBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveDataBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SaveDataBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.SaveDataBtn.Location = new System.Drawing.Point(244, 539);
            this.SaveDataBtn.Name = "SaveDataBtn";
            this.SaveDataBtn.Size = new System.Drawing.Size(329, 44);
            this.SaveDataBtn.TabIndex = 6;
            this.SaveDataBtn.Text = "Сохранить данные";
            this.SaveDataBtn.UseVisualStyleBackColor = false;
            this.SaveDataBtn.Click += new System.EventHandler(this.SaveDataBtn_Click);
            // 
            // LogoOpenFileDialog
            // 
            this.LogoOpenFileDialog.FileName = "openFileDialog1";
            this.LogoOpenFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif;" +
    " *.png";
            // 
            // phoneHotelTextBox
            // 
            this.phoneHotelTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.phoneHotelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.phoneHotelTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.phoneHotelTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.phoneHotelTextBox.Location = new System.Drawing.Point(226, 234);
            this.phoneHotelTextBox.Name = "phoneHotelTextBox";
            this.phoneHotelTextBox.Size = new System.Drawing.Size(584, 32);
            this.phoneHotelTextBox.TabIndex = 3;
            this.phoneHotelTextBox.Enter += new System.EventHandler(this.phoneHotelTextBox_Enter);
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(52, 91);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(758, 509);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 36;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // HotelInformationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.SaveDataBtn);
            this.Controls.Add(this.LogoBtn);
            this.Controls.Add(this.materialDivider5);
            this.Controls.Add(this.LogoTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LogoPictureBox);
            this.Controls.Add(this.materialDivider4);
            this.Controls.Add(this.emailHotelTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.materialDivider3);
            this.Controls.Add(this.phoneHotelTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.addressHotelTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.nameHotelTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "HotelInformationControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox nameHotelTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox addressHotelTextBox;
        private System.Windows.Forms.Label label3;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private System.Windows.Forms.Label label4;
        private MaterialSkin.Controls.MaterialDivider materialDivider4;
        private System.Windows.Forms.TextBox emailHotelTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox LogoPictureBox;
        private System.Windows.Forms.Label label6;
        private MaterialSkin.Controls.MaterialDivider materialDivider5;
        private System.Windows.Forms.TextBox LogoTextBox;
        private System.Windows.Forms.Button LogoBtn;
        private System.Windows.Forms.Button SaveDataBtn;
        private System.Windows.Forms.OpenFileDialog LogoOpenFileDialog;
        private System.Windows.Forms.TextBox phoneHotelTextBox;
        private System.Windows.Forms.PictureBox preloader;
    }
}
