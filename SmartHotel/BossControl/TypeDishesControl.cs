﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel.BossControl
{
    public partial class TypeDishesControl : UserControl
    {
        TypeDishes _tdishes;

        public TypeDishesControl()
        {
            InitializeComponent();
            ShowTDishDataGrid();

        }
        //отображение в DataGrid
        private void ShowTDishDataGrid()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                TypeDishesGridView.Rows.Clear();
                int i = 0;
                foreach (var item in db.TypeDishes.ToList())
                {
                    TypeDishesGridView.Rows.Add();
                    TypeDishesGridView[0, i].Value = item.Name;
                    TypeDishesGridView[1, i].Value = item.Cost;
                    TypeDishesGridView[2, i].Value = item.Weight;
                    TypeDishesGridView[3, i].Value = item.Ingredients;

                    i++;
                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            NameDishTextbox.Text = "";
            CostDishTextBox.Text = "";
            weightTextBox.Text = "";
            ingridientsTextBox.Text = "";

        }

        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (NameDishTextbox.Text != "" && CostDishTextBox.Text != "")
            {
                try
                {
                    double weight;
                    if (weightTextBox.Text!="")
                     weight = Double.Parse(weightTextBox.Text);
                    double cost = Double.Parse(CostDishTextBox.Text);
                    return cost;
                }
                catch
                {
                    return -1;
                }
            }
            return -1;
        }

        private void AddDishesBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                double _c = TextBoxesHaveText();
                if (_c > 0)//если текстовые поля заполнены
                {                    
                    if (weightTextBox.Text != "")
                        db.TypeDishes.Add(new TypeDishes { Name = NameDishTextbox.Text, Cost = _c, Weight = Double.Parse(weightTextBox.Text), Ingredients = ingridientsTextBox.Text });
                    
                    else
                        db.TypeDishes.Add(new TypeDishes { Name = NameDishTextbox.Text, Cost = _c, Weight = null, Ingredients = ingridientsTextBox.Text });
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowTDishDataGrid();
                    SmartHotel.MessageClass.AddSuccessfully();
                    ClearTextBox();
                    _tdishes = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            preloader.Visible = false;
        }

        private void ChangeDishBtn_Click(object sender, EventArgs e)
        {
            if (_tdishes != null)//если выбран из перечня
            {
                preloader.Visible = true;

                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    double _c = TextBoxesHaveText();
                    if (_c > 0)//если текстовые поля заполнены
                    {
                        //заполняем структуру данных
                        _tdishes.Name = NameDishTextbox.Text;
                        _tdishes.Cost = _c;
                        _tdishes.Ingredients = ingridientsTextBox.Text;
                        if (weightTextBox.Text != "")
                            _tdishes.Weight = Double.Parse(weightTextBox.Text);
                        else
                            _tdishes.Weight = null;
                        //изменение
                        db.Entry(_tdishes).State = EntityState.Modified;
                        db.SaveChanges();//сохранение
                        ShowTDishDataGrid();//перерисовка DataGrid
                        SmartHotel.MessageClass.ChangeSuccessfully();
                        ClearTextBox();
                        _tdishes = null;
                    }
                    else
                    {
                        SmartHotel.MessageClass.Error();
                    }
                }
                preloader.Visible = false;

            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteDishBtn_Click(object sender, EventArgs e)
        {
            if (TypeDishesGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string name = TypeDishesGridView[0, TypeDishesGridView.CurrentRow.Index].Value.ToString();
                    double cost = Double.Parse(TypeDishesGridView[1, TypeDishesGridView.CurrentRow.Index].Value.ToString());
                    //удаляем элемент, название и цена которого соответствуют выбранному
                    db.TypeDishes.Remove(db.TypeDishes.FirstOrDefault(x => x.Name == name && x.Cost == cost));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowTDishDataGrid();//отображаем изменения в DataGrid
                    _tdishes = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }

        private void TypeDishesGridView_DoubleClick(object sender, EventArgs e)
        {
            if (TypeDishesGridView.RowCount > 0)
            {
                if (TypeDishesGridView.CurrentRow.Index != -1)
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        string name = TypeDishesGridView[0, TypeDishesGridView.CurrentRow.Index].Value.ToString();
                        _tdishes = db.TypeDishes.FirstOrDefault(x => x.Name == name);
                        NameDishTextbox.Text = _tdishes.Name;
                        CostDishTextBox.Text = _tdishes.Cost.ToString();
                        ingridientsTextBox.Text = _tdishes.Ingredients;
                        weightTextBox.Text = _tdishes.Weight.ToString();
                    }
                }
            }
        }
    }
}
