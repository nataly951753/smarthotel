﻿namespace SmartHotel
{
    partial class TypeNumberControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DeleteNumberBtn = new System.Windows.Forms.Button();
            this.AddNumberBtn = new System.Windows.Forms.Button();
            this.TypeNumberGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.TypeNumberTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.TypeNumberGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // DeleteNumberBtn
            // 
            this.DeleteNumberBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeleteNumberBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteNumberBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteNumberBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteNumberBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeleteNumberBtn.Location = new System.Drawing.Point(505, 221);
            this.DeleteNumberBtn.Name = "DeleteNumberBtn";
            this.DeleteNumberBtn.Size = new System.Drawing.Size(146, 44);
            this.DeleteNumberBtn.TabIndex = 3;
            this.DeleteNumberBtn.Text = "Удалить";
            this.DeleteNumberBtn.UseVisualStyleBackColor = false;
            this.DeleteNumberBtn.Click += new System.EventHandler(this.DeleteEmployerBtn_Click);
            // 
            // AddNumberBtn
            // 
            this.AddNumberBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddNumberBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddNumberBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddNumberBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddNumberBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddNumberBtn.Location = new System.Drawing.Point(657, 221);
            this.AddNumberBtn.Name = "AddNumberBtn";
            this.AddNumberBtn.Size = new System.Drawing.Size(146, 44);
            this.AddNumberBtn.TabIndex = 2;
            this.AddNumberBtn.Text = "Добавить";
            this.AddNumberBtn.UseVisualStyleBackColor = false;
            this.AddNumberBtn.Click += new System.EventHandler(this.AddEmployerBtn_Click);
            // 
            // TypeNumberGridView
            // 
            this.TypeNumberGridView.AllowUserToAddRows = false;
            this.TypeNumberGridView.AllowUserToDeleteRows = false;
            this.TypeNumberGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.TypeNumberGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TypeNumberGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TypeNumberGridView.ColumnHeadersHeight = 30;
            this.TypeNumberGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TypeNumberGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.TypeNumberGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.TypeNumberGridView.Location = new System.Drawing.Point(47, 303);
            this.TypeNumberGridView.MultiSelect = false;
            this.TypeNumberGridView.Name = "TypeNumberGridView";
            this.TypeNumberGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TypeNumberGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TypeNumberGridView.RowHeadersVisible = false;
            this.TypeNumberGridView.RowHeadersWidth = 65;
            this.TypeNumberGridView.RowTemplate.Height = 30;
            this.TypeNumberGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TypeNumberGridView.Size = new System.Drawing.Size(756, 289);
            this.TypeNumberGridView.TabIndex = 49;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Тип";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 754;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(216, 182);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(584, 1);
            this.materialDivider1.TabIndex = 41;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // TypeNumberTextbox
            // 
            this.TypeNumberTextbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TypeNumberTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TypeNumberTextbox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TypeNumberTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.TypeNumberTextbox.Location = new System.Drawing.Point(216, 150);
            this.TypeNumberTextbox.Name = "TypeNumberTextbox";
            this.TypeNumberTextbox.Size = new System.Drawing.Size(584, 32);
            this.TypeNumberTextbox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(50, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 32);
            this.label2.TabIndex = 40;
            this.label2.Text = "Тип";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(303, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 43);
            this.label1.TabIndex = 39;
            this.label1.Text = "Типы номеров";
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(47, 83);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(758, 509);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 54;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // TypeNumberControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.DeleteNumberBtn);
            this.Controls.Add(this.AddNumberBtn);
            this.Controls.Add(this.TypeNumberGridView);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.TypeNumberTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "TypeNumberControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.TypeNumberGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DeleteNumberBtn;
        private System.Windows.Forms.Button AddNumberBtn;
        private System.Windows.Forms.DataGridView TypeNumberGridView;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox TypeNumberTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.PictureBox preloader;
    }
}
