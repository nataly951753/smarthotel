﻿namespace SmartHotel
{
    partial class ConferencHallControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DeleteHallBtn = new System.Windows.Forms.Button();
            this.AddHallBtn = new System.Windows.Forms.Button();
            this.HallGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.NameHallTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.CostHallTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ChangeHallBtn = new System.Windows.Forms.Button();
            this.preloader = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.HallGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).BeginInit();
            this.SuspendLayout();
            // 
            // DeleteHallBtn
            // 
            this.DeleteHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DeleteHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteHallBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DeleteHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.DeleteHallBtn.Location = new System.Drawing.Point(353, 221);
            this.DeleteHallBtn.Name = "DeleteHallBtn";
            this.DeleteHallBtn.Size = new System.Drawing.Size(146, 44);
            this.DeleteHallBtn.TabIndex = 5;
            this.DeleteHallBtn.Text = "Удалить";
            this.DeleteHallBtn.UseVisualStyleBackColor = false;
            this.DeleteHallBtn.Click += new System.EventHandler(this.DeleteHallBtn_Click);
            // 
            // AddHallBtn
            // 
            this.AddHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.AddHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddHallBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.AddHallBtn.Location = new System.Drawing.Point(659, 221);
            this.AddHallBtn.Name = "AddHallBtn";
            this.AddHallBtn.Size = new System.Drawing.Size(146, 44);
            this.AddHallBtn.TabIndex = 3;
            this.AddHallBtn.Text = "Добавить";
            this.AddHallBtn.UseVisualStyleBackColor = false;
            this.AddHallBtn.Click += new System.EventHandler(this.AddHallBtn_Click);
            // 
            // HallGridView
            // 
            this.HallGridView.AllowUserToAddRows = false;
            this.HallGridView.AllowUserToDeleteRows = false;
            this.HallGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.HallGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HallGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.HallGridView.ColumnHeadersHeight = 30;
            this.HallGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HallGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.HallGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.HallGridView.Location = new System.Drawing.Point(49, 303);
            this.HallGridView.MultiSelect = false;
            this.HallGridView.Name = "HallGridView";
            this.HallGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HallGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.HallGridView.RowHeadersVisible = false;
            this.HallGridView.RowHeadersWidth = 65;
            this.HallGridView.RowTemplate.Height = 30;
            this.HallGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.HallGridView.Size = new System.Drawing.Size(756, 289);
            this.HallGridView.TabIndex = 58;
            this.HallGridView.DoubleClick += new System.EventHandler(this.HallGridView_DoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Название";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 377;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Стоимость";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 377;
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(221, 137);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(584, 1);
            this.materialDivider1.TabIndex = 57;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // NameHallTextbox
            // 
            this.NameHallTextbox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.NameHallTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NameHallTextbox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NameHallTextbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.NameHallTextbox.Location = new System.Drawing.Point(221, 105);
            this.NameHallTextbox.Name = "NameHallTextbox";
            this.NameHallTextbox.Size = new System.Drawing.Size(584, 32);
            this.NameHallTextbox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(55, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 32);
            this.label2.TabIndex = 56;
            this.label2.Text = "Название";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(4)))), ((int)(((byte)(8)))));
            this.label1.Location = new System.Drawing.Point(280, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(292, 43);
            this.label1.TabIndex = 55;
            this.label1.Text = "Конференц-залы";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(221, 193);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(584, 1);
            this.materialDivider2.TabIndex = 63;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // CostHallTextBox
            // 
            this.CostHallTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CostHallTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CostHallTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CostHallTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.CostHallTextBox.Location = new System.Drawing.Point(221, 161);
            this.CostHallTextBox.Name = "CostHallTextBox";
            this.CostHallTextBox.Size = new System.Drawing.Size(584, 32);
            this.CostHallTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(55, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 32);
            this.label3.TabIndex = 62;
            this.label3.Text = "Стоимость";
            // 
            // ChangeHallBtn
            // 
            this.ChangeHallBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ChangeHallBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChangeHallBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeHallBtn.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeHallBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(215)))), ((int)(((byte)(206)))));
            this.ChangeHallBtn.Location = new System.Drawing.Point(507, 221);
            this.ChangeHallBtn.Name = "ChangeHallBtn";
            this.ChangeHallBtn.Size = new System.Drawing.Size(146, 44);
            this.ChangeHallBtn.TabIndex = 4;
            this.ChangeHallBtn.Text = "Изменить";
            this.ChangeHallBtn.UseVisualStyleBackColor = false;
            this.ChangeHallBtn.Click += new System.EventHandler(this.ChangeHallBtn_Click);
            // 
            // preloader
            // 
            this.preloader.Image = global::SmartHotel.Properties.Resources.preloader3;
            this.preloader.Location = new System.Drawing.Point(47, 84);
            this.preloader.Name = "preloader";
            this.preloader.Size = new System.Drawing.Size(758, 509);
            this.preloader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.preloader.TabIndex = 66;
            this.preloader.TabStop = false;
            this.preloader.Visible = false;
            // 
            // ConferencHallControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::SmartHotel.Properties.Resources.Panel;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.preloader);
            this.Controls.Add(this.ChangeHallBtn);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.CostHallTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DeleteHallBtn);
            this.Controls.Add(this.AddHallBtn);
            this.Controls.Add(this.HallGridView);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.NameHallTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Name = "ConferencHallControl";
            this.Size = new System.Drawing.Size(852, 621);
            ((System.ComponentModel.ISupportInitialize)(this.HallGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preloader)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DeleteHallBtn;
        private System.Windows.Forms.Button AddHallBtn;
        private System.Windows.Forms.DataGridView HallGridView;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private System.Windows.Forms.TextBox NameHallTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox CostHallTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button ChangeHallBtn;
        private System.Windows.Forms.PictureBox preloader;
    }
}
