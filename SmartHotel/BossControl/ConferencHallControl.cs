﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel
{
    public partial class ConferencHallControl : UserControl
    {
        ConferenceHall _hall;

        public ConferencHallControl()
        {
            InitializeComponent();
            ShowHallDataGrid();

        }
        
        //отображение в DataGrid
        private void ShowHallDataGrid()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                HallGridView.Rows.Clear();
                int i = 0;
                foreach (var item in db.ConferenceHall.ToList())
                {
                    HallGridView.Rows.Add();
                    HallGridView[0, i].Value = item.Name;
                    HallGridView[1, i].Value = item.Cost;
                    i++;
                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            NameHallTextbox.Text = "";
            CostHallTextBox.Text = "";
        }

        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (NameHallTextbox.Text != "" && CostHallTextBox.Text != "")
            {
                double cost;
                try
                {
                    cost = Double.Parse(CostHallTextBox.Text);
                    return cost;
                }
                catch
                {
                    return -1;
                }
            }
            return -1;
        }
        private void AddHallBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                double _c = TextBoxesHaveText();
                if (_c > 0)//если текстовые поля заполнены
                {
                    db.ConferenceHall.Add(new ConferenceHall { Name = NameHallTextbox.Text, Cost = _c });
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowHallDataGrid();
                    SmartHotel.MessageClass.AddSuccessfully();
                    ClearTextBox();
                    _hall = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            preloader.Visible = false;
        }

        private void ChangeHallBtn_Click(object sender, EventArgs e)
        {
            if (_hall != null)//если выбран  из перечня
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    double _c = TextBoxesHaveText();
                    if (_c > 0)//если текстовые поля заполнены
                    {
                        //заполняем структуру данных
                        _hall.Name = NameHallTextbox.Text;
                        _hall.Cost = _c;
                        //изменение
                        db.Entry(_hall).State = EntityState.Modified;
                        db.SaveChanges();//сохранение
                        ShowHallDataGrid();//перерисовка DataGrid
                        SmartHotel.MessageClass.ChangeSuccessfully();
                        ClearTextBox();
                        _hall = null;
                    }
                    else
                    {
                        SmartHotel.MessageClass.Error();
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteHallBtn_Click(object sender, EventArgs e)
        {
            if (HallGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string name = HallGridView[0, HallGridView.CurrentRow.Index].Value.ToString();
                    double cost = Double.Parse(HallGridView[1, HallGridView.CurrentRow.Index].Value.ToString());
                    //удаляем элемент, название и цена которого соответствуют выбранному
                    db.ConferenceHall.Remove(db.ConferenceHall.FirstOrDefault(x => x.Name == name && x.Cost == cost));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowHallDataGrid();//отображаем изменения в DataGrid
                    _hall = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }

        private void HallGridView_DoubleClick(object sender, EventArgs e)
        {
            if (HallGridView.RowCount > 0)
            {
                if (HallGridView.CurrentRow.Index != -1)
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        string name = HallGridView[0, HallGridView.CurrentRow.Index].Value.ToString();
                        _hall = db.ConferenceHall.FirstOrDefault(x => x.Name == name);
                        NameHallTextbox.Text = _hall.Name;
                        CostHallTextBox.Text = _hall.Cost.ToString();
                    }
                }
            }
        }
    }
}
