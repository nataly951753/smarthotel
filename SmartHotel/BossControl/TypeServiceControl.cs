﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel.BossControl
{
    public partial class TypeServiceControl : UserControl
    {
        TypeService _tservice;
        public TypeServiceControl()
        {
            InitializeComponent();
            ShowServiceDataGrid();

        }
        //отображение в DataGrid
        private void ShowServiceDataGrid()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                TypeServiceGridView.Rows.Clear();
                int i = 0;
                foreach (var item in db.TypeService.ToList())
                {
                    TypeServiceGridView.Rows.Add();
                    TypeServiceGridView[0, i].Value = item.Name;
                    TypeServiceGridView[1, i].Value = item.Cost;
                    i++;
                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            NameServiceTextbox.Text = "";
            CostServiceTextBox.Text = "";
        }

        //проверка заполненности текстовых полей
        private double TextBoxesHaveText()
        {
            if (NameServiceTextbox.Text != "" && CostServiceTextBox.Text != "")
            {
                double cost;
                try
                {
                    cost = Double.Parse(CostServiceTextBox.Text);
                    return cost;
                }
                catch
                {
                    return -1;
                }
            }
            return -1;
        }

        private void AddServiceBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                double _c = TextBoxesHaveText();
                if (_c >= 0)//если текстовые поля заполнены
                {
                    db.TypeService.Add(new TypeService { Name = NameServiceTextbox.Text, Cost = _c });
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowServiceDataGrid();
                    SmartHotel.MessageClass.AddSuccessfully();
                    ClearTextBox();
                    _tservice = null;
                }
                else
                {
                    SmartHotel.MessageClass.Error();
                }
            }
            preloader.Visible = false;
        }

        private void ChangeServiceBtn_Click(object sender, EventArgs e)
        {
            if (_tservice != null)//если выбран из перечня
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    double _c = TextBoxesHaveText();
                    if (_c > 0)//если текстовые поля заполнены
                    {
                        //заполняем структуру данных
                        _tservice.Name = NameServiceTextbox.Text;
                        _tservice.Cost = _c;
                        
                        //изменение
                        db.Entry(_tservice).State = EntityState.Modified;
                        db.SaveChanges();//сохранение
                        ShowServiceDataGrid();//перерисовка DataGrid
                        SmartHotel.MessageClass.ChangeSuccessfully();
                        ClearTextBox();
                        _tservice = null;
                    }
                    else
                    {
                        SmartHotel.MessageClass.Error();
                    }
                }
            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteServiceBtn_Click(object sender, EventArgs e)
        {
            if (TypeServiceGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string name = TypeServiceGridView[0, TypeServiceGridView.CurrentRow.Index].Value.ToString();
                    double cost = Double.Parse(TypeServiceGridView[1, TypeServiceGridView.CurrentRow.Index].Value.ToString());
                    //удаляем элемент, название и цена которого соответствуют выбранному
                    db.TypeService.Remove(db.TypeService.FirstOrDefault(x => x.Name == name && x.Cost == cost));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowServiceDataGrid();//отображаем изменения в DataGrid
                    _tservice = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }

        private void TypeServiceGridView_DoubleClick(object sender, EventArgs e)
        {
            if (TypeServiceGridView.RowCount > 0)
            {
                if (TypeServiceGridView.CurrentRow.Index != -1)
                {
                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        string name = TypeServiceGridView[0, TypeServiceGridView.CurrentRow.Index].Value.ToString();
                        _tservice = db.TypeService.FirstOrDefault(x => x.Name == name);
                        NameServiceTextbox.Text = _tservice.Name;
                        CostServiceTextBox.Text = _tservice.Cost.ToString();
                    }
                }
            }
        }
    }
}
