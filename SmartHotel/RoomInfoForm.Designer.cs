﻿namespace SmartHotel
{
    partial class RoomInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ExitButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.lunchcheckBox = new System.Windows.Forms.CheckBox();
            this.breakfastcheckBox = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Number_label = new System.Windows.Forms.Label();
            this.Floor_label = new System.Windows.Forms.Label();
            this.Typ_label = new System.Windows.Forms.Label();
            this.Adult_label = new System.Windows.Forms.Label();
            this.Children_label = new System.Windows.Forms.Label();
            this.Expense_label = new System.Windows.Forms.Label();
            this.dinnercheckBox = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.materialDivider5 = new MaterialSkin.Controls.MaterialDivider();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.dragControl1 = new SmartHotel.DragControl();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.ExitButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(459, 27);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.pictureBox1.BackgroundImage = global::SmartHotel.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 27);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // ExitButton
            // 
            this.ExitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.BackgroundImage = global::SmartHotel.Properties.Resources.Exit;
            this.ExitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ExitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.ExitButton.Location = new System.Drawing.Point(430, 3);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(20, 20);
            this.ExitButton.TabIndex = 2;
            this.ExitButton.UseVisualStyleBackColor = false;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label11.Location = new System.Drawing.Point(16, 226);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(157, 32);
            this.label11.TabIndex = 96;
            this.label11.Text = "Стоимость";
            // 
            // lunchcheckBox
            // 
            this.lunchcheckBox.AutoSize = true;
            this.lunchcheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.lunchcheckBox.Enabled = false;
            this.lunchcheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.lunchcheckBox.FlatAppearance.BorderSize = 5;
            this.lunchcheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.lunchcheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lunchcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lunchcheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.lunchcheckBox.Location = new System.Drawing.Point(270, 274);
            this.lunchcheckBox.Name = "lunchcheckBox";
            this.lunchcheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.lunchcheckBox.Size = new System.Drawing.Size(26, 25);
            this.lunchcheckBox.TabIndex = 82;
            this.lunchcheckBox.UseVisualStyleBackColor = false;
            // 
            // breakfastcheckBox
            // 
            this.breakfastcheckBox.AutoSize = true;
            this.breakfastcheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.breakfastcheckBox.Enabled = false;
            this.breakfastcheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.breakfastcheckBox.FlatAppearance.BorderSize = 5;
            this.breakfastcheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.breakfastcheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.breakfastcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.breakfastcheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.breakfastcheckBox.Location = new System.Drawing.Point(144, 273);
            this.breakfastcheckBox.Name = "breakfastcheckBox";
            this.breakfastcheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.breakfastcheckBox.Size = new System.Drawing.Size(26, 25);
            this.breakfastcheckBox.TabIndex = 81;
            this.breakfastcheckBox.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label9.Location = new System.Drawing.Point(184, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 32);
            this.label9.TabIndex = 95;
            this.label9.Text = "Обед";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label6.Location = new System.Drawing.Point(19, 269);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 32);
            this.label6.TabIndex = 94;
            this.label6.Text = "Завтрак";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label3.Location = new System.Drawing.Point(16, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 32);
            this.label3.TabIndex = 93;
            this.label3.Text = "Кол-во детей";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label4.Location = new System.Drawing.Point(16, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(236, 32);
            this.label4.TabIndex = 91;
            this.label4.Text = "Кол-во взрослых";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label8.Location = new System.Drawing.Point(16, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 32);
            this.label8.TabIndex = 90;
            this.label8.Text = "Тип";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label7.Location = new System.Drawing.Point(264, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 32);
            this.label7.TabIndex = 88;
            this.label7.Text = "Этаж";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label5.Location = new System.Drawing.Point(16, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 32);
            this.label5.TabIndex = 86;
            this.label5.Text = "Описание";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label2.Location = new System.Drawing.Point(16, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 32);
            this.label2.TabIndex = 84;
            this.label2.Text = "Номер";
            // 
            // Number_label
            // 
            this.Number_label.AutoSize = true;
            this.Number_label.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Number_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.Number_label.Location = new System.Drawing.Point(138, 40);
            this.Number_label.Name = "Number_label";
            this.Number_label.Size = new System.Drawing.Size(0, 32);
            this.Number_label.TabIndex = 98;
            // 
            // Floor_label
            // 
            this.Floor_label.AutoSize = true;
            this.Floor_label.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Floor_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.Floor_label.Location = new System.Drawing.Point(353, 40);
            this.Floor_label.Name = "Floor_label";
            this.Floor_label.Size = new System.Drawing.Size(0, 32);
            this.Floor_label.TabIndex = 99;
            // 
            // Typ_label
            // 
            this.Typ_label.AutoSize = true;
            this.Typ_label.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Typ_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.Typ_label.Location = new System.Drawing.Point(117, 86);
            this.Typ_label.Name = "Typ_label";
            this.Typ_label.Size = new System.Drawing.Size(0, 32);
            this.Typ_label.TabIndex = 100;
            // 
            // Adult_label
            // 
            this.Adult_label.AutoSize = true;
            this.Adult_label.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Adult_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.Adult_label.Location = new System.Drawing.Point(280, 135);
            this.Adult_label.Name = "Adult_label";
            this.Adult_label.Size = new System.Drawing.Size(0, 32);
            this.Adult_label.TabIndex = 101;
            // 
            // Children_label
            // 
            this.Children_label.AutoSize = true;
            this.Children_label.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Children_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.Children_label.Location = new System.Drawing.Point(215, 180);
            this.Children_label.Name = "Children_label";
            this.Children_label.Size = new System.Drawing.Size(0, 32);
            this.Children_label.TabIndex = 102;
            // 
            // Expense_label
            // 
            this.Expense_label.AutoSize = true;
            this.Expense_label.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Expense_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.Expense_label.Location = new System.Drawing.Point(200, 223);
            this.Expense_label.Name = "Expense_label";
            this.Expense_label.Size = new System.Drawing.Size(0, 32);
            this.Expense_label.TabIndex = 103;
            // 
            // dinnercheckBox
            // 
            this.dinnercheckBox.AutoSize = true;
            this.dinnercheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.dinnercheckBox.Enabled = false;
            this.dinnercheckBox.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.dinnercheckBox.FlatAppearance.BorderSize = 5;
            this.dinnercheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Maroon;
            this.dinnercheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dinnercheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dinnercheckBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.dinnercheckBox.Location = new System.Drawing.Point(421, 273);
            this.dinnercheckBox.Name = "dinnercheckBox";
            this.dinnercheckBox.Padding = new System.Windows.Forms.Padding(7);
            this.dinnercheckBox.Size = new System.Drawing.Size(26, 25);
            this.dinnercheckBox.TabIndex = 104;
            this.dinnercheckBox.UseVisualStyleBackColor = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.label10.Location = new System.Drawing.Point(318, 269);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 32);
            this.label10.TabIndex = 105;
            this.label10.Text = "Ужин";
            // 
            // materialDivider5
            // 
            this.materialDivider5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(58)))), ((int)(((byte)(80)))));
            this.materialDivider5.Depth = 0;
            this.materialDivider5.Location = new System.Drawing.Point(27, 436);
            this.materialDivider5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider5.Name = "materialDivider5";
            this.materialDivider5.Size = new System.Drawing.Size(420, 1);
            this.materialDivider5.TabIndex = 182;
            this.materialDivider5.Text = "materialDivider5";
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.DescriptionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DescriptionTextBox.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DescriptionTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(21)))), ((int)(((byte)(38)))));
            this.DescriptionTextBox.Location = new System.Drawing.Point(27, 341);
            this.DescriptionTextBox.Multiline = true;
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.ReadOnly = true;
            this.DescriptionTextBox.Size = new System.Drawing.Size(420, 96);
            this.DescriptionTextBox.TabIndex = 181;
            // 
            // dragControl1
            // 
            this.dragControl1.selectControl = this.panel1;
            // 
            // RoomInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 445);
            this.Controls.Add(this.materialDivider5);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.dinnercheckBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Expense_label);
            this.Controls.Add(this.Children_label);
            this.Controls.Add(this.Adult_label);
            this.Controls.Add(this.Typ_label);
            this.Controls.Add(this.Floor_label);
            this.Controls.Add(this.Number_label);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lunchcheckBox);
            this.Controls.Add(this.breakfastcheckBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RoomInfoForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "RoomInfoForm";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox lunchcheckBox;
        private System.Windows.Forms.CheckBox breakfastcheckBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Number_label;
        private System.Windows.Forms.Label Floor_label;
        private System.Windows.Forms.Label Typ_label;
        private System.Windows.Forms.Label Adult_label;
        private System.Windows.Forms.Label Children_label;
        private System.Windows.Forms.Label Expense_label;
        private System.Windows.Forms.CheckBox dinnercheckBox;
        private System.Windows.Forms.Label label10;
        private MaterialSkin.Controls.MaterialDivider materialDivider5;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private DragControl dragControl1;
    }
}