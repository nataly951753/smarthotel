﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;

namespace SmartHotel
{
    public partial class RestaurantForm : Form
    {
        public Button btnNow;//выбранная в меню кнопка


        public RestaurantForm()
        {
            InitializeComponent();
            btnNow = GraphBtn;
            _graphRestaurantControl.SetControl(_newDishControl);
            _graphRestaurantControl.BringToFront();

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();

        }
        private void ChangeMenuBtn(Button changeBtn)
        {
            SelectorMenu.Top = changeBtn.Top;
            changeBtn.BackColor = Color.FromArgb(60, 23, 49);
            btnNow.BackColor = Color.FromArgb(54, 21, 38);
            btnNow = changeBtn;
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }

        private void GraphBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(GraphBtn);
            _graphRestaurantControl.ShowForm();
            _graphRestaurantControl.BringToFront();
        }

        private void NewDishBtn_Click(object sender, EventArgs e)
        {
            ChangeMenuBtn(NewDishBtn);
            _newDishControl.ShowForm();
            _newDishControl.BringToFront();
        }
    }
}
