﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel.ServiceControl
{
    public partial class GraphServiceControl : UserControl
    {

        Color WillDoneColor = Color.Yellow;
        Color DontPaidColor = Color.PaleVioletRed;
        Color DontPaidAndDoneColor = Color.AliceBlue;

        NewServiceControl _controlNS;


        public GraphServiceControl()
        {
            InitializeComponent();

            ShowDataGrid();//отображение графика в виде таблицы
            labelDay.Text = DateTime.Now.ToString("dd MMMM");//текущий день в заголовке
        }
        public void SetControl(NewServiceControl r)
        {
            _controlNS = r;
        }
        public void ShowForm()
        {
            ShowDataGrid();//отображение графика в виде таблицы
        }
        private void SetCell(int g, PlanningService plan, int i)
        {

            if (plan.Done && !plan.Paid)
                GraphGridView[g, i].Style.BackColor = DontPaidColor;
            else if (!plan.Done && plan.Paid)
                GraphGridView[g, i].Style.BackColor = WillDoneColor;
            else if (!plan.Done && !plan.Paid)
                GraphGridView[g, i].Style.BackColor = DontPaidAndDoneColor;
        }
        private void ShowDataGrid()
        {
            //очистка
            GraphGridView.Rows.Clear();
            GraphGridView.Columns.Clear();
            int i = 0;//строки
            GraphGridView.Columns.Add("colNumber", "Номер");
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                //заголовки колонок по типам предоставляемых услуг
                foreach (var item in db.TypeService)
                {
                    GraphGridView.Columns.Add("col" + item.Id, item.Name);
                }

                foreach (var item in db.PlanningRooms.ToList())
                {
                    GraphGridView.Rows.Add();
                    GraphGridView[0, i].Value = item.FullName;
                    //создаем пустые ячейки по кол-ву услуг
                    for (int k = 1; k <= db.TypeService.Count(); k++)
                    {
                        GraphGridView[k, i].Value = "";
                        //проверка на service на текущей номер(первый столбец строки) в текущий день
                        foreach (var plan in db.PlanningService.ToList())
                        {
                            //если день заказа - текущий 
                            if (plan.Date.Month == DateTime.Now.Month && plan.Date.Year == DateTime.Now.Year && plan.Date.Day == DateTime.Now.Day)
                            {
                                //если номер из коллекции - номер в столбце
                                if (plan.PlanningRooms.FullName == GraphGridView[0, i].Value.ToString())
                                {
                                    //если соответствует столбец услуги
                                    if (plan.TypeService.Name == GraphGridView.Columns[k].HeaderText)
                                    {                                        
                                        SetCell(k, plan, i);
                                    }
                                }
                            }
                        }
                    }
                    i++;
                }
            }
        }
        private void InfoRoomBtn_Click(object sender, EventArgs e)
        {
            PlanningRooms room = null;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningRooms.ToList())
                {
                    //если выбранная ячейка является номером комнаты
                    if (GraphGridView.CurrentCell.Value.ToString() == item.FullName.ToString())
                    {
                        room = item;
                    }
                }
                if (room != null)
                {
                    ArrivalInfoForm form = new ArrivalInfoForm(room);
                    form.ShowDialog();
                }
                else
                    SmartHotel.MessageClass.RoomError();
            }
        }

        private void AddServiceBtn_Click(object sender, EventArgs e)
        {
            //иконка не является номером

            if ( GraphGridView.CurrentCell.Value.ToString() == "")
            {
                _controlNS.SetQuick(GraphGridView[0, GraphGridView.CurrentCell.RowIndex].Value.ToString(), GraphGridView.Columns[GraphGridView.CurrentCell.ColumnIndex].HeaderText);
                SmartHotel.MessageClass.GoToNew();
            }
            else
                SmartHotel.MessageClass.EmptyCellError();
        }

       
    }
}
