﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;
using System.Data.Entity;

namespace SmartHotel.ServiceControl
{
    public partial class NewServiceControl : UserControl
    {
        PlanningService pl;
        bool flagUpdateCombo = true;

        public NewServiceControl()
        {
            InitializeComponent();
            ShowForm();
            ShowDataGrid();

        }
        public void ShowForm()
        {
            if (flagUpdateCombo)
            {
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    //номера все
                    numberComboBox.DataSource = db.PlanningRooms.Select(x => x.FullName).ToList();
                    //названия service
                    service_comboBox.DataSource = db.TypeService.Select(x => x.Name).ToList();
                    ShowDataGrid();
                }
            }
            else
                flagUpdateCombo = true;
        }
        //заполнение данными из Графика
        public void SetQuick(string num, string ser)
        {
            numberComboBox.Text = num;
            service_comboBox.Text = ser;
            service_monthCalendar.SetDate(DateTime.Now);
            flagUpdateCombo = false;

        }
        //отображение в DataGrid
        private void ShowDataGrid()
        {
            ServiceGridView.Rows.Clear();
            int i = 0;
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                foreach (var item in db.PlanningService.ToList())
                {

                    ServiceGridView.Rows.Add();
                    ServiceGridView[0, i].Value = item.PlanningRooms.FullName;
                    ServiceGridView[1, i].Value = item.TypeService.Name;
                    ServiceGridView[2, i].Value = item.Date.ToShortDateString();
                    ServiceGridView[3, i].Value = item.Paid;
                    ServiceGridView[4, i].Value = item.Id;
                    i++;

                }
            }
        }
        //очистка текстовых полей
        private void ClearTextBox()
        {
            DescriptionTextBox.Text = "";
            paidcheckBox.Checked = false;
            doneCheckBox.Checked = false;
            ExpenseTextBox.Text = "0";
        }


        private void SetExpense()
        {
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                double cost = db.TypeService.FirstOrDefault(x => x.Name == service_comboBox.Text).Cost;//стоимость услуги
                ExpenseTextBox.Text = cost.ToString();
            }
        }

        private void DishGridView_DoubleClick(object sender, EventArgs e)
        {
            if (ServiceGridView.RowCount > 0)
            {
                if (ServiceGridView.CurrentRow.Index != -1)
                {
                    ClearTextBox();
                    string id = ServiceGridView[4, ServiceGridView.CurrentRow.Index].Value.ToString();

                    using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                    {
                        pl = db.PlanningService.FirstOrDefault(x => x.Id.ToString() == id);
                        ExpenseTextBox.Text = pl.TypeService.Cost.ToString();
                        paidcheckBox.Checked = pl.Paid;
                        doneCheckBox.Checked = pl.Done;
                        DescriptionTextBox.Text = pl.Description;
                        numberComboBox.Text = pl.PlanningRooms.FullName;
                        service_comboBox.Text = pl.TypeService.Name;
                        service_monthCalendar.SetDate(pl.Date);
                    }
                    SetExpense();
                }
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            //анимация выполнения поверх кнопки
            preloader.Visible = true;

            SetExpense();
            using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
            {
                int number = db.PlanningRooms.FirstOrDefault(x => x.FullName == numberComboBox.Text).Id;//номер комнаты
                int service = db.TypeService.FirstOrDefault(x => x.Name == service_comboBox.Text).Id;//service

                db.PlanningService.Add(new PlanningService { Room = number, Service = service, Description = DescriptionTextBox.Text, Paid = paidcheckBox.Checked, Done = doneCheckBox.Checked, Date = service_monthCalendar.SelectionRange.Start });

                db.SaveChanges();//сохраняем изменения бд      
                ShowDataGrid();
                SmartHotel.MessageClass.AddSuccessfully();
                ClearTextBox();
                pl = null;
            }


            preloader.Visible = false;
        }

        private void ChangeBtn_Click(object sender, EventArgs e)
        {
            if (pl != null)//если выбран из перечня 
            {

                SetExpense();
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    int number = db.PlanningRooms.FirstOrDefault(x => x.FullName == numberComboBox.Text).Id;//номер комнаты

                    int service = db.TypeService.FirstOrDefault(x => x.Name == service_comboBox.Text).Id;//service

                    PlanningService plan_new = db.PlanningService.Find(pl.Id);
                    //заполняем структуру данных
                    plan_new.Room = number;
                    plan_new.Service = service;
                    plan_new.Paid = paidcheckBox.Checked;
                    plan_new.Done = doneCheckBox.Checked;
                    plan_new.Description = DescriptionTextBox.Text;
                    plan_new.Date = service_monthCalendar.SelectionRange.Start;
                    //изменение
                    db.Entry(plan_new).State = EntityState.Modified;
                    db.SaveChanges();//сохранение
                    ShowDataGrid();//перерисовка DataGrid
                    SmartHotel.MessageClass.ChangeSuccessfully();
                    ClearTextBox();
                }

                pl = null;

            }
            else
            {
                SmartHotel.MessageClass.ErrorClick();
            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (ServiceGridView.RowCount > 0)
            {
                preloader.Visible = true;
                using (SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities())
                {
                    string number = ServiceGridView[4, ServiceGridView.CurrentRow.Index].Value.ToString();
                    //удаляем элемент, номер которого соответствует выбранному
                    db.PlanningService.Remove(db.PlanningService.FirstOrDefault(x => x.Id.ToString() == number));
                    db.SaveChanges();//сохраняем изменения бд      
                    ShowDataGrid();//отображаем изменения в DataGrid
                    pl = null;
                    SmartHotel.MessageClass.RemoveSuccessfully();
                }
                preloader.Visible = false;
            }
        }

        private void ExpenseBtn_Click(object sender, EventArgs e)
        {
            SetExpense();

        }
    }
}
