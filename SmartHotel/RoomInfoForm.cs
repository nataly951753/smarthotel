﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartHotel.EntityModels;

namespace SmartHotel
{
    public partial class RoomInfoForm : Form
    {
        public RoomInfoForm(Rooms r)
        {
            InitializeComponent();
            Expense_label.Text = r.Cost.ToString();
            Floor_label.Text = r.Floor;
            Children_label.Text = r.NumberOfChildren;
            Adult_label.Text = r.NumberOfAdults;
            Number_label.Text = r.Number;
            Typ_label.Text = r.TypeRooms.Type;
            DescriptionTextBox.Text = r.Description;
            breakfastcheckBox.Checked = r.Breakfast;
            lunchcheckBox.Checked = r.Lunch;
            dinnercheckBox.Checked = r.Dinner;
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
