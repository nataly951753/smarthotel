﻿using SmartHotel.EntityModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace SmartHotel
{
    public partial class LoginForm : Form
    {
        Thread th;
        Users user;
        public LoginForm()
        {
            InitializeComponent();

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (loginTextbox.Text != "" && PasswordTextBox.Text != "")
            {
                preloader.Visible = true;
                LoginButton.Visible = false;
                loginTextbox.ReadOnly = true;
                PasswordTextBox.ReadOnly = true;
                LoginBackgroundWorker.RunWorkerAsync();
            }

        }

        private void OpenBossForm()
        {
            Application.Run(new BossForm());
        }
        private void OpenManagerForm()
        {
            Application.Run(new ManagerForm());
        }
        private void OpenServiceForm()
        {
            Application.Run(new ServiceForm());
        }
        private void OpenRestaurantForm()
        {
            Application.Run(new RestaurantForm());
        }
        private void LoginBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            SmartHotelDatabaseEntities db = new SmartHotelDatabaseEntities();
            user = db.Users.FirstOrDefault(x => (x.Login == loginTextbox.Text) && (x.Password == PasswordTextBox.Text));

        }

        private void LoginBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            preloader.Visible = false;
            if (user != null)
            {
                this.Close();
                if (user.TypeUsers.Post == "BOSS")
                    th = new Thread(OpenBossForm);
                else if (user.TypeUsers.Post == "MANAGER")
                    th = new Thread(OpenManagerForm);
                else if (user.TypeUsers.Post == "SERVICE")
                    th = new Thread(OpenServiceForm);
                else if (user.TypeUsers.Post == "RESTAURANT")
                    th = new Thread(OpenRestaurantForm);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
            }
            else
            {
                LoginButton.Visible = true;
                preloader.Visible = false;
                loginTextbox.ReadOnly = false;
                PasswordTextBox.ReadOnly = false;
                MessageForm form = new MessageForm("Не верные\n Логин или Пароль");
                form.ShowDialog();
            }
        }
    }
}
